
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import java.lang.String

import org.sikuli.script.Screen

import java.util.List


def static "callCenter.Window_OperacionExitosa.verificarOperacionExitosa"() {
    (new callCenter.Window_OperacionExitosa()).verificarOperacionExitosa()
}

def static "callCenter.Window_OperacionExitosa.copiarCodigoReclamo"() {
    (new callCenter.Window_OperacionExitosa()).copiarCodigoReclamo()
}

def static "callCenter.Window_OperacionExitosa.aceptarOperacion"() {
    (new callCenter.Window_OperacionExitosa()).aceptarOperacion()
}

def static "callCenter.Window_OperacionExitosa.guardarCodigoDeReclamo"(
    	Object path	
     , 	Object sheet	) {
    (new callCenter.Window_OperacionExitosa()).guardarCodigoDeReclamo(
        	path
         , 	sheet)
}

def static "callCenter.Window_OperacionExitosa.guardarCodigoDeReclamo"(
    	Object path	
     , 	Object sheet	
     , 	Object row	
     , 	Object column	) {
    (new callCenter.Window_OperacionExitosa()).guardarCodigoDeReclamo(
        	path
         , 	sheet
         , 	row
         , 	column)
}

def static "callCenter.Window_OperacionExitosa.guardarCodigoDeReclamoOverwrite"(
    	Object path	
     , 	Object sheet	
     , 	Object row	
     , 	Object column	) {
    (new callCenter.Window_OperacionExitosa()).guardarCodigoDeReclamoOverwrite(
        	path
         , 	sheet
         , 	row
         , 	column)
}

def static "cromo.Page_Main.verificarNombreDeUsuarioVisible"() {
    (new cromo.Page_Main()).verificarNombreDeUsuarioVisible()
}

def static "cromo.Page_Main.verificarMapaVisible"() {
    (new cromo.Page_Main()).verificarMapaVisible()
}

def static "cromo.Page_Main.seleccionarAnomalia"(
    	String zona	
     , 	String partido	
     , 	String localidad	
     , 	String idElemento	) {
    (new cromo.Page_Main()).seleccionarAnomalia(
        	zona
         , 	partido
         , 	localidad
         , 	idElemento)
}

def static "cromo.Page_Main.seleccionarAreaActiva"(
    	String zona	
     , 	String partido	
     , 	String localidad	) {
    (new cromo.Page_Main()).seleccionarAreaActiva(
        	zona
         , 	partido
         , 	localidad)
}

def static "cromo.Page_Main.clickAreaDeConsecion"() {
    (new cromo.Page_Main()).clickAreaDeConsecion()
}

def static "cromo.Page_Main.ingresarZona"(
    	String zona	) {
    (new cromo.Page_Main()).ingresarZona(
        	zona)
}

def static "cromo.Page_Main.ingresarPartido"(
    	String partido	) {
    (new cromo.Page_Main()).ingresarPartido(
        	partido)
}

def static "cromo.Page_Main.ingresarLocalidad"(
    	String localidad	) {
    (new cromo.Page_Main()).ingresarLocalidad(
        	localidad)
}

def static "cromo.Page_Main.clickIrASeleccion"() {
    (new cromo.Page_Main()).clickIrASeleccion()
}

def static "cromo.Page_Main.seleccionarElementoAfectado"(
    	String idLocalidad	
     , 	String idSuministro	) {
    (new cromo.Page_Main()).seleccionarElementoAfectado(
        	idLocalidad
         , 	idSuministro)
}

def static "cromo.Page_Main.home"() {
    (new cromo.Page_Main()).home()
}

def static "cromo.Page_Main.irALocalidad"(
    	String idLocalidad	) {
    (new cromo.Page_Main()).irALocalidad(
        	idLocalidad)
}

def static "cromo.Page_Main.seleccionarSuministro"(
    	String suministro	) {
    (new cromo.Page_Main()).seleccionarSuministro(
        	suministro)
}

def static "cromo.Page_Main.zoomSuministro"(
    	String idSuministro	) {
    (new cromo.Page_Main()).zoomSuministro(
        	idSuministro)
}

def static "cromo.Page_Main.clickSuministro"(
    	String idSuministro	) {
    (new cromo.Page_Main()).clickSuministro(
        	idSuministro)
}

def static "cromo.Page_Main.clickElemento"(
    	String idElemento	) {
    (new cromo.Page_Main()).clickElemento(
        	idElemento)
}

def static "cromo.Page_Main.aceptarElemento"() {
    (new cromo.Page_Main()).aceptarElemento()
}

def static "cromo.Page_Main.seleccionarCliente"(
    	String idCliente	) {
    (new cromo.Page_Main()).seleccionarCliente(
        	idCliente)
}

def static "cromo.Page_Main.buscarCliente"(
    	String idCliente	) {
    (new cromo.Page_Main()).buscarCliente(
        	idCliente)
}

def static "cromo.Page_Main.getIdSuministro"() {
    (new cromo.Page_Main()).getIdSuministro()
}

def static "cromo.Page_Main.clickVerClientes"() {
    (new cromo.Page_Main()).clickVerClientes()
}

def static "cromo.Page_Main.buscarSuministro"(
    	Object suministro	) {
    (new cromo.Page_Main()).buscarSuministro(
        	suministro)
}

def static "cromo.Page_Main.verificarpopUpZoom"() {
    (new cromo.Page_Main()).verificarpopUpZoom()
}

def static "cromo.Page_Main.seleccionarElementoPorId"(
    	String zona	
     , 	String partido	
     , 	String localidad	
     , 	String idElemento	) {
    (new cromo.Page_Main()).seleccionarElementoPorId(
        	zona
         , 	partido
         , 	localidad
         , 	idElemento)
}

def static "cromo.Page_Main.seleccionarCoordenadasElemento"(
    	String zona	
     , 	String partido	
     , 	String localidad	
     , 	String idElemento	) {
    (new cromo.Page_Main()).seleccionarCoordenadasElemento(
        	zona
         , 	partido
         , 	localidad
         , 	idElemento)
}

def static "cromo.Page_Main.clickDinamicElement"(
    	String dynamicId	) {
    (new cromo.Page_Main()).clickDinamicElement(
        	dynamicId)
}

def static "oms.Window_Documento.focus"() {
    (new oms.Window_Documento()).focus()
}

def static "oms.Window_Documento.clickEncabezado"() {
    (new oms.Window_Documento()).clickEncabezado()
}

def static "oms.Window_Documento.clickActividades"() {
    (new oms.Window_Documento()).clickActividades()
}

def static "oms.Window_Documento.clickReclamos"() {
    (new oms.Window_Documento()).clickReclamos()
}

def static "oms.Window_Documento.clickDespachoMovil"() {
    (new oms.Window_Documento()).clickDespachoMovil()
}

def static "oms.Window_Documento.clickAfectar"() {
    (new oms.Window_Documento()).clickAfectar()
}

def static "oms.Window_Documento.clickRestauracion"() {
    (new oms.Window_Documento()).clickRestauracion()
}

def static "oms.Window_Documento.clickOperar"() {
    (new oms.Window_Documento()).clickOperar()
}

def static "oms.Window_Documento.clickNuevoTrabajo"() {
    (new oms.Window_Documento()).clickNuevoTrabajo()
}

def static "oms.Window_Documento.clickCerrar"() {
    (new oms.Window_Documento()).clickCerrar()
}

def static "oms.Window_Documento.clickModificarModoRegistracion"() {
    (new oms.Window_Documento()).clickModificarModoRegistracion()
}

def static "oms.Window_Documento.verificarCierreDefinitivo"() {
    (new oms.Window_Documento()).verificarCierreDefinitivo()
}

def static "oms.Window_Documento.actualizarRestauracion"(
    	String horasRestauracion	) {
    (new oms.Window_Documento()).actualizarRestauracion(
        	horasRestauracion)
}

def static "oms.Window_Documento.clickActualizar"() {
    (new oms.Window_Documento()).clickActualizar()
}

def static "oms.Window_Documento.cerrarVentana"() {
    (new oms.Window_Documento()).cerrarVentana()
}

def static "oms.Window_Documento.guardarNumeroDocumento"() {
    (new oms.Window_Documento()).guardarNumeroDocumento()
}

def static "oms.Window_Documento.verificarClientesRestaurados"() {
    (new oms.Window_Documento()).verificarClientesRestaurados()
}

def static "oms.Window_Documento.verificarCierreProvisorio"() {
    (new oms.Window_Documento()).verificarCierreProvisorio()
}

def static "oms.Window_Documento.verificarCierrePostoperacion"() {
    (new oms.Window_Documento()).verificarCierrePostoperacion()
}

def static "oms.Window_Documento.guardarDocumento"() {
    (new oms.Window_Documento()).guardarDocumento()
}

def static "oms.Window_Documento.verificarNumeroDeDocumento"(
    	String path	
     , 	int sheet	
     , 	int row	
     , 	int column	) {
    (new oms.Window_Documento()).verificarNumeroDeDocumento(
        	path
         , 	sheet
         , 	row
         , 	column)
}

def static "oms.Window_Documento.getAfectadosAlComienzo"() {
    (new oms.Window_Documento()).getAfectadosAlComienzo()
}

def static "oms.Window_Documento.verificarNoAfectadosActuales"() {
    (new oms.Window_Documento()).verificarNoAfectadosActuales()
}

def static "oms.Window_Documento.verificarNoReclamosAbiertosActuales"() {
    (new oms.Window_Documento()).verificarNoReclamosAbiertosActuales()
}

def static "oms.Window_Documento.getAfectadosAhora"() {
    (new oms.Window_Documento()).getAfectadosAhora()
}

def static "oms.Window_Documento.getReclamosAbiertos"() {
    (new oms.Window_Documento()).getReclamosAbiertos()
}

def static "oms.Window_Documento.clickTomar"() {
    (new oms.Window_Documento()).clickTomar()
}

def static "oms.Window_Documento.tabAuditoria"() {
    (new oms.Window_Documento()).tabAuditoria()
}

def static "oms.Window_Documento.guardarDocumento"(
    	Object path	
     , 	Object sheet	
     , 	Object row	
     , 	Object column	) {
    (new oms.Window_Documento()).guardarDocumento(
        	path
         , 	sheet
         , 	row
         , 	column)
}

def static "oms.Window_Documento.clickAnomalias"() {
    (new oms.Window_Documento()).clickAnomalias()
}

def static "oms.Window_Documento.clickCerrarVentana"() {
    (new oms.Window_Documento()).clickCerrarVentana()
}

def static "oms.Window_Documento.clickEsquina"() {
    (new oms.Window_Documento()).clickEsquina()
}

def static "oms.Window_Documento.verificarCliente"(
    	String path	
     , 	int sheet	
     , 	int row	
     , 	int column	) {
    (new oms.Window_Documento()).verificarCliente(
        	path
         , 	sheet
         , 	row
         , 	column)
}

def static "oms.Window_Documento.cerrarVentanaForzado"() {
    (new oms.Window_Documento()).cerrarVentanaForzado()
}

def static "oms.Window_Documento.clickEquiposMoviles"() {
    (new oms.Window_Documento()).clickEquiposMoviles()
}

def static "oms.Window_Documento.clickTaskBarIcon"() {
    (new oms.Window_Documento()).clickTaskBarIcon()
}

def static "oms.Window_Documento.focusOnDocument"() {
    (new oms.Window_Documento()).focusOnDocument()
}

def static "oms.Window_Documento.clickRecuperarReclamos"() {
    (new oms.Window_Documento()).clickRecuperarReclamos()
}

def static "oms.Window_Documento.verificarDocumentoCancelado"() {
    (new oms.Window_Documento()).verificarDocumentoCancelado()
}

def static "oms.Window_Documento.clickLiberar"() {
    (new oms.Window_Documento()).clickLiberar()
}

def static "oms.Window_Documento.clickZoom"() {
    (new oms.Window_Documento()).clickZoom()
}

def static "oms.Window_Documento.getNumeroDocumento"() {
    (new oms.Window_Documento()).getNumeroDocumento()
}

def static "callCenter.Windows_AfeccionesRecientesDelCliente.clickNuevoReclamo"() {
    (new callCenter.Windows_AfeccionesRecientesDelCliente()).clickNuevoReclamo()
}

def static "cdp.CDP_Main.focus"() {
    (new cdp.CDP_Main()).focus()
}

def static "cdp.CDP_Main.verificarInicioDeCDP"() {
    (new cdp.CDP_Main()).verificarInicioDeCDP()
}

def static "oms.ProcesoAfectacionRestauracion.inicioAfectacion"(
    	String tipoSeleccion	) {
    (new oms.ProcesoAfectacionRestauracion()).inicioAfectacion(
        	tipoSeleccion)
}

def static "oms.ProcesoAfectacionRestauracion.inicioRestauracion"(
    	String tipoSeleccion	) {
    (new oms.ProcesoAfectacionRestauracion()).inicioRestauracion(
        	tipoSeleccion)
}

def static "oms.ProcesoAfectacionRestauracion.finalizarAfectacion"() {
    (new oms.ProcesoAfectacionRestauracion()).finalizarAfectacion()
}

def static "oms.ProcesoAfectacionRestauracion.finalizarRestauracion"() {
    (new oms.ProcesoAfectacionRestauracion()).finalizarRestauracion()
}

def static "oms.ProcesoAfectacionRestauracion.esperarAfectacion"(
    	Object secs	) {
    (new oms.ProcesoAfectacionRestauracion()).esperarAfectacion(
        	secs)
}

def static "sikuli.Oms.EnfocarOMS"() {
    (new sikuli.Oms()).EnfocarOMS()
}

def static "sikuli.Oms.GenerarDocProgmadoBT"(
    	String fechaCorte	) {
    (new sikuli.Oms()).GenerarDocProgmadoBT(
        	fechaCorte)
}

def static "sikuli.Oms.AfectarElementoBT"() {
    (new sikuli.Oms()).AfectarElementoBT()
}

def static "svp.SVP_Main.focus"() {
    (new svp.SVP_Main()).focus()
}

def static "svp.SVP_Main.verificarInicioDeSVP"() {
    (new svp.SVP_Main()).verificarInicioDeSVP()
}

def static "helper.UIContext.killAllApps"() {
    (new helper.UIContext()).killAllApps()
}

def static "helper.UIContext.getCount"() {
    (new helper.UIContext()).getCount()
}

def static "helper.UIContext.updateCount"() {
    (new helper.UIContext()).updateCount()
}

def static "helper.UIContext.resetCount"() {
    (new helper.UIContext()).resetCount()
}

def static "helper.UIContext.selectField"() {
    (new helper.UIContext()).selectField()
}

def static "oms.ProcesoOperacion.inicioAperturaDocumento"() {
    (new oms.ProcesoOperacion()).inicioAperturaDocumento()
}

def static "oms.ProcesoOperacion.finalizarAperturaDocumento"() {
    (new oms.ProcesoOperacion()).finalizarAperturaDocumento()
}

def static "oms.ProcesoOperacion.inicioCierreDocumento"() {
    (new oms.ProcesoOperacion()).inicioCierreDocumento()
}

def static "oms.ProcesoOperacion.finalizarCierreDocumento"() {
    (new oms.ProcesoOperacion()).finalizarCierreDocumento()
}

def static "oms.Window_SeleccionTopologica.seleccionarElementoClienteGrafico"() {
    (new oms.Window_SeleccionTopologica()).seleccionarElementoClienteGrafico()
}

def static "callCenter.Casos.caso15VerificarDocumento"() {
    (new callCenter.Casos()).caso15VerificarDocumento()
}

def static "oms.Window_Documento_Tab_Auditoria.verificarDespacho"() {
    (new oms.Window_Documento_Tab_Auditoria()).verificarDespacho()
}

def static "workstation.Configuration.verifyConfigurationFile"(
    	String ambiente	) {
    (new workstation.Configuration()).verifyConfigurationFile(
        	ambiente)
}

def static "callCenter.Window_Reclamos.ingresarMotivoSubmotivoDeReclamo"(
    	String tipoReclamo	
     , 	String userId	
     , 	String motivo	
     , 	String submotivo	) {
    (new callCenter.Window_Reclamos()).ingresarMotivoSubmotivoDeReclamo(
        	tipoReclamo
         , 	userId
         , 	motivo
         , 	submotivo)
}

def static "callCenter.Window_Reclamos.clickAceptar"() {
    (new callCenter.Window_Reclamos()).clickAceptar()
}

def static "callCenter.Window_Reclamos.seleccionaSubMotivo"(
    	Object index	) {
    (new callCenter.Window_Reclamos()).seleccionaSubMotivo(
        	index)
}

def static "callCenter.Window_Reclamos.aceptarFormulario"() {
    (new callCenter.Window_Reclamos()).aceptarFormulario()
}

def static "callCenter.Window_Reclamos.seleccionaPeligrosidad"(
    	Object index	) {
    (new callCenter.Window_Reclamos()).seleccionaPeligrosidad(
        	index)
}

def static "callCenter.Window_Reclamos.seleccionarTipo"(
    	Object index	) {
    (new callCenter.Window_Reclamos()).seleccionarTipo(
        	index)
}

def static "callCenter.Window_Reclamos.seleccionarMotivo"(
    	Object index	) {
    (new callCenter.Window_Reclamos()).seleccionarMotivo(
        	index)
}

def static "callCenter.Window_resultadosDeLaBusqueda.clickAceptar"() {
    (new callCenter.Window_resultadosDeLaBusqueda()).clickAceptar()
}

def static "workstation.Workstation.startWorkstationCore"() {
    (new workstation.Workstation()).startWorkstationCore()
}

def static "workstation.Workstation.login"(
    	String user	
     , 	String password	) {
    (new workstation.Workstation()).login(
        	user
         , 	password)
}

def static "workstation.Workstation.clickEnCartografia"() {
    (new workstation.Workstation()).clickEnCartografia()
}

def static "workstation.Workstation.login2"(
    	String user	
     , 	String password	) {
    (new workstation.Workstation()).login2(
        	user
         , 	password)
}

def static "workstation.Workstation.openOMS"() {
    (new workstation.Workstation()).openOMS()
}

def static "workstation.Workstation.openCallCenter"() {
    (new workstation.Workstation()).openCallCenter()
}

def static "workstation.Workstation.openSVP"() {
    (new workstation.Workstation()).openSVP()
}

def static "workstation.Workstation.openCDP"(
    	Object ambiente	) {
    (new workstation.Workstation()).openCDP(
        	ambiente)
}

def static "workstation.Workstation.cerrarApps"() {
    (new workstation.Workstation()).cerrarApps()
}

def static "workstation.Workstation.verificacionNavigatorAbierto"(
    	Object user	
     , 	Object password	) {
    (new workstation.Workstation()).verificacionNavigatorAbierto(
        	user
         , 	password)
}

def static "workstation.Workstation.loginCorrecto2"() {
    (new workstation.Workstation()).loginCorrecto2()
}

def static "workstation.Workstation.saveCodReclamo"() {
    (new workstation.Workstation()).saveCodReclamo()
}

def static "workstation.Workstation.focusNavigator"() {
    (new workstation.Workstation()).focusNavigator()
}

def static "cromo.Page_Login.ingresaUsuario"(
    	Object usuario	) {
    (new cromo.Page_Login()).ingresaUsuario(
        	usuario)
}

def static "cromo.Page_Login.ingresaPassword"(
    	Object password	) {
    (new cromo.Page_Login()).ingresaPassword(
        	password)
}

def static "cromo.Page_Login.clickenIngresar"() {
    (new cromo.Page_Login()).clickenIngresar()
}

def static "cromo.Page_Login.login"(
    	String usuario	
     , 	String password	) {
    (new cromo.Page_Login()).login(
        	usuario
         , 	password)
}

def static "oms.TAB_Afectacion.inicioAfectacion"(
    	String tipoAfectacion	) {
    (new oms.TAB_Afectacion()).inicioAfectacion(
        	tipoAfectacion)
}

def static "oms.TAB_Afectacion.finalizarAfectacion"() {
    (new oms.TAB_Afectacion()).finalizarAfectacion()
}

def static "nexus.Script705.caso1_GenerarReclamo"(
    	int cantidadReclamos	) {
    (new nexus.Script705()).caso1_GenerarReclamo(
        	cantidadReclamos)
}

def static "nexus.Script705.caso2_VerificarTipoDeDocumento"(
    	int cantidadReclamos	) {
    (new nexus.Script705()).caso2_VerificarTipoDeDocumento(
        	cantidadReclamos)
}

def static "oms.Panel_AnomaliasDetectadas.clickNuevaAnomalia"() {
    (new oms.Panel_AnomaliasDetectadas()).clickNuevaAnomalia()
}

def static "helper.CodReclamos.saveData"() {
    (new helper.CodReclamos()).saveData()
}

def static "helper.CodReclamos.saveTextData"() {
    (new helper.CodReclamos()).saveTextData()
}

def static "helper.CodReclamos.getTextData"() {
    (new helper.CodReclamos()).getTextData()
}

def static "helper.CodReclamos.quit"() {
    (new helper.CodReclamos()).quit()
}

def static "helper.CodReclamos.getData"() {
    (new helper.CodReclamos()).getData()
}

def static "oms.Panel_De_Navegacion.clickHeaderPanelDeNavegacion"() {
    (new oms.Panel_De_Navegacion()).clickHeaderPanelDeNavegacion()
}

def static "oms.Panel_De_Navegacion.inputReclamoId"(
    	String codigoReclamo	) {
    (new oms.Panel_De_Navegacion()).inputReclamoId(
        	codigoReclamo)
}

def static "oms.Panel_De_Navegacion.seleccionarAnomalias"() {
    (new oms.Panel_De_Navegacion()).seleccionarAnomalias()
}

def static "oms.Panel_De_Navegacion.clickAnomaliasDetectadas"() {
    (new oms.Panel_De_Navegacion()).clickAnomaliasDetectadas()
}

def static "oms.Panel_De_Navegacion.clickAbiertosForzadosBT"() {
    (new oms.Panel_De_Navegacion()).clickAbiertosForzadosBT()
}

def static "helper.ScreenDriver.quit"() {
    (new helper.ScreenDriver()).quit()
}

def static "helper.WebUIContext.startCromo"() {
    (new helper.WebUIContext()).startCromo()
}

def static "helper.WebUIContext.startGI"() {
    (new helper.WebUIContext()).startGI()
}

def static "helper.WebUIContext.startCromo"(
    	Object ambiente	) {
    (new helper.WebUIContext()).startCromo(
        	ambiente)
}

def static "helper.WebUIContext.startSituacionMonitor"() {
    (new helper.WebUIContext()).startSituacionMonitor()
}

def static "callCenter.ProcesoGenerarReclamo.finalizarGeneracionReclamo"() {
    (new callCenter.ProcesoGenerarReclamo()).finalizarGeneracionReclamo()
}

def static "oms.Window_EditarAnomalia.ingresarElemento"(
    	Object elemento	) {
    (new oms.Window_EditarAnomalia()).ingresarElemento(
        	elemento)
}

def static "oms.Window_EditarAnomalia.seleccionarinstalacion"(
    	Object index	) {
    (new oms.Window_EditarAnomalia()).seleccionarinstalacion(
        	index)
}

def static "oms.Window_EditarAnomalia.ingresarInstalacion"(
    	Object instalacion	) {
    (new oms.Window_EditarAnomalia()).ingresarInstalacion(
        	instalacion)
}

def static "oms.Window_EditarAnomalia.seleccionarCausa"(
    	Object index	) {
    (new oms.Window_EditarAnomalia()).seleccionarCausa(
        	index)
}

def static "oms.Window_EditarAnomalia.seleccionarPrioridad"(
    	Object index	) {
    (new oms.Window_EditarAnomalia()).seleccionarPrioridad(
        	index)
}

def static "oms.Window_EditarAnomalia.ingresarPrioridad"(
    	Object prioridad	) {
    (new oms.Window_EditarAnomalia()).ingresarPrioridad(
        	prioridad)
}

def static "oms.Window_EditarAnomalia.clickAceptar"() {
    (new oms.Window_EditarAnomalia()).clickAceptar()
}

def static "oms.Window_EditarAnomalia.clickBotonVerde"() {
    (new oms.Window_EditarAnomalia()).clickBotonVerde()
}

def static "oms.Window_EditarAnomalia.seleccionarClasificacion"(
    	Object index	) {
    (new oms.Window_EditarAnomalia()).seleccionarClasificacion(
        	index)
}

def static "oms.Window_EditarAnomalia.ingresarClasificacion"(
    	Object clasificacion	) {
    (new oms.Window_EditarAnomalia()).ingresarClasificacion(
        	clasificacion)
}

def static "oms.Window_EditarAnomalia.seleccionarTipoDeAnomalia"(
    	Object index	) {
    (new oms.Window_EditarAnomalia()).seleccionarTipoDeAnomalia(
        	index)
}

def static "oms.Window_EditarAnomalia.ingresarTipoDeAnomalia"(
    	Object tipoAnomalia	) {
    (new oms.Window_EditarAnomalia()).ingresarTipoDeAnomalia(
        	tipoAnomalia)
}

def static "oms.Window_EditarAnomalia.seleccionarCalle"(
    	Object index	) {
    (new oms.Window_EditarAnomalia()).seleccionarCalle(
        	index)
}

def static "oms.Window_EditarAnomalia.ingresarCalle"(
    	Object calle	) {
    (new oms.Window_EditarAnomalia()).ingresarCalle(
        	calle)
}

def static "oms.Window_EditarAnomalia.seleccionarEntreCalle1"(
    	Object index	) {
    (new oms.Window_EditarAnomalia()).seleccionarEntreCalle1(
        	index)
}

def static "oms.Window_EditarAnomalia.ingresarEntreCalle1"(
    	Object entreCalle1	) {
    (new oms.Window_EditarAnomalia()).ingresarEntreCalle1(
        	entreCalle1)
}

def static "oms.Window_EditarAnomalia.ingresarAnomalia"(
    	Object calle	
     , 	Object entreCalle1	
     , 	Object instalacion	
     , 	Object tipoAnomalia	
     , 	Object clasificacion	
     , 	Object prioridad	) {
    (new oms.Window_EditarAnomalia()).ingresarAnomalia(
        	calle
         , 	entreCalle1
         , 	instalacion
         , 	tipoAnomalia
         , 	clasificacion
         , 	prioridad)
}

def static "oms.Window_EditarAnomalia.seleccionarTipoDered"(
    	Object index	) {
    (new oms.Window_EditarAnomalia()).seleccionarTipoDered(
        	index)
}

def static "oms.Window_EditarAnomalia.seleccionarAreaOperativa"(
    	Object index	) {
    (new oms.Window_EditarAnomalia()).seleccionarAreaOperativa(
        	index)
}

def static "oms.Window_EditarAnomalia.seleccionarPartido"(
    	Object index	) {
    (new oms.Window_EditarAnomalia()).seleccionarPartido(
        	index)
}

def static "oms.Window_EditarAnomalia.seleccionarLocalidad"(
    	Object index	) {
    (new oms.Window_EditarAnomalia()).seleccionarLocalidad(
        	index)
}

def static "oms.Window_EditarAnomalia.getNumeroOT"() {
    (new oms.Window_EditarAnomalia()).getNumeroOT()
}

def static "oms.Window_EditarAnomalia.verificacionNumeroOT"() {
    (new oms.Window_EditarAnomalia()).verificacionNumeroOT()
}

def static "oms.Window_Despacho.clickConfirmarDespacho"() {
    (new oms.Window_Despacho()).clickConfirmarDespacho()
}

def static "callCenter.dialog_noseencontrocliente.clickSi"() {
    (new callCenter.dialog_noseencontrocliente()).clickSi()
}

def static "callCenter.dialog_noseencontrocliente.noSeEncontroClienteAparece"() {
    (new callCenter.dialog_noseencontrocliente()).noSeEncontroClienteAparece()
}

def static "oms.ProcesoCambioTipoRegistracion.cambiarTipoAPostOperacion"() {
    (new oms.ProcesoCambioTipoRegistracion()).cambiarTipoAPostOperacion()
}

def static "oms.Window_AgregarActividad.ingresarCodigo"() {
    (new oms.Window_AgregarActividad()).ingresarCodigo()
}

def static "oms.Window_AgregarActividad.aceptarActividad"() {
    (new oms.Window_AgregarActividad()).aceptarActividad()
}

def static "gi.Page_Login.ingresaUsuario"(
    	Object usuario	) {
    (new gi.Page_Login()).ingresaUsuario(
        	usuario)
}

def static "gi.Page_Login.ingresaPassword"(
    	Object password	) {
    (new gi.Page_Login()).ingresaPassword(
        	password)
}

def static "gi.Page_Login.clickenIngresar"() {
    (new gi.Page_Login()).clickenIngresar()
}

def static "oms.Window_Documento_Tab_Reclamos.clickCerrar"() {
    (new oms.Window_Documento_Tab_Reclamos()).clickCerrar()
}

def static "oms.Window_Documento_Tab_Reclamos.seleccionarPrimerDocumento"() {
    (new oms.Window_Documento_Tab_Reclamos()).seleccionarPrimerDocumento()
}

def static "oms.Window_Documento_Tab_Reclamos.seleccionarReclamo"(
    	String numeroReclamo	) {
    (new oms.Window_Documento_Tab_Reclamos()).seleccionarReclamo(
        	numeroReclamo)
}

def static "oms.Window_Documento_Tab_Reclamos.esperarReclamoCerrado"() {
    (new oms.Window_Documento_Tab_Reclamos()).esperarReclamoCerrado()
}

def static "oms.Window_Documento_Tab_Reclamos.clickDetalles"() {
    (new oms.Window_Documento_Tab_Reclamos()).clickDetalles()
}

def static "oms.Window_Documento_Tab_Reclamos.seleccionarDocumento"(
    	Object index	) {
    (new oms.Window_Documento_Tab_Reclamos()).seleccionarDocumento(
        	index)
}

def static "oms.Window_Documento_Tab_Reclamos.verificarClienteSensible"() {
    (new oms.Window_Documento_Tab_Reclamos()).verificarClienteSensible()
}

def static "oms.Window_Documento_Tab_Reclamos.clickMoverReclamos"() {
    (new oms.Window_Documento_Tab_Reclamos()).clickMoverReclamos()
}

def static "oms.SeleccioneUnEquipoMovil.clickViaDeComunicacion"() {
    (new oms.SeleccioneUnEquipoMovil()).clickViaDeComunicacion()
}

def static "oms.SeleccioneUnEquipoMovil.seleccionarElementosMostrados"(
    	Object index	) {
    (new oms.SeleccioneUnEquipoMovil()).seleccionarElementosMostrados(
        	index)
}

def static "oms.SeleccioneUnEquipoMovil.seleccionarEquipoMovilClick"() {
    (new oms.SeleccioneUnEquipoMovil()).seleccionarEquipoMovilClick()
}

def static "oms.SeleccioneUnEquipoMovil.aceptarEquipoMovil"() {
    (new oms.SeleccioneUnEquipoMovil()).aceptarEquipoMovil()
}

def static "oms.SeleccioneUnEquipoMovil.seleccionarEquipoMovilClick"(
    	Object index	) {
    (new oms.SeleccioneUnEquipoMovil()).seleccionarEquipoMovilClick(
        	index)
}

def static "oms.SeleccioneUnEquipoMovil.clickAceptar"() {
    (new oms.SeleccioneUnEquipoMovil()).clickAceptar()
}

def static "oms.SeleccioneUnEquipoMovil.filtrarPorCodigo"(
    	String codigo	) {
    (new oms.SeleccioneUnEquipoMovil()).filtrarPorCodigo(
        	codigo)
}

def static "oms.SeleccioneUnEquipoMovil.clickFiltrar"() {
    (new oms.SeleccioneUnEquipoMovil()).clickFiltrar()
}

def static "oms.SeleccioneUnEquipoMovil.seleccionarFiltrado"() {
    (new oms.SeleccioneUnEquipoMovil()).seleccionarFiltrado()
}

def static "oms.SeleccioneUnEquipoMovil.seleccionarElementoPorCodigo"(
    	String codigo	) {
    (new oms.SeleccioneUnEquipoMovil()).seleccionarElementoPorCodigo(
        	codigo)
}

def static "oms.SeleccioneUnEquipoMovil.seleccionarElementoPorIndex"(
    	int index	) {
    (new oms.SeleccioneUnEquipoMovil()).seleccionarElementoPorIndex(
        	index)
}

def static "oms.Main.focus"() {
    (new oms.Main()).focus()
}

def static "oms.Main.clickTitle"() {
    (new oms.Main()).clickTitle()
}

def static "oms.Main.verificarInicioDeOMS"() {
    (new oms.Main()).verificarInicioDeOMS()
}

def static "oms.Main.clickHerramientas"() {
    (new oms.Main()).clickHerramientas()
}

def static "oms.Main.openSituacionMonitor"() {
    (new oms.Main()).openSituacionMonitor()
}

def static "oms.Main.comprimirPanel"() {
    (new oms.Main()).comprimirPanel()
}

def static "oms.Main.clickMenuNuevoDocumento"() {
    (new oms.Main()).clickMenuNuevoDocumento()
}

def static "oms.Main.clickAbiertosPrgBT"() {
    (new oms.Main()).clickAbiertosPrgBT()
}

def static "oms.Main.clickColumnaNumero"() {
    (new oms.Main()).clickColumnaNumero()
}

def static "oms.Main.selecionarPrimerDocumento"() {
    (new oms.Main()).selecionarPrimerDocumento()
}

def static "oms.Main.clickVerDetalles"() {
    (new oms.Main()).clickVerDetalles()
}

def static "oms.Main.checkCodigoReclamo"(
    	String reclamo	) {
    (new oms.Main()).checkCodigoReclamo(
        	reclamo)
}

def static "oms.Main.selecionarUltimoDocumento"() {
    (new oms.Main()).selecionarUltimoDocumento()
}

def static "oms.Main.buscarDocumento"(
    	Object doc	) {
    (new oms.Main()).buscarDocumento(
        	doc)
}

def static "oms.Main.clickDocumentosProgramables"() {
    (new oms.Main()).clickDocumentosProgramables()
}

def static "oms.Main.clickDocumentosProgramables2Pendientes"() {
    (new oms.Main()).clickDocumentosProgramables2Pendientes()
}

def static "oms.Main.buscarReclamo"(
    	Object rec	) {
    (new oms.Main()).buscarReclamo(
        	rec)
}

def static "oms.Main.buscarDocumentoGuardado"(
    	Object path	
     , 	Object docSheet	
     , 	Object row	
     , 	Object column	) {
    (new oms.Main()).buscarDocumentoGuardado(
        	path
         , 	docSheet
         , 	row
         , 	column)
}

def static "oms.Main.buscarReclamo"(
    	Object path	
     , 	Object docSheet	
     , 	Object row	
     , 	Object column	) {
    (new oms.Main()).buscarReclamo(
        	path
         , 	docSheet
         , 	row
         , 	column)
}

def static "situacionMonitor.Main.verificacionVentanaAbierta"() {
    (new situacionMonitor.Main()).verificacionVentanaAbierta()
}

def static "situacionMonitor.Main.verificarReclamo"() {
    (new situacionMonitor.Main()).verificarReclamo()
}

def static "situacionMonitor.Main.clickLlamadasSalientes"() {
    (new situacionMonitor.Main()).clickLlamadasSalientes()
}

def static "situacionMonitor.Main.getNewBTDocuments"() {
    (new situacionMonitor.Main()).getNewBTDocuments()
}

def static "situacionMonitor.Main.getAfectadosActual"() {
    (new situacionMonitor.Main()).getAfectadosActual()
}

def static "situacionMonitor.Main.getAfectadosTotal"() {
    (new situacionMonitor.Main()).getAfectadosTotal()
}

def static "situacionMonitor.Main.verificarClientesAfectados"() {
    (new situacionMonitor.Main()).verificarClientesAfectados()
}

def static "situacionMonitor.Main.verificarClientesRestaurados"() {
    (new situacionMonitor.Main()).verificarClientesRestaurados()
}

def static "situacionMonitor.Main.guardarAfectadosActuales"() {
    (new situacionMonitor.Main()).guardarAfectadosActuales()
}

def static "situacionMonitor.Main.focus"() {
    (new situacionMonitor.Main()).focus()
}

def static "situacionMonitor.Main.getTime"() {
    (new situacionMonitor.Main()).getTime()
}

def static "situacionMonitor.Main.copyData"() {
    (new situacionMonitor.Main()).copyData()
}

def static "situacionMonitor.Main.waitForRegionRefresh"(
    	String filename	) {
    (new situacionMonitor.Main()).waitForRegionRefresh(
        	filename)
}

def static "situacionMonitor.Main.checkPattern"() {
    (new situacionMonitor.Main()).checkPattern()
}

def static "situacionMonitor.Main.refreshPage"() {
    (new situacionMonitor.Main()).refreshPage()
}

def static "oms.Window_Documento_Header.checkTipoForzado"(
    	String tipoForzado	
     , 	String iteracion	) {
    (new oms.Window_Documento_Header()).checkTipoForzado(
        	tipoForzado
         , 	iteracion)
}

def static "cromo.Page_Clientes.guardarClientes"() {
    (new cromo.Page_Clientes()).guardarClientes()
}

def static "cromo.Page_Clientes.guardarClientes"(
    	Object path	
     , 	Object sheet	) {
    (new cromo.Page_Clientes()).guardarClientes(
        	path
         , 	sheet)
}

def static "cromo.Page_Clientes.guardarClientesAfectados"(
    	Object path	
     , 	Object sheet	
     , 	Object column	) {
    (new cromo.Page_Clientes()).guardarClientesAfectados(
        	path
         , 	sheet
         , 	column)
}

def static "cromo.Page_Clientes.clickCerrarVentana"() {
    (new cromo.Page_Clientes()).clickCerrarVentana()
}

def static "sikuli.Utils.Esperar"(
    	Screen s	
     , 	String imagen	) {
    (new sikuli.Utils()).Esperar(
        	s
         , 	imagen)
}

def static "sikuli.Utils.SeleccionarTodo"(
    	Screen s	
     , 	String imagen	) {
    (new sikuli.Utils()).SeleccionarTodo(
        	s
         , 	imagen)
}

def static "sikuli.Utils.GetData"() {
    (new sikuli.Utils()).GetData()
}

def static "helper.ConfigHelper.crearCarpetaTest"(
    	String path	) {
    (new helper.ConfigHelper()).crearCarpetaTest(
        	path)
}

def static "helper.ConfigHelper.incrementarContador"() {
    (new helper.ConfigHelper()).incrementarContador()
}

def static "oms.Window_ModoRegistracion.modificarModoRegistracion"(
    	String modoRegistracion	) {
    (new oms.Window_ModoRegistracion()).modificarModoRegistracion(
        	modoRegistracion)
}

def static "oms.Window_ModoRegistracion.clickButtonOk"() {
    (new oms.Window_ModoRegistracion()).clickButtonOk()
}

def static "oms.Window_SeleccioneUnEquipoMovil.seleccionar"(
    	String codigo	) {
    (new oms.Window_SeleccioneUnEquipoMovil()).seleccionar(
        	codigo)
}

def static "oms.Window_SeleccioneUnEquipoMovil.aceptarSeleccion"() {
    (new oms.Window_SeleccioneUnEquipoMovil()).aceptarSeleccion()
}

def static "oms.Window_SeleccioneUnEquipoMovil.clicklista"() {
    (new oms.Window_SeleccioneUnEquipoMovil()).clicklista()
}

def static "oms.Window_NuevoDocumento.fechaDeCorteActualMasMinutos"(
    	Object minutes	) {
    (new oms.Window_NuevoDocumento()).fechaDeCorteActualMasMinutos(
        	minutes)
}

def static "oms.Window_NuevoDocumento.tiempoDeRestauracion"() {
    (new oms.Window_NuevoDocumento()).tiempoDeRestauracion()
}

def static "oms.Window_NuevoDocumento.ingresarCoordenadas"(
    	Object coordenadas	) {
    (new oms.Window_NuevoDocumento()).ingresarCoordenadas(
        	coordenadas)
}

def static "oms.Window_NuevoDocumento.seleccionarMotivo"() {
    (new oms.Window_NuevoDocumento()).seleccionarMotivo()
}

def static "oms.Window_NuevoDocumento.clickAceptar"() {
    (new oms.Window_NuevoDocumento()).clickAceptar()
}

def static "oms.Window_NuevoDocumento.generarNuevoDocumento"(
    	String tipoDocumento	
     , 	String minutos	
     , 	String coordenadas	) {
    (new oms.Window_NuevoDocumento()).generarNuevoDocumento(
        	tipoDocumento
         , 	minutos
         , 	coordenadas)
}

def static "oms.Window_NuevoDocumento.generarNuevoDocumento"(
    	String tipoDocumento	
     , 	String coordenadas	) {
    (new oms.Window_NuevoDocumento()).generarNuevoDocumento(
        	tipoDocumento
         , 	coordenadas)
}

def static "oms.Window_NuevoDocumento.ingresarTipoDocumento"(
    	String tipoDocumento	) {
    (new oms.Window_NuevoDocumento()).ingresarTipoDocumento(
        	tipoDocumento)
}

def static "oms.Window_NuevoDocumento.seleccionarCoordenadas"() {
    (new oms.Window_NuevoDocumento()).seleccionarCoordenadas()
}

def static "nexus.Script705_ExcelData.getTiposReclamo"(
    	int cantidadReclamos	) {
    (new nexus.Script705_ExcelData()).getTiposReclamo(
        	cantidadReclamos)
}

def static "nexus.Script705_ExcelData.getCodReclamosAndTipoDocumento"(
    	int cantidadReclamos	) {
    (new nexus.Script705_ExcelData()).getCodReclamosAndTipoDocumento(
        	cantidadReclamos)
}

def static "nexus.Script705_ExcelData.getAllData"(
    	int cantidadReclamos	) {
    (new nexus.Script705_ExcelData()).getAllData(
        	cantidadReclamos)
}

def static "nexus.Script705_ExcelData.saveCodReclamo"(
    	int i	) {
    (new nexus.Script705_ExcelData()).saveCodReclamo(
        	i)
}

def static "nexus.Script705_ExcelData.getCodigoReclamo"(
    	int i	) {
    (new nexus.Script705_ExcelData()).getCodigoReclamo(
        	i)
}

def static "nexus.Script705_ExcelData.saveTipoDocumentoObtenido"(
    	int i	
     , 	String tipoDocumentoObtenido	) {
    (new nexus.Script705_ExcelData()).saveTipoDocumentoObtenido(
        	i
         , 	tipoDocumentoObtenido)
}

def static "oms.Window_Documento_Tab_Anomalias.clickNuevaAnomalia"() {
    (new oms.Window_Documento_Tab_Anomalias()).clickNuevaAnomalia()
}

def static "oms.Window_Documento_Tab_Anomalias.clickDesvincular"() {
    (new oms.Window_Documento_Tab_Anomalias()).clickDesvincular()
}

def static "oms.Window_Documento_Tab_Anomalias.clickAceptar"() {
    (new oms.Window_Documento_Tab_Anomalias()).clickAceptar()
}

def static "oms.Window_Documento_Tab_Anomalias.clickVerDetalle"() {
    (new oms.Window_Documento_Tab_Anomalias()).clickVerDetalle()
}

def static "oms.Window_Documento_Tab_Anomalias.seleccionarAnomalia"(
    	int indexAnomalia	) {
    (new oms.Window_Documento_Tab_Anomalias()).seleccionarAnomalia(
        	indexAnomalia)
}

def static "oms.ProcesoCreacionAnomalia.inicio"() {
    (new oms.ProcesoCreacionAnomalia()).inicio()
}

def static "oms.ProcesoCreacionAnomalia.seleccionAnomalia"(
    	String localidad	
     , 	String suministro	) {
    (new oms.ProcesoCreacionAnomalia()).seleccionAnomalia(
        	localidad
         , 	suministro)
}

def static "oms.ProcesoCreacionAnomalia.seleccionAnomalia"(
    	String zona	
     , 	String partido	
     , 	String localidad	
     , 	String idElemento	) {
    (new oms.ProcesoCreacionAnomalia()).seleccionAnomalia(
        	zona
         , 	partido
         , 	localidad
         , 	idElemento)
}

def static "oms.ProcesoCreacionAnomalia.ingresarDatos"(
    	Object calle	
     , 	Object entreCalle1	
     , 	Object instalacion	
     , 	Object tipoAnomalia	
     , 	Object clasificacion	
     , 	Object prioridad	) {
    (new oms.ProcesoCreacionAnomalia()).ingresarDatos(
        	calle
         , 	entreCalle1
         , 	instalacion
         , 	tipoAnomalia
         , 	clasificacion
         , 	prioridad)
}

def static "callCenter.ExcelData.saveCodigoReclamo"(
    	int fila	) {
    (new callCenter.ExcelData()).saveCodigoReclamo(
        	fila)
}

def static "callCenter.ExcelData.saveCodigoReclamo"(
    	String codigoReclamo	
     , 	int fila	) {
    (new callCenter.ExcelData()).saveCodigoReclamo(
        	codigoReclamo
         , 	fila)
}

def static "callCenter.ExcelData.getCodigoReclamo"(
    	int i	) {
    (new callCenter.ExcelData()).getCodigoReclamo(
        	i)
}

def static "callCenter.ExcelData.saveTipoDocumentoObtenido"(
    	int i	
     , 	String tipoDocumentoObtenido	) {
    (new callCenter.ExcelData()).saveTipoDocumentoObtenido(
        	i
         , 	tipoDocumentoObtenido)
}

def static "nexus.CircuitoCritico1.paso1AbrirAplicacionesNexus"(
    	String usuario	
     , 	String password	
     , 	String ambiente	) {
    (new nexus.CircuitoCritico1()).paso1AbrirAplicacionesNexus(
        	usuario
         , 	password
         , 	ambiente)
}

def static "nexus.CircuitoCritico1.paso2CreacionDocumentoProgramadoBT"(
    	String tipoDocumento	
     , 	String minutos	
     , 	String coordenadas	) {
    (new nexus.CircuitoCritico1()).paso2CreacionDocumentoProgramadoBT(
        	tipoDocumento
         , 	minutos
         , 	coordenadas)
}

def static "nexus.CircuitoCritico1.paso3AfectacionDocumentoProgramadoBT"(
    	String tipoAfectacion	
     , 	String idLocalidad	
     , 	String idSuministro	) {
    (new nexus.CircuitoCritico1()).paso3AfectacionDocumentoProgramadoBT(
        	tipoAfectacion
         , 	idLocalidad
         , 	idSuministro)
}

def static "nexus.CircuitoCritico1.paso4DespachoMovilDocumentoProgramado"(
    	String equipoMovil	
     , 	String horasRestauracion	) {
    (new nexus.CircuitoCritico1()).paso4DespachoMovilDocumentoProgramado(
        	equipoMovil
         , 	horasRestauracion)
}

def static "nexus.CircuitoCritico1.paso5AltaDeReclamosAsociarADocProgBT"() {
    (new nexus.CircuitoCritico1()).paso5AltaDeReclamosAsociarADocProgBT()
}

def static "nexus.CircuitoCritico1.paso6VerificacionDeDocumentosSituacionMonitor"() {
    (new nexus.CircuitoCritico1()).paso6VerificacionDeDocumentosSituacionMonitor()
}

def static "nexus.CircuitoCritico1.paso7LlamadasSalientesSituacionMonitor"(
    	String ambiente	) {
    (new nexus.CircuitoCritico1()).paso7LlamadasSalientesSituacionMonitor(
        	ambiente)
}

def static "nexus.CircuitoCritico1.paso8RestauracionDocumentoProgramadoBT"(
    	String usuario	
     , 	String password	
     , 	String ambiente	
     , 	String idSuministro	) {
    (new nexus.CircuitoCritico1()).paso8RestauracionDocumentoProgramadoBT(
        	usuario
         , 	password
         , 	ambiente
         , 	idSuministro)
}

def static "nexus.CircuitoCritico1.paso9VerificarClientesRestaurados"() {
    (new nexus.CircuitoCritico1()).paso9VerificarClientesRestaurados()
}

def static "nexus.CircuitoCritico1.paso10CerrarReclamosDocProgramadoBT"() {
    (new nexus.CircuitoCritico1()).paso10CerrarReclamosDocProgramadoBT()
}

def static "nexus.CircuitoCritico1.paso11ManiobraAperturaDocumento"(
    	String usuario	
     , 	String password	
     , 	String ambiente	) {
    (new nexus.CircuitoCritico1()).paso11ManiobraAperturaDocumento(
        	usuario
         , 	password
         , 	ambiente)
}

def static "nexus.CircuitoCritico1.paso12DarDeAltaReclamosClienteSensibles"() {
    (new nexus.CircuitoCritico1()).paso12DarDeAltaReclamosClienteSensibles()
}

def static "nexus.CircuitoCritico1.paso13ManiobraCierreDocumento"() {
    (new nexus.CircuitoCritico1()).paso13ManiobraCierreDocumento()
}

def static "nexus.CircuitoCritico1.paso14CierreProvisorioDeDocumento"() {
    (new nexus.CircuitoCritico1()).paso14CierreProvisorioDeDocumento()
}

def static "nexus.CircuitoCritico1.paso15CambioTipoRegistracion"(
    	String ambiente	) {
    (new nexus.CircuitoCritico1()).paso15CambioTipoRegistracion(
        	ambiente)
}

def static "nexus.CircuitoCritico1.paso16CierreDeDocumentoPostOperacion"() {
    (new nexus.CircuitoCritico1()).paso16CierreDeDocumentoPostOperacion()
}

def static "nexus.CircuitoCritico1.resultadoEsperado"() {
    (new nexus.CircuitoCritico1()).resultadoEsperado()
}

def static "nexus.CircuitoCritico1.paso1B_guardarAfectadosActuales"() {
    (new nexus.CircuitoCritico1()).paso1B_guardarAfectadosActuales()
}

def static "oms.Window_RecuperacionDeReclamos.restarMinutos"(
    	Object min	) {
    (new oms.Window_RecuperacionDeReclamos()).restarMinutos(
        	min)
}

def static "oms.Window_RecuperacionDeReclamos.clickAceptar"() {
    (new oms.Window_RecuperacionDeReclamos()).clickAceptar()
}

def static "oms.Window_RecuperacionDeReclamos.aceptarPopUp"() {
    (new oms.Window_RecuperacionDeReclamos()).aceptarPopUp()
}

def static "gi.Page_Main.verificarGIInicio"() {
    (new gi.Page_Main()).verificarGIInicio()
}

def static "nexus.Main.inicioWorkstation"(
    	String usuario	
     , 	String password	
     , 	String ambiente	) {
    (new nexus.Main()).inicioWorkstation(
        	usuario
         , 	password
         , 	ambiente)
}

def static "nexus.Main.inicioCallCenter"(
    	String usuario	
     , 	String password	
     , 	String ambiente	) {
    (new nexus.Main()).inicioCallCenter(
        	usuario
         , 	password
         , 	ambiente)
}

def static "nexus.Main.inicioCromo"(
    	String usuario	
     , 	String password	
     , 	String ambiente	) {
    (new nexus.Main()).inicioCromo(
        	usuario
         , 	password
         , 	ambiente)
}

def static "nexus.Main.inicioOMS"(
    	String usuario	
     , 	String password	
     , 	String ambiente	) {
    (new nexus.Main()).inicioOMS(
        	usuario
         , 	password
         , 	ambiente)
}

def static "nexus.Main.inicioSituacionMonitor"(
    	String usuario	
     , 	String password	
     , 	String ambiente	) {
    (new nexus.Main()).inicioSituacionMonitor(
        	usuario
         , 	password
         , 	ambiente)
}

def static "nexus.Main.abrirOMS"() {
    (new nexus.Main()).abrirOMS()
}

def static "nexus.Main.abrirSituacionMonitor"() {
    (new nexus.Main()).abrirSituacionMonitor()
}

def static "nexus.Main.abrirCallCenter"() {
    (new nexus.Main()).abrirCallCenter()
}

def static "nexus.Main.abrirAplicacionesNexus"(
    	String usuario	
     , 	String password	
     , 	String ambiente	) {
    (new nexus.Main()).abrirAplicacionesNexus(
        	usuario
         , 	password
         , 	ambiente)
}

def static "nexus.Main.abrirOMSConCromo"(
    	String usuario	
     , 	String password	
     , 	String ambiente	) {
    (new nexus.Main()).abrirOMSConCromo(
        	usuario
         , 	password
         , 	ambiente)
}

def static "oms.PopUp_DocumentoCreado.verificarDocumentoCreado"() {
    (new oms.PopUp_DocumentoCreado()).verificarDocumentoCreado()
}

def static "oms.PopUp_DocumentoCreado.clickAbrir"() {
    (new oms.PopUp_DocumentoCreado()).clickAbrir()
}

def static "oms.PopUp_DocumentoCreado.clickCerrar"() {
    (new oms.PopUp_DocumentoCreado()).clickCerrar()
}

def static "oms.PopUp_DocumentoCreado.verificarAnomaliaCreada"() {
    (new oms.PopUp_DocumentoCreado()).verificarAnomaliaCreada()
}

def static "oms.Window_Documento_Tab_EquiposMoviles.clickDespachado"() {
    (new oms.Window_Documento_Tab_EquiposMoviles()).clickDespachado()
}

def static "oms.Window_Documento_Tab_EquiposMoviles.clickCancelar"() {
    (new oms.Window_Documento_Tab_EquiposMoviles()).clickCancelar()
}

def static "oms.Window_Documento_Tab_EquiposMoviles.aceptar"() {
    (new oms.Window_Documento_Tab_EquiposMoviles()).aceptar()
}

def static "oms.Window_Documento_Tab_EquiposMoviles.verificarCancelado"() {
    (new oms.Window_Documento_Tab_EquiposMoviles()).verificarCancelado()
}

def static "oms.Window_Documento_Tab_EquiposMoviles.clickSi"() {
    (new oms.Window_Documento_Tab_EquiposMoviles()).clickSi()
}

def static "oms.Window_Documento_Tab_EquiposMoviles.clickDespachar"() {
    (new oms.Window_Documento_Tab_EquiposMoviles()).clickDespachar()
}

def static "oms.Window_Documento_Tab_EquiposMoviles.verificarMultipleDespacho"() {
    (new oms.Window_Documento_Tab_EquiposMoviles()).verificarMultipleDespacho()
}

def static "oms.Window_Documento_Tab_EquiposMoviles.verificarDespacho"() {
    (new oms.Window_Documento_Tab_EquiposMoviles()).verificarDespacho()
}

def static "oms.Window_Regiones.guardarRegionesSeleccionadas"() {
    (new oms.Window_Regiones()).guardarRegionesSeleccionadas()
}

def static "oms.Window_Regiones.verificarTiempoDeInicioOMS"() {
    (new oms.Window_Regiones()).verificarTiempoDeInicioOMS()
}

def static "oms.Window_OperarElemento.seleccionarManiobra"() {
    (new oms.Window_OperarElemento()).seleccionarManiobra()
}

def static "oms.Window_OperarElemento.seleccionarAccionAbrir"() {
    (new oms.Window_OperarElemento()).seleccionarAccionAbrir()
}

def static "oms.Window_OperarElemento.seleccionarAccionCerrar"() {
    (new oms.Window_OperarElemento()).seleccionarAccionCerrar()
}

def static "oms.Window_OperarElemento.setFechaHora"() {
    (new oms.Window_OperarElemento()).setFechaHora()
}

def static "oms.Window_OperarElemento.getTime"() {
    (new oms.Window_OperarElemento()).getTime()
}

def static "callCenter.Window_ReclamosActivosDelCliente.clickReiterar"() {
    (new callCenter.Window_ReclamosActivosDelCliente()).clickReiterar()
}

def static "callCenter.Window_ReclamosActivosDelCliente.clickCancelar"() {
    (new callCenter.Window_ReclamosActivosDelCliente()).clickCancelar()
}

def static "callCenter.Window_ReclamosActivosDelCliente.clickNuevo1"() {
    (new callCenter.Window_ReclamosActivosDelCliente()).clickNuevo1()
}

def static "callCenter.Window_ReclamosActivosDelCliente.clickNuevo2"() {
    (new callCenter.Window_ReclamosActivosDelCliente()).clickNuevo2()
}

def static "callCenter.Window_ReclamosActivosDelCliente.verificarReiteracionDisponible"() {
    (new callCenter.Window_ReclamosActivosDelCliente()).verificarReiteracionDisponible()
}

def static "callCenter.Window_ReclamosActivosDelCliente.clickImprimir"() {
    (new callCenter.Window_ReclamosActivosDelCliente()).clickImprimir()
}

def static "callCenter.Window_ReclamosActivosDelCliente.clickDetaDatCliente"() {
    (new callCenter.Window_ReclamosActivosDelCliente()).clickDetaDatCliente()
}

def static "callCenter.Window_ReclamosActivosDelCliente.clickOtrosTelef"() {
    (new callCenter.Window_ReclamosActivosDelCliente()).clickOtrosTelef()
}

def static "callCenter.Window_ReclamosActivosDelCliente.clickCerrarTelef"() {
    (new callCenter.Window_ReclamosActivosDelCliente()).clickCerrarTelef()
}

def static "callCenter.Window_ReclamosActivosDelCliente.clickNuevoTelef"() {
    (new callCenter.Window_ReclamosActivosDelCliente()).clickNuevoTelef()
}

def static "callCenter.Window_ReclamosActivosDelCliente.clickTelAceptarError"() {
    (new callCenter.Window_ReclamosActivosDelCliente()).clickTelAceptarError()
}

def static "callCenter.Window_ReclamosActivosDelCliente.clickTelCerrarPreview"() {
    (new callCenter.Window_ReclamosActivosDelCliente()).clickTelCerrarPreview()
}

def static "callCenter.Window_ReclamosActivosDelCliente.clickSelTipoTel"() {
    (new callCenter.Window_ReclamosActivosDelCliente()).clickSelTipoTel()
}

def static "callCenter.Window_ReclamosActivosDelCliente.clickIngresoNroTel"() {
    (new callCenter.Window_ReclamosActivosDelCliente()).clickIngresoNroTel()
}

def static "callCenter.Window_ReclamosActivosDelCliente.clickCheckCtoTel"() {
    (new callCenter.Window_ReclamosActivosDelCliente()).clickCheckCtoTel()
}

def static "callCenter.Window_ReclamosActivosDelCliente.clickTelAcepta"() {
    (new callCenter.Window_ReclamosActivosDelCliente()).clickTelAcepta()
}

def static "callCenter.Window_ReclamosActivosDelCliente.clickTelAceptaMsg"() {
    (new callCenter.Window_ReclamosActivosDelCliente()).clickTelAceptaMsg()
}

def static "callCenter.Window_ReclamosActivosDelCliente.seleccionarTipoTel"(
    	Object index	) {
    (new callCenter.Window_ReclamosActivosDelCliente()).seleccionarTipoTel(
        	index)
}

def static "callCenter.Window_ReclamosActivosDelCliente.ingresarTelefono"(
    	String numeroTelefono	) {
    (new callCenter.Window_ReclamosActivosDelCliente()).ingresarTelefono(
        	numeroTelefono)
}

def static "callCenter.Window_ReclamosActivosDelCliente.randomNumber"(
    	int minimum	
     , 	int maximum	) {
    (new callCenter.Window_ReclamosActivosDelCliente()).randomNumber(
        	minimum
         , 	maximum)
}

def static "oms.Window_DespachoMovil.ingresarEquipoMovil"(
    	String equipoMovil	) {
    (new oms.Window_DespachoMovil()).ingresarEquipoMovil(
        	equipoMovil)
}

def static "oms.Window_DetalleDeReclamo.abrirDocumento"() {
    (new oms.Window_DetalleDeReclamo()).abrirDocumento()
}

def static "oms.Window_DetalleDeReclamo.cerrarVentana"() {
    (new oms.Window_DetalleDeReclamo()).cerrarVentana()
}

def static "oms.Window_DetalleDeReclamo.verificarCerradoDefinitivo"() {
    (new oms.Window_DetalleDeReclamo()).verificarCerradoDefinitivo()
}

def static "oms.Window_DetalleDeReclamo.verificarVentana"() {
    (new oms.Window_DetalleDeReclamo()).verificarVentana()
}

def static "oms.Window_DetalleDeReclamo.clickVerDocumento"() {
    (new oms.Window_DetalleDeReclamo()).clickVerDocumento()
}

def static "oms.Window_DetalleDeReclamo.verificarCliente"(
    	String path	
     , 	int sheet	
     , 	int row	
     , 	int column	) {
    (new oms.Window_DetalleDeReclamo()).verificarCliente(
        	path
         , 	sheet
         , 	row
         , 	column)
}

def static "oms.ProcesoCerrarReclamo.cerrarReclamo"(
    	String numeroReclamo	) {
    (new oms.ProcesoCerrarReclamo()).cerrarReclamo(
        	numeroReclamo)
}

def static "oms.ProcesoCerrarReclamo.verificarReclamoCerrado"(
    	String numeroReclamo	) {
    (new oms.ProcesoCerrarReclamo()).verificarReclamoCerrado(
        	numeroReclamo)
}

def static "callCenter.Window_DocumentoAlQueElReclamoPuedeSerAsociado.clickAsociar"() {
    (new callCenter.Window_DocumentoAlQueElReclamoPuedeSerAsociado()).clickAsociar()
}

def static "helper.ExcelData.getListaNumeroCuentas"(
    	String Circuito	) {
    (new helper.ExcelData()).getListaNumeroCuentas(
        	Circuito)
}

def static "helper.ExcelData.getNumeroDocumento"() {
    (new helper.ExcelData()).getNumeroDocumento()
}

def static "helper.ExcelData.testLeerCantidadAfectadosInicial"() {
    (new helper.ExcelData()).testLeerCantidadAfectadosInicial()
}

def static "helper.ExcelData.guardarCantidadDeClientes"(
    	java.util.List<org.openqa.selenium.WebElement> listaDeClientes	) {
    (new helper.ExcelData()).guardarCantidadDeClientes(
        	listaDeClientes)
}

def static "helper.ExcelData.guardarNumeroDeDocumento"(
    	String numeroDocumento	) {
    (new helper.ExcelData()).guardarNumeroDeDocumento(
        	numeroDocumento)
}

def static "helper.ExcelData.guardarCantidadAfectadosInicial"(
    	String cantidadAfectados	) {
    (new helper.ExcelData()).guardarCantidadAfectadosInicial(
        	cantidadAfectados)
}

def static "helper.ExcelData.guardarFechaHoraManiobraApertura"(
    	String fechaHora	) {
    (new helper.ExcelData()).guardarFechaHoraManiobraApertura(
        	fechaHora)
}

def static "helper.ExcelData.getListaNumeroCuentasSensibles"() {
    (new helper.ExcelData()).getListaNumeroCuentasSensibles()
}

def static "oms.Window_CerrarReclamos.clickAceptar"() {
    (new oms.Window_CerrarReclamos()).clickAceptar()
}

def static "callCenter.Page_Administracion_De_Reclamos.focus"() {
    (new callCenter.Page_Administracion_De_Reclamos()).focus()
}

def static "callCenter.Page_Administracion_De_Reclamos.ingresarReclamo"(
    	String tipoReclamo	
     , 	String userId	
     , 	String motivo	
     , 	String submotivo	) {
    (new callCenter.Page_Administracion_De_Reclamos()).ingresarReclamo(
        	tipoReclamo
         , 	userId
         , 	motivo
         , 	submotivo)
}

def static "callCenter.Page_Administracion_De_Reclamos.ingresarNumeroDeCuenta"(
    	String numeroCuenta	) {
    (new callCenter.Page_Administracion_De_Reclamos()).ingresarNumeroDeCuenta(
        	numeroCuenta)
}

def static "callCenter.Page_Administracion_De_Reclamos.ingresarReclamo"(
    	String tipoReclamo	
     , 	String numeroCuenta	) {
    (new callCenter.Page_Administracion_De_Reclamos()).ingresarReclamo(
        	tipoReclamo
         , 	numeroCuenta)
}

def static "callCenter.Page_Administracion_De_Reclamos.ingresarCuenta"(
    	String numeroCuenta	) {
    (new callCenter.Page_Administracion_De_Reclamos()).ingresarCuenta(
        	numeroCuenta)
}

def static "callCenter.Page_Administracion_De_Reclamos.buscarReclamo"() {
    (new callCenter.Page_Administracion_De_Reclamos()).buscarReclamo()
}

def static "callCenter.Page_Administracion_De_Reclamos.limpiar"() {
    (new callCenter.Page_Administracion_De_Reclamos()).limpiar()
}

def static "callCenter.Page_Administracion_De_Reclamos.existsBtnAceptar"() {
    (new callCenter.Page_Administracion_De_Reclamos()).existsBtnAceptar()
}

def static "callCenter.Page_Administracion_De_Reclamos.cerrarApp"() {
    (new callCenter.Page_Administracion_De_Reclamos()).cerrarApp()
}

def static "callCenter.Page_Administracion_De_Reclamos.verificarNuevaVentana"() {
    (new callCenter.Page_Administracion_De_Reclamos()).verificarNuevaVentana()
}

def static "callCenter.Page_Administracion_De_Reclamos.verificarVentanaReclamo"() {
    (new callCenter.Page_Administracion_De_Reclamos()).verificarVentanaReclamo()
}

def static "callCenter.Page_Administracion_De_Reclamos.seleccionarTipoReclamo"(
    	Object index	) {
    (new callCenter.Page_Administracion_De_Reclamos()).seleccionarTipoReclamo(
        	index)
}

def static "callCenter.Page_Administracion_De_Reclamos.ingresarReclamo"(
    	String tipoReclamo	
     , 	String path	
     , 	int sheet	
     , 	int row	
     , 	int column	) {
    (new callCenter.Page_Administracion_De_Reclamos()).ingresarReclamo(
        	tipoReclamo
         , 	path
         , 	sheet
         , 	row
         , 	column)
}

def static "callCenter.Page_Administracion_De_Reclamos.seleccionarAreasOperativas"(
    	Object index	) {
    (new callCenter.Page_Administracion_De_Reclamos()).seleccionarAreasOperativas(
        	index)
}

def static "callCenter.Page_Administracion_De_Reclamos.seleccionarPartidos"(
    	Object index	) {
    (new callCenter.Page_Administracion_De_Reclamos()).seleccionarPartidos(
        	index)
}

def static "callCenter.Page_Administracion_De_Reclamos.seleccionarLocalidades"(
    	Object index	) {
    (new callCenter.Page_Administracion_De_Reclamos()).seleccionarLocalidades(
        	index)
}

def static "callCenter.Page_Administracion_De_Reclamos.seleccionarCalle"(
    	Object index	) {
    (new callCenter.Page_Administracion_De_Reclamos()).seleccionarCalle(
        	index)
}

def static "callCenter.Page_Administracion_De_Reclamos.seleccionarNumero"(
    	Object numero	) {
    (new callCenter.Page_Administracion_De_Reclamos()).seleccionarNumero(
        	numero)
}

def static "callCenter.Page_Administracion_De_Reclamos.seleccionarBusqueda"(
    	Object index	) {
    (new callCenter.Page_Administracion_De_Reclamos()).seleccionarBusqueda(
        	index)
}

def static "callCenter.Page_Administracion_De_Reclamos.clickCallesPuntos"() {
    (new callCenter.Page_Administracion_De_Reclamos()).clickCallesPuntos()
}

def static "oms.ProcesoCreacionNuevoDocumento.crearNuevoDocumento"(
    	String tipoDocumento	
     , 	String minutos	
     , 	String coordenadas	) {
    (new oms.ProcesoCreacionNuevoDocumento()).crearNuevoDocumento(
        	tipoDocumento
         , 	minutos
         , 	coordenadas)
}

def static "oms.ProcesoCreacionNuevoDocumento.crear"(
    	String tipoDocumento	
     , 	String coordenadas	) {
    (new oms.ProcesoCreacionNuevoDocumento()).crear(
        	tipoDocumento
         , 	coordenadas)
}

def static "oms.ProcesoCreacionNuevoDocumento.ingresarDatos"(
    	String tipoDocumento	) {
    (new oms.ProcesoCreacionNuevoDocumento()).ingresarDatos(
        	tipoDocumento)
}

def static "oms.ProcesoCreacionNuevoDocumento.seleccionarElementoCromo"(
    	String zona	
     , 	String partido	
     , 	String localidad	
     , 	String idElemento	) {
    (new oms.ProcesoCreacionNuevoDocumento()).seleccionarElementoCromo(
        	zona
         , 	partido
         , 	localidad
         , 	idElemento)
}

def static "oms.ProcesoCreacionNuevoDocumento.finalizarCreacion"() {
    (new oms.ProcesoCreacionNuevoDocumento()).finalizarCreacion()
}

def static "oms.ProcesoCreacionNuevoDocumento.guardarNumeroDeDocumento"() {
    (new oms.ProcesoCreacionNuevoDocumento()).guardarNumeroDeDocumento()
}

def static "oms.ProcesoCreacionNuevoDocumento.abrirNuevoDocumento"() {
    (new oms.ProcesoCreacionNuevoDocumento()).abrirNuevoDocumento()
}

def static "sikuli.Workstation.AbrirWorkstation"() {
    (new sikuli.Workstation()).AbrirWorkstation()
}

def static "sikuli.Workstation.AbrirOMS"() {
    (new sikuli.Workstation()).AbrirOMS()
}

def static "nexus.EAM.crearAnomaliaConDocumento"(
    	String zona	
     , 	String partido	
     , 	String localidad	
     , 	String idElemento	
     , 	String calle	
     , 	String entreCalle1	
     , 	String instalacion	
     , 	String tipoAnomalia	
     , 	String clasificacion	
     , 	String prioridad	) {
    (new nexus.EAM()).crearAnomaliaConDocumento(
        	zona
         , 	partido
         , 	localidad
         , 	idElemento
         , 	calle
         , 	entreCalle1
         , 	instalacion
         , 	tipoAnomalia
         , 	clasificacion
         , 	prioridad)
}

def static "nexus.EAM.verificarNumeroOTGenerado"(
    	int index	) {
    (new nexus.EAM()).verificarNumeroOTGenerado(
        	index)
}

def static "oms.ProcesoDespachoEquipoMovil.ingresarEquipoMovil"(
    	String codigoEquipoMovil	) {
    (new oms.ProcesoDespachoEquipoMovil()).ingresarEquipoMovil(
        	codigoEquipoMovil)
}

def static "oms.Window_ApliqueUnFiltroYSeleccionUnDocumento.buscarDocumento"(
    	Object path	
     , 	Object docSheet	
     , 	Object row	
     , 	Object column	) {
    (new oms.Window_ApliqueUnFiltroYSeleccionUnDocumento()).buscarDocumento(
        	path
         , 	docSheet
         , 	row
         , 	column)
}

def static "oms.Window_ApliqueUnFiltroYSeleccionUnDocumento.clickAceptar"() {
    (new oms.Window_ApliqueUnFiltroYSeleccionUnDocumento()).clickAceptar()
}

def static "oms.Window_CerrarDocumento.seleccionarInstalacion"() {
    (new oms.Window_CerrarDocumento()).seleccionarInstalacion()
}

def static "oms.Window_CerrarDocumento.seleccionarCausa"() {
    (new oms.Window_CerrarDocumento()).seleccionarCausa()
}

def static "oms.Window_CerrarDocumento.cerrarActividad"() {
    (new oms.Window_CerrarDocumento()).cerrarActividad()
}

def static "oms.Window_CerrarDocumento.clickAceptar"() {
    (new oms.Window_CerrarDocumento()).clickAceptar()
}

def static "oms.Window_CerrarDocumento.clickCierreProvisorio"() {
    (new oms.Window_CerrarDocumento()).clickCierreProvisorio()
}

def static "oms.Window_CerrarDocumento.seleccionarInstalacion"(
    	Object index	) {
    (new oms.Window_CerrarDocumento()).seleccionarInstalacion(
        	index)
}

def static "oms.Window_CerrarDocumento.seleccionarCausa"(
    	Object index	) {
    (new oms.Window_CerrarDocumento()).seleccionarCausa(
        	index)
}

def static "oms.Window_CerrarDocumento.clickNuevoTrabajo"() {
    (new oms.Window_CerrarDocumento()).clickNuevoTrabajo()
}

def static "oms.Window_CerrarDocumento.realizarCierre"() {
    (new oms.Window_CerrarDocumento()).realizarCierre()
}

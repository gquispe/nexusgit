package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object CROMO_URL
     
    /**
     * <p></p>
     */
    public static Object DEFAULT_WAIT
     
    /**
     * <p></p>
     */
    public static Object JAVASCRIPT_WAIT
     
    /**
     * <p></p>
     */
    public static Object CERTA
     
    /**
     * <p></p>
     */
    public static Object IMAGENES
     
    /**
     * <p></p>
     */
    public static Object CONTADOR
     
    /**
     * <p></p>
     */
    public static Object GI_URL
     
    /**
     * <p></p>
     */
    public static Object START_TIME
     
    /**
     * <p></p>
     */
    public static Object LOGINTRY
     
    /**
     * <p></p>
     */
    public static Object MAXIMO_INTENTOS
     
    /**
     * <p>Profile default : http:&#47;&#47;itlin12:8500&#47;</p>
     */
    public static Object CROMO_QA1_URL
     
    /**
     * <p></p>
     */
    public static Object CROMO_QA2_URL
     
    /**
     * <p></p>
     */
    public static Object MINIMUM_WAIT
     
    /**
     * <p></p>
     */
    public static Object MAXIMUM_WAIT
     
    /**
     * <p></p>
     */
    public static Object ENVIRONMENT
     
    /**
     * <p></p>
     */
    public static Object COUNT
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += RunConfiguration.getOverridingParameters()
    
            CROMO_URL = selectedVariables['CROMO_URL']
            DEFAULT_WAIT = selectedVariables['DEFAULT_WAIT']
            JAVASCRIPT_WAIT = selectedVariables['JAVASCRIPT_WAIT']
            CERTA = selectedVariables['CERTA']
            IMAGENES = selectedVariables['IMAGENES']
            CONTADOR = selectedVariables['CONTADOR']
            GI_URL = selectedVariables['GI_URL']
            START_TIME = selectedVariables['START_TIME']
            LOGINTRY = selectedVariables['LOGINTRY']
            MAXIMO_INTENTOS = selectedVariables['MAXIMO_INTENTOS']
            CROMO_QA1_URL = selectedVariables['CROMO_QA1_URL']
            CROMO_QA2_URL = selectedVariables['CROMO_QA2_URL']
            MINIMUM_WAIT = selectedVariables['MINIMUM_WAIT']
            MAXIMUM_WAIT = selectedVariables['MAXIMUM_WAIT']
            ENVIRONMENT = selectedVariables['ENVIRONMENT']
            COUNT = selectedVariables['COUNT']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}

package oms

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable
import org.sikuli.script.Key

public class Window_SeleccioneUnEquipoMovil extends helper.UIContext{

	protected static final String IMAGENES_OMS_DESPACHOMOVIL =  GlobalVariable.IMAGENES + "OMS\\Windows\\SeleccioneUnEquipoMovil\\"

	private static final String IMG_BUTTON_ACEPTAR	= IMAGENES_OMS_DESPACHOMOVIL + "Button_Aceptar.PNG"
	private static final String IMG_BUTTON_LISTA 	= IMAGENES_OMS_DESPACHOMOVIL + "Button_Lista.PNG"

	@Keyword
	def seleccionar(String codigo){
		esperarVentanaAbierta()
		ingresarCodigoEquipoMovil(codigo)
		aceptarSeleccion()
	}

	def esperarVentanaAbierta(){
		driver.wait(IMG_BUTTON_LISTA,GlobalVariable.MINIMUM_WAIT)
	}

	def ingresarCodigoEquipoMovil(String codigo){
		driver.paste(codigo)
		driver.type(Key.ENTER)
	}

	@Keyword
	def aceptarSeleccion(){
		clickElement(IMG_BUTTON_ACEPTAR)
		Thread.sleep(1000)
	}

	@Keyword
	def clicklista(){
		clickElement(IMG_BUTTON_LISTA)
		Thread.sleep(2000)
	}
}

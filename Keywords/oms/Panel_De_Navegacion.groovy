package oms

import org.sikuli.script.Key
import org.sikuli.script.Region

import com.kms.katalon.core.annotation.Keyword

import helper.UIContext
import internal.GlobalVariable

public class Panel_De_Navegacion extends helper.UIContext{

	private static final String IMG_PANEL_DE_NAVEGACION = GlobalVariable.IMAGENES + "OMS\\Panels\\PanelDeNavegacion\\"

	private static final String IMG_ICON_RECLAMOS = IMG_PANEL_DE_NAVEGACION + "Icon_Reclamo.PNG"
	private static final String IMG_ANOMALIAS = IMG_PANEL_DE_NAVEGACION + "anomalias.PNG"
	private static final String IMG_ANOMALIAS_DETECTADAS = IMG_PANEL_DE_NAVEGACION + "anomaliasdetectadas.PNG"
	private static final String IMG_ABIERTOS_FORZADOS_BT= IMG_PANEL_DE_NAVEGACION + "abiertosforzadosbt.PNG"

	@Keyword
	def clickHeaderPanelDeNavegacion(){
		Region header = getNorthRegion(IMG_ICON_RECLAMOS)
		clickElement(header)
	}

	@Keyword
	def inputReclamoId(String codigoReclamo){
		driver.type(Key.TAB)
		driver.type(Key.TAB)
		driver.type(Key.TAB)
		driver.type(codigoReclamo)
		driver.type(Key.ENTER)
	}

	@Keyword
	def seleccionarAnomalias(){
		doubleClickElement(IMG_ANOMALIAS)
	}

	@Keyword
	def clickAnomaliasDetectadas(){
		clickElement(IMG_ANOMALIAS_DETECTADAS)
		Thread.sleep(2000)
	}
	
	@Keyword
	def clickAbiertosForzadosBT(){
		clickElement(IMG_ABIERTOS_FORZADOS_BT)
		Thread.sleep(10000)
	}
}

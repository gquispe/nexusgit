package oms

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.sikuli.script.Screen
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import helper.ScreenDriver
import org.sikuli.script.KeyModifier
import org.sikuli.script.Key
import org.sikuli.script.Pattern
import internal.GlobalVariable
import helper.CustomScreenshot
import helper.CodReclamos
import junit.framework.Assert
import nexus.Script705_ExcelData

public class Window_Documento_Header extends Main{
	protected static Screen screen = ScreenDriver.getIntance()
	private ArrayList<String> codigosReclamo = CodReclamos.getIntance()
	private Script705_ExcelData excelData = new Script705_ExcelData()

	private static String IMG_OPT_FORZADO_BT = IMAGENES_OMS + "opt_forzado_bt.PNG"
	private static String IMG_OPT_FORZADO_MT = IMAGENES_OMS + "opt_forzado_mt.PNG"

	private static String forzadoMT = "FORZADO MT"
	private static String forzadoBT = "FORZADO BT"

	private Pattern patternTipoForzadoEsperado
	private String tipoForzadoActual
	private Pattern IMG_OPT_FORZADO_BT_EXACT = new Pattern(IMG_OPT_FORZADO_BT).exact()
	private Pattern IMG_OPT_FORZADO_MT_EXACT = new Pattern(IMG_OPT_FORZADO_MT).exact()

	@Keyword
	def checkTipoForzado(String tipoForzado, String iteracion){
		int i = Integer.parseInt(iteracion)
		patternTipoForzadoEsperado = new Pattern(asignarTipoForzadoImg(tipoForzado)).exact()
		tipoForzadoActual = tipoForzadoActual()
		new CustomScreenshot(screen,"Iteracion_" + iteracion)
		KeywordUtil.logInfo("Tipo de Forzado Esperado: " + tipoForzado)
		KeywordUtil.logInfo( "Tipo de Forzado Actual: " + tipoForzadoActual)
		if (tipoForzado == tipoForzadoActual){
			KeywordUtil.markPassed("Tipo de Forzado Igual. ")
			excelData.saveTipoDocumentoObtenido(i, tipoForzadoActual)
		}
		else{
			KeywordUtil.markFailed("Tipo de Forzado Diferente. ")
			excelData.saveTipoDocumentoObtenido(i, tipoForzadoActual)
		}
	}

	def asignarTipoForzadoImg(String tipoForzado){
		return tipoForzado==forzadoMT?IMG_OPT_FORZADO_MT:IMG_OPT_FORZADO_BT;
	}

	def asignarTipoForzado(String tipoForzado){
		return tipoForzado==forzadoMT?IMG_OPT_FORZADO_MT:IMG_OPT_FORZADO_BT;
	}

	def tipoForzadoActual(){
		return screen.exists(IMG_OPT_FORZADO_BT_EXACT,5)?forzadoBT:forzadoMT;
	}
}

package oms

import org.sikuli.script.Key
import org.sikuli.script.Pattern

import com.kms.katalon.core.annotation.Keyword

import internal.GlobalVariable
import org.sikuli.script.Region

public class Window_CerrarDocumento extends helper.UIContext{

	private static final String IMG_OMS_DOCUMENTO= GlobalVariable.IMAGENES + "oms\\Windows\\Documento\\"
	private static final String IMG_OMS_ACEPTAR = IMG_OMS_DOCUMENTO + "Aceptar.PNG"
	private static final String IMG_OMS_CERRAR_DOCUMENTO= GlobalVariable.IMAGENES + "oms\\Windows\\CerrarDocumento\\"
	private static final String IMG_OMS_INSTALACION = IMG_OMS_CERRAR_DOCUMENTO + "instalacion.PNG"
	private static final String IMG_OMS_OBSERVACIONES = IMG_OMS_CERRAR_DOCUMENTO + "observaciones.PNG"
	private static final String IMG_OMS_CHECKOBOX_PROVISORIO = IMG_OMS_CERRAR_DOCUMENTO + "checkbox_provisorio.PNG"
	private static final String IMG_OMS_INSTALACION_FLECHA = IMG_OMS_CERRAR_DOCUMENTO + "instalacionflecha.PNG"
	private static final String IMG_OMS_CAUSA_FLECHA = IMG_OMS_CERRAR_DOCUMENTO + "causaflecha.PNG"
	private static final String IMG_OMS_FLECHA = IMG_OMS_CERRAR_DOCUMENTO + "flecha.PNG"
	private static final String IMG_OMS_FLECHA_ACTIVA = IMG_OMS_CERRAR_DOCUMENTO + "flechaactiva.PNG"
	private static final String IMG_OMS_NUEVO_TRABAJO= IMG_OMS_CERRAR_DOCUMENTO + "nuevotrabajo.PNG"

	private Pattern IMG_OMS_INSTALACION_EXACT = new Pattern(IMG_OMS_INSTALACION).exact()

	def esperarVentanaAbierta(){
		driver.wait(IMG_OMS_ACEPTAR,GlobalVariable.DEFAULT_WAIT)
	}

	@Keyword
	def seleccionarInstalacion(){
		Thread.sleep(3000)
		driver.find(IMG_OMS_INSTALACION_FLECHA).click(IMG_OMS_FLECHA)
		driver.type("l")
		driver.click(IMG_OMS_FLECHA_ACTIVA)
	}

	@Keyword
	def seleccionarCausa(){
		driver.find(IMG_OMS_CAUSA_FLECHA).click(IMG_OMS_FLECHA)
		driver.type("r")
	}

	@Keyword
	def cerrarActividad(){
		Thread.sleep(3000)
		driver.type(Key.ENTER)
	}

	@Keyword
	def clickAceptar(){
		Region afectados = driver.find(IMG_OMS_ACEPTAR)
		afectados.setX(afectados.getX() - afectados.getW())
		afectados.click()
		clickElement(IMG_OMS_ACEPTAR)
		Thread.sleep(2000)
	}

	@Keyword
	def clickCierreProvisorio(){
		clickElement(IMG_OMS_CHECKOBOX_PROVISORIO)
	}

	@Keyword
	def seleccionarInstalacion(index){
		driver.find(IMG_OMS_INSTALACION_FLECHA).click(IMG_OMS_FLECHA)
		down(index)
		clickElement(IMG_OMS_FLECHA_ACTIVA)
	}

	@Keyword
	def seleccionarCausa(index){
		driver.find(IMG_OMS_CAUSA_FLECHA).click(IMG_OMS_FLECHA)
		down(index)
		clickElement(IMG_OMS_FLECHA_ACTIVA)
	}

	@Keyword
	def clickNuevoTrabajo(){
		clickElement(IMG_OMS_NUEVO_TRABAJO)
	}

	@Keyword
	def realizarCierre(){
		seleccionarInstalacion()
		seleccionarCausa()
		clickAceptar()
	}
}

package oms

import org.sikuli.script.Region

import com.kms.katalon.core.annotation.Keyword

import internal.GlobalVariable

public class SeleccioneUnEquipoMovil extends helper.UIContext{

	protected static final String IMAGENES_OMS_SELECCION_UN_EQUIPO_MOVIL =  GlobalVariable.IMAGENES + "OMS\\Windows\\SeleccioneUnEquipoMovil\\"

	private static final String IMG_VIA_DE_COMUNICACION	= IMAGENES_OMS_SELECCION_UN_EQUIPO_MOVIL + "ViaDeComunicacion.PNG"
	private static final String IMG_ELEMENTOS_MOSTRADOS	= IMAGENES_OMS_SELECCION_UN_EQUIPO_MOVIL + "elementosmostrados.PNG"
	private static final String IMG_CLICK	= IMAGENES_OMS_SELECCION_UN_EQUIPO_MOVIL + "click.PNG"
	private static final String IMG_ACEPTAR	= IMAGENES_OMS_SELECCION_UN_EQUIPO_MOVIL + "aceptar.PNG"
	private static final String IMG_ICON_FILTRAR	= IMAGENES_OMS_SELECCION_UN_EQUIPO_MOVIL + "Icon_Filtrar.PNG"
	private static final String IMG_HEADER_CODIGO	= IMAGENES_OMS_SELECCION_UN_EQUIPO_MOVIL + "Header_Codigo.PNG"

	@Keyword
	def clickViaDeComunicacion(){
		clickElement(IMG_VIA_DE_COMUNICACION)
	}

	@Keyword
	def seleccionarElementosMostrados(index){
		seleccionDropDown(IMG_ELEMENTOS_MOSTRADOS, index)
		Thread.sleep(1000)
	}

	@Keyword
	def seleccionarEquipoMovilClick(){
		verifyAndClick(IMG_CLICK)
		enter(1)
		Thread.sleep(1000)
	}

	@Keyword
	def aceptarEquipoMovil(){
		tab(1)
		enter(1)
	}

	@Keyword
	def seleccionarEquipoMovilClick(index){
		down(index)
	}

	@Keyword
	def clickAceptar(){
		clickElement(IMG_ACEPTAR)
	}

	@Keyword
	def filtrarPorCodigo(String codigo){
		Region filter = getBottomRegion(IMG_HEADER_CODIGO)
		driver.click(filter)
		driver.type(codigo)
	}

	@Keyword
	def clickFiltrar(){
		clickElement(IMG_ICON_FILTRAR)
	}

	@Keyword
	def seleccionarFiltrado(){
		Region headerCodigo = driver.find(IMG_HEADER_CODIGO)
		selectElementFromList(headerCodigo,2)
	}

	@Keyword
	def seleccionarElementoPorCodigo(String codigo){
		clickFiltrar()
		filtrarPorCodigo(codigo)
		seleccionarFiltrado()
		clickAceptar()
	}

	@Keyword
	def seleccionarElementoPorIndex(int index){
		Region elemento = selectElementFromList(IMG_HEADER_CODIGO,index)
		driver.click(elemento)
		clickAceptar()
	}
}

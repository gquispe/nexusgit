package oms

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.sikuli.script.App

import internal.GlobalVariable

public class Window_SeleccionTopologica extends helper.UIContext{

	protected static final String IMAGENES_OMS_SELECCION_TOPOLOGICA =  GlobalVariable.IMAGENES + "OMS\\Windows\\SeleccionTopologica\\"
	private static final String IMG_SELECCIONAR_ELEMENTOS 		= IMAGENES_OMS_SELECCION_TOPOLOGICA + "BotonGreenArrows.PNG"
	private static final String IMG_OPTION_SELECCIONAR_ELEMENTO = IMAGENES_OMS_SELECCION_TOPOLOGICA + "Option_Elemento.PNG"
	private static final String IMG_POPUP_SELECCIONAR_ELEMENTO  = IMAGENES_OMS_SELECCION_TOPOLOGICA + "Popup_SeleccionarElemento.PNG"
	private static final String IMG_BUTTON_ACEPTAR_SELECCION    = IMAGENES_OMS_SELECCION_TOPOLOGICA + "BotonAceptar.PNG"

	@Keyword
	def seleccionarElementoClienteGrafico(){
		clickOpcionElemento()
		clickElement(IMG_SELECCIONAR_ELEMENTOS)
		confirmacionInicioSeleccion()
	}

	def confirmacionInicioSeleccion(){
		checkThatIsVisibleWithTime(IMG_POPUP_SELECCIONAR_ELEMENTO,"Pop up Seleccionar Elemento en Cromo",GlobalVariable.DEFAULT_WAIT)
	}

	def clickAceptarElementoSeleccionado(){
		focus()
		clickElement(IMG_BUTTON_ACEPTAR_SELECCION)
	}

	def clickOpcionElemento(){
		if(verifyElementIsVisible(IMG_OPTION_SELECCIONAR_ELEMENTO,GlobalVariable.MINIMUM_WAIT)){
			clickElement(IMG_OPTION_SELECCIONAR_ELEMENTO)
		}
	}

	def focus(){
		App.focus("Selecci")
		App.focus("Selecci")
	}
}

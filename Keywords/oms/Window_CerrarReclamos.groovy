package oms

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class Window_CerrarReclamos extends helper.UIContext{

	protected static final String IMG_OMS_CERRAR_RECLAMOS = GlobalVariable.IMAGENES + "OMS\\Windows\\CerrarReclamos\\"
	private static final String IMG_ACEPTAR	 	  = IMG_OMS_CERRAR_RECLAMOS + "aceptar.PNG"

	@Keyword
	def clickAceptar(){
		Thread.sleep(1000)
		//		clickElement(IMG_ACEPTAR)
		enter(1)
		Thread.sleep(2000)
	}
}

package oms

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.sikuli.script.App

import internal.GlobalVariable
import com.kms.katalon.core.util.KeywordUtil

public class TAB_Afectacion extends Window_Documento{

	protected static final String IMAGENES_AFECTACION		   = IMG_OMS_DOCUMENTO + "TAB_Afectacion\\"

	private static final String IMG_TOPOLOGICA		  		   = IMAGENES_AFECTACION + "BotonTopologica.PNG"
	private static final String IMG_GEOGRAFICA		     	   = IMAGENES_AFECTACION + "BotonGeografica.PNG"
	private static final String IMG_GREEN_ARROWS 			   = IMAGENES_AFECTACION + "BotonGreenArrows.PNG"
	private static final String IMG_BUTTON_ACEPTAR_SELECCION   = IMAGENES_AFECTACION + "BotonAceptar.PNG"
	private static final String IMG_BUTTON_ACEPTAR_AFECTACION  = IMAGENES_AFECTACION + "BotonAceptar_AfectarDocumento.PNG"
	private static final String IMG_POPUP_SELECCIONAR_ELEMENTO = IMAGENES_AFECTACION + "Popup_SeleccionarElemento.PNG"


	@Keyword
	def inicioAfectacion(String tipoAfectacion){
		clickAfectar()
		seleccionarTipoDeAfectacion(tipoAfectacion)
		seleccionarElemento()
	}

	def seleccionarTipoDeAfectacion(String tipoAfectacion){
		if (tipoAfectacion == "Topologica")
			clickTopologica()
		else if (tipoAfectacion == "Geografica")
			clickGeografica()
		else
			KeywordUtil.markFailedAndStop("Tipo de Afectacion no valido.")
	}
	def clickTopologica(){
		clickElement(IMG_TOPOLOGICA)
	}

	def clickGeografica(){
		clickElement(IMG_GEOGRAFICA)
	}

	def seleccionarElemento(){
		clickElement(IMG_GREEN_ARROWS)
		checkThatIsVisibleWithTime(IMG_POPUP_SELECCIONAR_ELEMENTO,"Pop up Seleccionar Elemento en Cromo",GlobalVariable.DEFAULT_WAIT)
	}

	@Keyword
	def finalizarAfectacion(){
		focus()
		clickAceptarElementoSeleccionado()
		clickAceptarAfectacionDocumento()
	}

	def focus(){
		App.focus("Selecci")
		App.focus("Selecci")
	}

	def clickAceptarElementoSeleccionado(){
		clickElement(IMG_BUTTON_ACEPTAR_SELECCION)
	}

	def clickAceptarAfectacionDocumento(){
		clickElement(IMG_BUTTON_ACEPTAR_AFECTACION)
	}
}

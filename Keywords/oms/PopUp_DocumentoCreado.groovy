package oms

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class PopUp_DocumentoCreado extends helper.UIContext{

	private static final String TITULO_VENTANA = "Documento Creado"
	protected static final String IMAGENES_OMS_DOCUMENTO_CREADO =  GlobalVariable.IMAGENES + "OMS\\PopUp\\DocumentoCreado\\"
	private static final String IMG_DOCUMENTO_CREADO = IMAGENES_OMS_DOCUMENTO_CREADO + "TituloDocumentoCreado.PNG"
	private static final String IMG_ABRIR			 = IMAGENES_OMS_DOCUMENTO_CREADO + "BotonAbrir.PNG"
	private static final String IMG_CERRAR		 = IMAGENES_OMS_DOCUMENTO_CREADO + "cerrar.PNG"
	private static final String IMG_ANOMALIA_CREADA		 = IMAGENES_OMS_DOCUMENTO_CREADO + "anomaliacreada.PNG"

	@Keyword
	def verificarDocumentoCreado(){
		checkThatIsVisibleWithTime(IMG_DOCUMENTO_CREADO, "Documento creado", GlobalVariable.MINIMUM_WAIT)
	}

	@Keyword
	def clickAbrir(){
		clickElement(IMG_ABRIR)
	}

	@Keyword
	def clickCerrar(){
		clickElement(IMG_CERRAR)
	}

	@Keyword
	def verificarAnomaliaCreada(){
		checkThatIsVisibleWithTime(IMG_ANOMALIA_CREADA, "Anomalia creada", GlobalVariable.MINIMUM_WAIT)
	}
}

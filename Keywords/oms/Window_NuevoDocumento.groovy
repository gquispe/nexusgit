package oms

import java.text.SimpleDateFormat

import org.sikuli.script.App
import org.sikuli.script.Key

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.util.KeywordUtil

import internal.GlobalVariable

public class Window_NuevoDocumento extends helper.UIContext{

	private static final String TITULO_VENTANA = "Nuevo Documento"
	protected static final String IMAGENES_OMS_NUEVO_DOCUMENTO =  GlobalVariable.IMAGENES + "OMS\\Windows\\NuevoDocumento\\"
	
	private static final String IMG_SELECT_TIPODOCUMENTO = IMAGENES_OMS_NUEVO_DOCUMENTO + "Select_TipoDocumento.PNG"
	private static final String IMG_ACEPTAR 	   		 = IMAGENES_OMS_NUEVO_DOCUMENTO + "BotonAceptar.PNG"
	private static final String IMG_TIEMPO_RESTAURACION	 = IMAGENES_OMS_NUEVO_DOCUMENTO + "tiemporestauracion.PNG"
	private static final String IMG_ICON_FLECHAS_VERDES	 = IMAGENES_OMS_NUEVO_DOCUMENTO + "Icon_FlechasVerdes.PNG"
	private static final String IMG_POPUP_CROMO			 = IMAGENES_OMS_NUEVO_DOCUMENTO + "PopUp_SeleccionarEnElMapa.PNG"

	@Keyword
	def fechaDeCorteActualMasMinutos(minutes){
		Calendar calendar = Calendar.getInstance();
		KeywordUtil.logInfo(">>> Getting time: " + calendar.getTime());
		calendar.add(Calendar.MINUTE, Integer.parseInt(minutes));
		KeywordUtil.logInfo(">>> Increasing time by " + minutes + " = "+ calendar.getTime());
		SimpleDateFormat omsFormat = new SimpleDateFormat("ddMMyyyyhhmm");
		String formatted = omsFormat.format(calendar.getTime());
		KeywordUtil.logInfo(">>> Formating time for entry: " + formatted);
		tab(2)
		driver.type(formatted)
	}

	@Keyword
	def tiempoDeRestauracion(){
		//		tab(6)
		clickElement(IMG_TIEMPO_RESTAURACION)
		driver.type("0")
		driver.type(Key.ENTER)
	}

	@Keyword
	def ingresarCoordenadas(coordenadas){
		KeywordUtil.logInfo(">>> Entering coords: " + coordenadas);
		driver.type(Key.TAB)
		driver.type(coordenadas.replaceAll("[^0-9]", ""))
	}

	@Keyword
	def seleccionarMotivo(){
		driver.type(Key.TAB)
		driver.type("o")
	}

	@Keyword
	def clickAceptar(){
		clickElement(IMG_ACEPTAR)
	}

	@Keyword
	def generarNuevoDocumento(String tipoDocumento, String minutos, String coordenadas){
		seleccionarTipoDocumento(tipoDocumento)
		fechaDeCorteActualMasMinutos(minutos)
		tiempoDeRestauracion()
		ingresarCoordenadas(coordenadas)
		seleccionarMotivo()
		clickAceptar()
	}
	
	@Keyword
	def generarNuevoDocumento(String tipoDocumento, String coordenadas){
		generarNuevoDocumento(tipoDocumento,"0",coordenadas)
	}

	def seleccionarTipoDocumento(String tipoDocumento){
		driver.type(Key.TAB)
		driver.type(tipoDocumento)
	}
	
	@Keyword
	def ingresarTipoDocumento(String tipoDocumento){
		clickRightRegion(IMG_SELECT_TIPODOCUMENTO)
		typeText(tipoDocumento)
		typeKey(Key.ENTER)
	}
	
	@Keyword
	def seleccionarCoordenadas(){
		clickElement(IMG_ICON_FLECHAS_VERDES)
		verifyElementIsVisible(IMG_POPUP_CROMO)
	}
	
	def focus(){
		App.focus("Nuevo Documento")
		App.focus("Nuevo Documento")
	}

}

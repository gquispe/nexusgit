package oms

import org.sikuli.script.Key

import com.kms.katalon.core.annotation.Keyword

public class Window_AgregarActividad extends helper.UIContext{

	@Keyword
	def ingresarCodigo(){
		driver.type(Key.TAB)
		driver.type("702")
		driver.type(Key.ENTER)
	}

	@Keyword
	def aceptarActividad(){
		driver.type(Key.ENTER)
	}
}

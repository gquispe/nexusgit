package oms

import com.kms.katalon.core.annotation.Keyword

public class ProcesoCerrarReclamo {

	private Window_Documento_Tab_Reclamos tabReclamos = new Window_Documento_Tab_Reclamos()
	private Window_CerrarReclamos  cerrarReclamos	  = new Window_CerrarReclamos()
	private Window_DetalleDeReclamo detalleReclamo	  = new Window_DetalleDeReclamo()

	@Keyword
	def cerrarReclamo(String numeroReclamo){
		tabReclamos.seleccionarReclamo(numeroReclamo)
		tabReclamos.clickCerrar()
		cerrarReclamos.clickAceptar()
		tabReclamos.esperarReclamoCerrado()
	}

	@Keyword
	def verificarReclamoCerrado(String numeroReclamo){
		tabReclamos.seleccionarReclamo(numeroReclamo)
		tabReclamos.clickDetalles()
		detalleReclamo.verificarCerradoDefinitivo()
		detalleReclamo.cerrarVentana()
	}
}

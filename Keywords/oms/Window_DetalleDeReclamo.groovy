package oms

import org.sikuli.script.Env
import org.sikuli.script.Key
import org.sikuli.script.KeyModifier
import org.sikuli.script.Pattern
import org.sikuli.script.Screen

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.util.KeywordUtil

import helper.DocUtils
import helper.ScreenDriver
import internal.GlobalVariable

public class Window_DetalleDeReclamo extends helper.UIContext{

	protected static Screen screen = ScreenDriver.getIntance()

	protected static final String IMG_OMS_DETALLE_DE_RECLAMO = GlobalVariable.IMAGENES + "OMS\\Windows\\DetalleDeReclamo\\"
	private static final String IMG_CERRADO_DEFINITIVO 	  = IMG_OMS_DETALLE_DE_RECLAMO + "cerradodefinitivo.PNG"

	private static final String IMG_ICO_VER_DOCUMENTO = GlobalVariable.IMAGENES + "OMS\\icon_ver_documento.PNG"
	private static final String IMG_ICO_VER_DISPONIBLES = GlobalVariable.IMAGENES + "OMS\\icons_disponibles.PNG"
	private static final String IMG_WINDOW_MAIN = GlobalVariable.IMAGENES + "OMS\\window_main.PNG"
	private Pattern patternIconosDisponibles = new Pattern(IMG_ICO_VER_DISPONIBLES).exact()
	private static final String IMG_CLOSE_WINDOW = GlobalVariable.IMAGENES + "OMS\\CerrarVentana.PNG"
	private static final String IMG_CLOSE_WINDOW_RED = GlobalVariable.IMAGENES + "OMS\\CerrarVentanaRoja.PNG"
	private static final String IMG_BUTTON_CERRAR = IMG_OMS_DETALLE_DE_RECLAMO +  "Cerrar.PNG"
	private static final String IMG_TITULO = IMG_OMS_DETALLE_DE_RECLAMO + "titulo.PNG"
	private static final String IMG_VER_DOCUMENTO = IMG_OMS_DETALLE_DE_RECLAMO + "verdocumento.PNG"


	@Keyword
	def abrirDocumento() {
		if (screen.exists(patternIconosDisponibles,5)!=null){
			screen.click(IMG_ICO_VER_DOCUMENTO)
		}
		else {
			if(screen.exists(IMG_CLOSE_WINDOW)!=null)
				screen.click(IMG_CLOSE_WINDOW)
			else if (screen.exists(IMG_CLOSE_WINDOW_RED)!=null)
				screen.click(IMG_CLOSE_WINDOW_RED)
			screen.type(Key.ENTER)
			abrirDocumento()
		}
	}
	@Keyword
	def cerrarVentana(){
		KeywordUtil.logInfo(" >>> CERRANDO VENTANA")
		driver.type(Key.F4, KeyModifier.ALT);
		Thread.sleep(1000)
	}

	@Keyword
	def verificarCerradoDefinitivo(){
		checkThatIsVisibleWithTime(IMG_CERRADO_DEFINITIVO, "Cerrado definitivo", 5)
	}

	@Keyword
	def verificarVentana(){
		checkThatIsVisibleWithTime(IMG_TITULO, "Ventana", 5)
	}

	@Keyword
	def clickVerDocumento(){
		clickElement(IMG_VER_DOCUMENTO)
		Thread.sleep(1000)
	}

	@Keyword
	def verificarCliente(String path, int sheet, int row,int column){
		Thread.sleep(1000)
		tab(1)
		driver.type("c", Key.CTRL);
		driver.type("c", Key.CTRL);
		String clipboard = Env.getClipboard();
		String doc = DocUtils.readCell(path, sheet, row, column);
		checkThatEquals(clipboard, doc, "CLIENTE");
	}
}

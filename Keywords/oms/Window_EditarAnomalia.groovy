package oms

import org.sikuli.script.App

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.util.KeywordUtil

import internal.GlobalVariable

public class Window_EditarAnomalia extends helper.UIContext{

	protected static final String IMG_OMS_EDITAR_ANOMALIAS = GlobalVariable.IMAGENES + "OMS\\Windows\\EditarAnomalias\\"

	private static final String IMG_ELEMENTO  = IMG_OMS_EDITAR_ANOMALIAS + "elemento.PNG"
	private static final String IMG_INSTALACION  = IMG_OMS_EDITAR_ANOMALIAS + "instalacion.PNG"
	private static final String IMG_CAUSA = IMG_OMS_EDITAR_ANOMALIAS + "causa.PNG"
	private static final String IMG_PRIORIDAD  = IMG_OMS_EDITAR_ANOMALIAS + "prioridad.PNG"
	private static final String IMG_ACEPTAR  = IMG_OMS_EDITAR_ANOMALIAS + "aceptar.PNG"
	private static final String IMG_CANCELAR  = IMG_OMS_EDITAR_ANOMALIAS + "cancelar.PNG"
	private static final String IMG_ARROWS  = IMG_OMS_EDITAR_ANOMALIAS + "arrows.PNG"
	private static final String IMG_CLASIFICACION = IMG_OMS_EDITAR_ANOMALIAS + "clasificacion.PNG"
	private static final String IMG_TIPO_DE_ANOMALIA  = IMG_OMS_EDITAR_ANOMALIAS + "tipodeanomalia.PNG"
	private static final String IMG_CALLE = IMG_OMS_EDITAR_ANOMALIAS + "calle.PNG"
	private static final String IMG_ENTRE_CALLE_1 = IMG_OMS_EDITAR_ANOMALIAS + "entrecalle1.PNG"
	private static final String IMG_TIPO_DE_RED = IMG_OMS_EDITAR_ANOMALIAS + "tipodered.PNG"
	private static final String IMG_AREA_OPERATIVA = IMG_OMS_EDITAR_ANOMALIAS + "areaoperativa.PNG"
	private static final String IMG_PARTIDO = IMG_OMS_EDITAR_ANOMALIAS + "partido.PNG"
	private static final String IMG_LOCALIDAD = IMG_OMS_EDITAR_ANOMALIAS + "localidad.PNG"
	private static final String IMG_FIELD_NUMERO_OT = IMG_OMS_EDITAR_ANOMALIAS + "Field_NumeroOT.PNG"
	
	def focus(){
		App.focus("Editar anomal")
		App.focus("Editar anomal")
		driver.wait(IMG_CANCELAR)
	}
	@Keyword
	def ingresarElemento(elemento){
		driver.type(IMG_ELEMENTO, elemento)
	}

	@Keyword
	def seleccionarinstalacion(index){
		seleccionDropDown(IMG_INSTALACION, index)
	}
	
	@Keyword
	def ingresarInstalacion(instalacion){
		typeDropDown(IMG_INSTALACION,instalacion)
	}
	
	@Keyword
	def seleccionarCausa(index){
		seleccionDropDown(IMG_CAUSA, index)
	}

	@Keyword
	def seleccionarPrioridad(index){
		seleccionDropDown(IMG_PRIORIDAD, index)
	}
	
	@Keyword
	def ingresarPrioridad(prioridad){
		typeDropDown(IMG_PRIORIDAD,prioridad)
	}

	@Keyword
	def clickAceptar(){
		clickElement(IMG_ACEPTAR)
	}

	@Keyword
	def clickBotonVerde(){
		clickElement(IMG_ARROWS)
		Thread.sleep(3000)
	}

	@Keyword
	def seleccionarClasificacion(index){
		seleccionDropDown(IMG_CLASIFICACION, index)
	}
	
	@Keyword
	def ingresarClasificacion(clasificacion){
		typeDropDown(IMG_CLASIFICACION,clasificacion)
	}
	
	@Keyword
	def seleccionarTipoDeAnomalia(index){
		seleccionDropDown(IMG_TIPO_DE_ANOMALIA, index)
	}
	
	@Keyword
	def ingresarTipoDeAnomalia(tipoAnomalia){
		typeDropDown(IMG_TIPO_DE_ANOMALIA,tipoAnomalia)
	}
	
	@Keyword
	def seleccionarCalle(index){
		seleccionDropDown(IMG_CALLE, index)
	}
	
	@Keyword
	def ingresarCalle(calle){
		clearTypeDropDown(IMG_CALLE,calle)
	}
	
	@Keyword
	def seleccionarEntreCalle1(index){
		seleccionDropDown(IMG_ENTRE_CALLE_1, index)
	}	
	@Keyword
	def ingresarEntreCalle1(entreCalle1){
		clearTypeDropDown(IMG_ENTRE_CALLE_1,entreCalle1)	
	}
	
	@Keyword
	def ingresarAnomalia(calle,entreCalle1,instalacion,tipoAnomalia,clasificacion,prioridad){
		ingresarCalle(calle)
		ingresarEntreCalle1(entreCalle1)
		ingresarClasificacion(clasificacion)
		ingresarInstalacion(instalacion)
		ingresarPrioridad(prioridad)
		ingresarTipoDeAnomalia(tipoAnomalia)
	}
	
	@Keyword
	def seleccionarTipoDered(index){
		seleccionDropDown(IMG_TIPO_DE_RED, index)
	}
	
	@Keyword
	def seleccionarAreaOperativa(index){
		seleccionDropDown(IMG_AREA_OPERATIVA, index)
	}
	
	@Keyword
	def seleccionarPartido(index){
		seleccionDropDown(IMG_PARTIDO, index)
	}
	
	@Keyword
	def seleccionarLocalidad(index){
		seleccionDropDown(IMG_LOCALIDAD, index)
	}
	
	
	@Keyword
	public String getNumeroOT(){
		clearClipboard()
		clickRightRegion(IMG_FIELD_NUMERO_OT)
		selectField()
		return copiarSeleccion()
	}
	
	@Keyword
	public String verificacionNumeroOT(){
		String numeroOT = getNumeroOT()
		if(numeroOT != ""){
			KeywordUtil.markPassed("Numero de OT generado correctamente")
			KeywordUtil.logInfo("Numero de OT: " + numeroOT)
		}
		else
			KeywordUtil.markFailed("Error. Numero de OT no fue generado.")
	}
}

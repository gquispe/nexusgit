package oms

import org.sikuli.script.Region

import com.kms.katalon.core.annotation.Keyword

import internal.GlobalVariable

public class Window_Documento_Tab_Anomalias extends helper.UIContext{

	protected static final String IMG_OMS_DOCUMENTO_TAB_ANOMALIAS = GlobalVariable.IMAGENES + "OMS\\Windows\\Documento\\TAB_Anomalias\\"

	private static final String IMG_OMS_NUEVA_ANOMALIA  = IMG_OMS_DOCUMENTO_TAB_ANOMALIAS + "anomalias.PNG"
	private static final String IMG_OMS_DESVINCULAR  = IMG_OMS_DOCUMENTO_TAB_ANOMALIAS + "desvincular.PNG"
	private static final String IMG_OMS_POPUP_ACEPTAR  = IMG_OMS_DOCUMENTO_TAB_ANOMALIAS + "aceptar.PNG"
	private static final String IMG_ICON_VER_DETALLE    = IMG_OMS_DOCUMENTO_TAB_ANOMALIAS + "Icon_VerDetalle.PNG"
	private static final String IMG_HEADER_NUMERO_ANOMALIA    = IMG_OMS_DOCUMENTO_TAB_ANOMALIAS + "Header_NumeroAnomalia.PNG"

	@Keyword
	def clickNuevaAnomalia(){
		clickElement(IMG_OMS_NUEVA_ANOMALIA)
	}

	@Keyword
	def clickDesvincular(){
		clickElement(IMG_OMS_DESVINCULAR)
		enter(1)
	}

	@Keyword
	def clickAceptar(){
		clickElement(IMG_OMS_POPUP_ACEPTAR)
	}
	
	@Keyword
	def clickVerDetalle(){
		clickElement(IMG_ICON_VER_DETALLE)
	}

	@Keyword
	def seleccionarAnomalia(int indexAnomalia){
		Region anomaliaSeleccionada = selectElementFromList(IMG_HEADER_NUMERO_ANOMALIA, indexAnomalia)
		driver.click(anomaliaSeleccionada)
	}

}

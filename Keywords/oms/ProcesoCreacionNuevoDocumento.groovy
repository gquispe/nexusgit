package oms

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import cromo.Page_Main

import internal.GlobalVariable

public class ProcesoCreacionNuevoDocumento {

	private Main main = new Main()
	private Window_NuevoDocumento  nuevoDocumento  = new Window_NuevoDocumento()
	private PopUp_DocumentoCreado documentoCreado = new PopUp_DocumentoCreado()
	private Window_Documento documento = new Window_Documento()

	@Keyword
	def crearNuevoDocumento(String tipoDocumento, String minutos, String coordenadas){
		main.clickMenuNuevoDocumento()
		nuevoDocumento.generarNuevoDocumento(tipoDocumento, minutos, coordenadas)
		documentoCreado.verificarDocumentoCreado()
		documentoCreado.clickAbrir()
	}

	@Keyword
	def crear(String tipoDocumento, String coordenadas){
		main.clickMenuNuevoDocumento()
		nuevoDocumento.generarNuevoDocumento(tipoDocumento, coordenadas)
		documentoCreado.verificarDocumentoCreado()
		documentoCreado.clickAbrir()
		documento.esperarVentanaAbierta()
	}
	
	@Keyword
	def ingresarDatos(String tipoDocumento){
		main.clickMenuNuevoDocumento()
		nuevoDocumento.ingresarTipoDocumento(tipoDocumento)
		nuevoDocumento.seleccionarCoordenadas()
	}
	
	@Keyword
	def seleccionarElementoCromo(String zona, String partido, String localidad, String idElemento){
		new Page_Main().seleccionarCoordenadasElemento(zona, partido, localidad, idElemento)
	}

	@Keyword
	def finalizarCreacion(){
		nuevoDocumento.focus()
		nuevoDocumento.clickAceptar()
	}
	
	@Keyword
	def guardarNumeroDeDocumento(){
		documento.guardarNumeroDocumento()
	}
	
	@Keyword
	def abrirNuevoDocumento(){
		documentoCreado.verificarDocumentoCreado()
		documentoCreado.clickAbrir()
		documento.esperarVentanaAbierta()
		documento.getNumeroDocumento()
	}
}

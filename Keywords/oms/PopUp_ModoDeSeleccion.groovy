package oms
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable
import com.kms.katalon.core.util.KeywordUtil

public class PopUp_ModoDeSeleccion extends helper.UIContext{
	protected static final String IMAGENES_OMS_MODO_DE_SELECCION =  GlobalVariable.IMAGENES + "OMS\\PopUp\\ModoDeSeleccion\\"
	private static final String IMG_TOPOLOGICA = IMAGENES_OMS_MODO_DE_SELECCION + "BotonTopologica.PNG"
	private static final String IMG_GEOGRAFICA = IMAGENES_OMS_MODO_DE_SELECCION + "BotonGeografica.PNG"

	def seleccionarTipoDeAfectacion(String tipoAfectacion){
		if (tipoAfectacion == "Topologica")
			clickTopologica()
		else if (tipoAfectacion == "Geografica")
			clickGeografica()
		else
			KeywordUtil.markFailedAndStop("Tipo de Afectacion no valido.")
	}

	def clickTopologica(){
		clickElement(IMG_TOPOLOGICA)
	}

	def clickGeografica(){
		clickElement(IMG_GEOGRAFICA)
	}
}
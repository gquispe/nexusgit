package oms

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import cromo.Page_Main

import internal.GlobalVariable

public class ProcesoCreacionAnomalia {
	private Window_EditarAnomalia anomalia = new Window_EditarAnomalia()
	private Window_Documento_Tab_Anomalias tab = new Window_Documento_Tab_Anomalias()

	@Keyword
	def inicio(){
		tab.clickNuevaAnomalia()
		anomalia.clickBotonVerde()
	}

	@Keyword
	def seleccionAnomalia(String localidad, String suministro){
		new Page_Main().seleccionarElementoAfectado(localidad, suministro)
	}

	@Keyword
	def seleccionAnomalia(String zona, String partido, String localidad, String idElemento) {
		new Page_Main().seleccionarAnomalia(zona, partido, localidad, idElemento)
	}

	@Keyword
	def ingresarDatos(calle, entreCalle1, instalacion, tipoAnomalia, clasificacion, prioridad){
		anomalia.focus()
		anomalia.ingresarAnomalia(calle, entreCalle1, instalacion, tipoAnomalia, clasificacion, prioridad)
		tab.clickAceptar()
	}
}

package oms

import org.sikuli.script.Screen

import com.kms.katalon.core.annotation.Keyword

import helper.ScreenDriver
import internal.GlobalVariable
import junit.framework.Assert
import com.kms.katalon.core.util.KeywordUtil

public class Window_Regiones extends Main{

	private static String IMG_ICO_GUARDAR = IMAGENES_OMS + "GuardarYSalir.png"

	Window_Regiones(){
		focus()
	}

	@Keyword
	def guardarRegionesSeleccionadas(){
		clickElement(IMG_ICO_GUARDAR)
	}

	@Keyword
	def verificarTiempoDeInicioOMS(){
		screen.wait(IMG_ICO_GUARDAR,10)
		Long elapasedTime = (System.currentTimeMillis() - GlobalVariable.START_TIME)/1000
		KeywordUtil.logInfo(">>> Tiempo de carga: " + String.valueOf(elapasedTime) + " segundos")
		if (elapasedTime < 45)
			KeywordUtil.markPassed("Tiempo de inicio inferior a 45 segundos")
		else
			KeywordUtil.markFailed("Tiempo de inicio superior a 45 segundos")
	}
}

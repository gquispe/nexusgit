package oms

import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.sikuli.script.Env
import org.sikuli.script.Key
import org.sikuli.script.Screen
import org.sikuli.script.Pattern
import org.sikuli.script.Region
import org.sikuli.script.App
import org.sikuli.script.KeyModifier

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.util.KeywordUtil

import helper.DocUtils
import internal.GlobalVariable
import helper.ExcelData


public class Window_Documento extends helper.UIContext{
	public static Screen driver = getIntance()

	private static final String EXCEL_PATH = "C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\DatosAnteriores.xlsx"
	private static final String CC2_EXCEL_PATH = "C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\CC2.xlsx"

	private ExcelData excel = new ExcelData()

	protected static final String IMG_OMS_DOCUMENTO = GlobalVariable.IMAGENES + "OMS\\Windows\\Documento\\"
	private static final String IMG_AFECTAR		 	  = IMG_OMS_DOCUMENTO + "MenuBotonAfectar.PNG"
	private static final String IMG_OPERAR 		 	  = IMG_OMS_DOCUMENTO + "MenuBotonOperar.PNG"
	private static final String IMG_DESPACHOMOVIL     = IMG_OMS_DOCUMENTO + "MenuDespachoMovil.PNG"
	private static final String IMG_MODO_REGISTRACION = IMG_OMS_DOCUMENTO + "MenuModoRegistracion.PNG"
	private static final String IMG_PANEL_ICONOS      = IMG_OMS_DOCUMENTO + "Iconos_Panel.PNG"
	private static final String IMG_SUBPANEL_FECHAS   = IMG_OMS_DOCUMENTO + "Fechas.PNG"
	private static final String IMG_BUTTON_ACTUALIZAR = IMG_OMS_DOCUMENTO + "Actualizar.PNG"
	private static final String IMG_CLOSE_WINDOW 	  = IMG_OMS_DOCUMENTO + "CerrarVentana.PNG"
	private static final String IMG_ACTIVIDADES		  = IMG_OMS_DOCUMENTO + "Actividades.PNG"
	private static final String IMG_OMS_NUEVO_TRABAJO = IMG_OMS_DOCUMENTO + "NuevoTrabajo.PNG"
	private static final String IMG_OMS_ENCABEZADO	  = IMG_OMS_DOCUMENTO + "Encabezado.PNG"
	private static final String IMG_OMS_REPOSICIONES  = IMG_OMS_DOCUMENTO + "Reposiciones.PNG"
	private static final String IMG_OMS_CERRAR 		  = IMG_OMS_DOCUMENTO + "Cerrar.PNG"
	private static final String IMG_OMS_SUBMENU		  = IMG_OMS_DOCUMENTO + "Submenu.PNG"
	private static final String IMG_OMS_RESTAURACION  = IMG_OMS_DOCUMENTO + "Restauracion.PNG"
	private static final String IMG_OMS_CIERRE_DEFINITIVO = IMG_OMS_DOCUMENTO + "CierreDefinitivo.PNG"
	private static final String IMG_OMS_COMENTARIOS = IMG_OMS_DOCUMENTO + "comentarios.PNG"
	private static final String IMG_OMS_CIERRE_PROVISORIO = IMG_OMS_DOCUMENTO + "cierreprovisorio.PNG"
	private static final String IMG_OMS_RECLAMOS = IMG_OMS_DOCUMENTO + "reclamos.PNG"
	private static final String IMG_OMS_CIERRE_DEFINITIVO_POSTOPERACION = IMG_OMS_DOCUMENTO + "CierreDefinitivoPostOperacion.PNG"
	private static final String IMG_AFECTADOSALCOMIENZO = IMG_OMS_DOCUMENTO + "AfectadosAlComienzo.PNG"
	private static final String IMG_AFECTADOSAHORA = IMG_OMS_DOCUMENTO + "AfectadosAhora.PNG"
	private static final String IMG_RECLAMOSABIERTOS = IMG_OMS_DOCUMENTO + "ReclamosAbiertos.PNG"
	private static final String IMG_OMS_TOMAR  = IMG_OMS_DOCUMENTO + "tomar.PNG"
	private static final String IMG_OMS_ANOMALIAS 		  = IMG_OMS_DOCUMENTO + "anomalias.PNG"
	private static final String IMG_OMS_ESQUINA		  = IMG_OMS_DOCUMENTO + "esquina.PNG"
	private static final String IMS_OMS_POPUP_DATOSMODIFICADOS = IMG_OMS_DOCUMENTO + "PopUp_DatosModificadosExitosamente.PNG"
	private static final String IMS_OMS_EQUIPOS_MOVILES= IMG_OMS_DOCUMENTO + "equiposmoviles.PNG"
	private static final String IMS_TASKBAR_ICON= IMG_OMS_DOCUMENTO + "taskbaricon.PNG"
	private static final String IMS_MENU= IMG_OMS_DOCUMENTO + "menu.PNG"
	private static final String IMS_RECUPERAR_RECLAMOS= IMG_OMS_DOCUMENTO + "recuperarreclamos.PNG"
	private static final String IMS_CANCELADO= IMG_OMS_DOCUMENTO + "cancelado.PNG"
	private static final String IMS_LIBERAR= IMG_OMS_DOCUMENTO + "liberar.PNG"
	private static final String IMG_ZOOM= IMG_OMS_DOCUMENTO + "zoom.PNG"
	private static final String IMG_NUMERO_DOCUMENTO= IMG_OMS_DOCUMENTO + "NumeroDocumento.PNG"
	

	//	Window_Documento(){
	//		checkThatIsVisible(IMG_BUTTON_ACTUALIZAR,"Boton Actualizar")
	//	}

	@Keyword
	def focus(){
		App.focus("D-")
		App.focus("D-")
		driver.type(Key.ENTER)
	}

	@Keyword
	def clickEncabezado(){
		clickElement(IMG_OMS_ENCABEZADO)
	}

	@Keyword
	def clickActividades(){
		clickElement(IMG_ACTIVIDADES)
	}

	@Keyword
	def clickReclamos(){
		driver.wait(IMG_OMS_REPOSICIONES,GlobalVariable.DEFAULT_WAIT)
		Region afectados = driver.find(IMG_OMS_REPOSICIONES)
		afectados.setX(afectados.getX() + afectados.getW())
		afectados.click()
		Thread.sleep(1000)
	}

	@Keyword
	def clickDespachoMovil(){
		clickElement(IMG_DESPACHOMOVIL)
	}

	@Keyword
	def clickAfectar(){
		clickElement(IMG_AFECTAR)
	}

	@Keyword
	def clickRestauracion(){
		driver.find(IMG_AFECTAR).click(IMG_OMS_SUBMENU)
		clickElement(IMG_OMS_RESTAURACION)
	}

	@Keyword
	def clickOperar(){
		clickElement(IMG_OPERAR)
	}

	@Keyword
	def clickNuevoTrabajo(){
		clickElement(IMG_OMS_NUEVO_TRABAJO)
	}

	@Keyword
	def clickCerrar(){
		Thread.sleep(1000)
		clickElement(IMG_OMS_CERRAR)
		Thread.sleep(1000)
	}

	@Keyword
	def clickModificarModoRegistracion(){
		clickElement(IMG_MODO_REGISTRACION)
	}

	@Keyword
	def verificarCierreDefinitivo(){
		checkThatIsVisibleWithTime(IMG_OMS_CIERRE_DEFINITIVO, "Cierre Definitivo", 5)
	}

	@Keyword
	def actualizarRestauracion(String horasRestauracion){
		modificarRestauracion(horasRestauracion)
		clickActualizar()
		datosModificadosExitosamente()
	}

	def modificarRestauracion(String horasRestauracion){
		seleccionarTiempoRestauracion()
		driver.type(horasRestauracion)
	}

	def seleccionarTiempoRestauracion(){
		seleccionarSubpanelFechas()
		tab(5)
	}

	@Keyword
	def clickActualizar(){
		clickElement(IMG_BUTTON_ACTUALIZAR)
	}

	def datosModificadosExitosamente(){
		if(verifyElementIsVisible(IMS_OMS_POPUP_DATOSMODIFICADOS)){
			driver.type(Key.ENTER)
		}
	}

	def seleccionarSubpanelFechas(){
		driver.click(IMG_SUBPANEL_FECHAS)
	}

	@Keyword
	def cerrarVentana(){
		if (verifyElementIsVisible(IMG_PANEL_ICONOS,GlobalVariable.MINIMUM_WAIT))
			driver.click(IMG_CLOSE_WINDOW)
	}

	@Keyword
	def guardarNumeroDocumento(){
		esperarVentanaAbierta()
		driver.type("c", Key.CTRL);
		String numeroDocumento = Env.getClipboard()
		excel.guardarNumeroDeDocumento(numeroDocumento)
		KeywordUtil.logInfo("Numero Documento: " + numeroDocumento)
	}

	def esperarVentanaAbierta(){
		checkThatIsVisible(IMG_BUTTON_ACTUALIZAR,"Boton Actualizar")
	}

	@Keyword
	def verificarClientesRestaurados(){
		Thread.sleep(2000)
		tab(34)
		Thread.sleep(1000)
		driver.type("c", Key.CTRL);
		driver.type("c", Key.CTRL);
		String clipboard = Env.getClipboard()
		String restaurados = clipboard.substring(clipboard.lastIndexOf("\t")+1)
		KeywordUtil.logInfo(">>> Restaurados actuales: " + restaurados)
		FileInputStream file = new FileInputStream (new File(EXCEL_PATH))
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		XSSFSheet sheet = workbook.getSheetAt(0);
		String restauradosAnteriores=sheet.getRow(1).getCell(1).getStringCellValue();
		KeywordUtil.logInfo(">>> Restaurados anteriores: " + restauradosAnteriores)
		sheet.getRow(1).createCell(1).setCellValue(restaurados);
		file.close();
		FileOutputStream outFile =new FileOutputStream(new File(EXCEL_PATH));
		workbook.write(outFile);
		outFile.close();
		checkThatNotEquals(restauradosAnteriores, restaurados, "RESTAURADOS")
	}

	@Keyword
	def verificarCierreProvisorio(){
		checkThatIsVisibleWithTime(IMG_OMS_CIERRE_PROVISORIO, "Cierre Provisorio", 5)
	}

	@Keyword
	def verificarCierrePostoperacion(){
		checkThatIsVisibleWithTime(IMG_OMS_CIERRE_DEFINITIVO_POSTOPERACION, "Cierre post operacion", 5)
	}

	@Keyword
	def guardarDocumento(){
		Thread.sleep(2000)
		driver.type("c", Key.CTRL);
		driver.type("c", Key.CTRL);
		String clipboard = Env.getClipboard()
		DocUtils.writeOldCell(CC2_EXCEL_PATH, 1, 1, 0, clipboard)
	}

	@Keyword
	def verificarNumeroDeDocumento(String path, int sheet, int row,int column){
		driver.type("c", Key.CTRL);
		driver.type("c", Key.CTRL);
		String clipboard = Env.getClipboard();
		String doc = DocUtils.readCell(path, sheet, row, column);
		checkThatEquals(clipboard, doc, "DOCUMENTO GENERADO");
	}

	@Keyword
	def getAfectadosAlComienzo(){
		Region afectados = driver.find(IMG_AFECTADOSALCOMIENZO)
		afectados.setX(afectados.getX() + afectados.getW())
		afectados.highlight(2)
		afectados.doubleClick()
	}

	@Keyword
	def verificarNoAfectadosActuales(){
		checkThatEquals(getAfectadosAhora(),"0","Revision Afectados")
	}

	@Keyword
	def verificarNoReclamosAbiertosActuales(){
		checkThatEquals(getReclamosAbiertos(),"0","Revision Reclamos actuales")
	}
	@Keyword
	public String getAfectadosAhora(){
		return getField(IMG_AFECTADOSAHORA)
	}

	@Keyword
	public String getReclamosAbiertos(){
		return getField(IMG_RECLAMOSABIERTOS)
	}

	public String getField(String imageField){
		Region reclamosAbiertos = getRightRegion(imageField)
		reclamosAbiertos.doubleClick()
		copiarSeleccion()
		String clipboard = Env.getClipboard()
		return clipboard
	}

	@Keyword
	def clickTomar(){
		clickElement(IMG_OMS_TOMAR)
		Thread.sleep(1000)
	}

	@Keyword
	def tabAuditoria(){
		clickElement(IMG_OMS_ENCABEZADO)
		end(1)
		Thread.sleep(1000)
	}

	@Keyword
	def guardarDocumento(path, sheet, row, column){
		Thread.sleep(2000)
		driver.type("c", Key.CTRL);
		driver.type("c", Key.CTRL);
		String clipboard = Env.getClipboard()
		DocUtils.writeOldCell(path, sheet, row, column, clipboard)
	}

	@Keyword
	def clickAnomalias(){
		clickElement(IMG_OMS_ANOMALIAS)
	}

	@Keyword
	def clickCerrarVentana(){
		clickElement(IMG_CLOSE_WINDOW)
		Thread.sleep(1000)
	}

	@Keyword
	def clickEsquina(){
		clickElement(IMG_OMS_ESQUINA)
	}

	@Keyword
	def verificarCliente(String path, int sheet, int row,int column){
		Thread.sleep(1000)
		tab(1)
		driver.type("c", Key.CTRL);
		driver.type("c", Key.CTRL);
		String clipboard = Env.getClipboard();
		String doc = DocUtils.readCell(path, sheet, row, column);
		checkThatEquals(clipboard, doc, "CLIENTE");
	}

	@Keyword
	def cerrarVentanaForzado(){
		Thread.sleep(1000)
		KeywordUtil.logInfo(" >>> CERRANDO VENTANA")
		driver.type(Key.F4, KeyModifier.ALT);
		Thread.sleep(1000)
	}

	@Keyword
	def clickEquiposMoviles(){
		clickElement(IMS_OMS_EQUIPOS_MOVILES)
		Thread.sleep(1000)
	}

	@Keyword
	def clickTaskBarIcon(){
		clickElement(IMS_TASKBAR_ICON)

	}

	@Keyword
	def focusOnDocument(){
		if(!verifyElementIsVisible(IMS_MENU)){
			clickTaskBarIcon()
		}
	}

	@Keyword
	def clickRecuperarReclamos(){
		clickElement(IMS_RECUPERAR_RECLAMOS)
	}

	@Keyword
	def verificarDocumentoCancelado(){
		checkThatIsVisibleWithTime(IMS_CANCELADO, "Docuemnto Cancelado", 5)
	}

	@Keyword
	def clickLiberar(){
		verifyAndClick(IMS_LIBERAR)
	}
	
	@Keyword
	def clickZoom(){
		clickElement(IMG_ZOOM)
	}
	
	@Keyword
	public String getNumeroDocumento(){
		Region campoNumeroDocumento = getRightRegion(IMG_NUMERO_DOCUMENTO)
		driver.click(campoNumeroDocumento)
		selectField()
		String numeroDocumento = copiarSeleccion()
		KeywordUtil.logInfo("Numero de Documento: " + numeroDocumento)
		return numeroDocumento
	}
}

package oms

import org.sikuli.script.Key

import com.kms.katalon.core.annotation.Keyword
import org.sikuli.script.Region

import internal.GlobalVariable

public class Window_Documento_Tab_Reclamos extends helper.UIContext{

	protected static final String IMG_OMS_RECLAMOS = GlobalVariable.IMAGENES + "OMS\\Windows\\Documento\\TAB_Reclamos\\"
	private static final String IMG_CERRAR	 	   = IMG_OMS_RECLAMOS + "Cerrar.PNG"
	private static final String IMG_CERRARGRISADO  = IMG_OMS_RECLAMOS + "CerrarGrisado.PNG"
	private static final String IMG_DETALLES 	   = IMG_OMS_RECLAMOS + "detalles.PNG"
	private static final String IMG_DOCUMENTO 	   = IMG_OMS_RECLAMOS + "Documento.PNG"
	private static final String IMG_SENSIBLE	   = IMG_OMS_RECLAMOS + "sensible.PNG"
	private static final String IMG_MOVER_RECLAMOS	   = IMG_OMS_RECLAMOS + "moverreclamos.PNG"

	@Keyword
	def clickCerrar(){
		clickElement(IMG_CERRAR)
	}

	@Keyword
	def seleccionarPrimerDocumento(){
		driver.type(Key.TAB)
	}

	@Keyword
	def seleccionarReclamo(String numeroReclamo){
		driver.wait(IMG_DOCUMENTO,GlobalVariable.MINIMUM_WAIT)
		Region region = driver.find(IMG_DOCUMENTO)
		region = selectElementFromList(region,Integer.parseInt(numeroReclamo))
		driver.click(region)
	}

	@Keyword
	def esperarReclamoCerrado(){
		driver.wait(IMG_CERRARGRISADO,GlobalVariable.DEFAULT_WAIT)
	}

	@Keyword
	def clickDetalles(){
		driver.doubleClick(IMG_DETALLES)
	}

	@Keyword
	def seleccionarDocumento(index){
		Window_Documento documento = new Window_Documento()
		documento.clickReclamos()
		driver.type(Key.TAB)
		down(index)
	}

	@Keyword
	def verificarClienteSensible(){
		checkThatIsVisibleWithTime(IMG_SENSIBLE, "Cliente Sensible", 5)
	}
	
	@Keyword
	def clickMoverReclamos(){
		driver.doubleClick(IMG_MOVER_RECLAMOS)
	}
}

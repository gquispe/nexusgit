package oms

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class ProcesoOperacion {

	private Window_OperarElemento windowOperarElemento 	= new Window_OperarElemento()

	@Keyword
	def inicioAperturaDocumento(){
		inicioProceso()
	}

	@Keyword
	def finalizarAperturaDocumento(){
		windowOperarElemento.seleccionarManiobra()
		windowOperarElemento.seleccionarAccionAbrir()
		windowOperarElemento.setFechaHora()
		finalizarProceso()
	}

	@Keyword
	def inicioCierreDocumento(){
		new Window_Documento().focus()
		new Window_Documento().clickEncabezado()
		new Window_Documento().clickOperar()
	}

	@Keyword
	def finalizarCierreDocumento(){
		windowOperarElemento.seleccionarElementoAbierto()
		windowOperarElemento.operarElemento()
		windowOperarElemento.seleccionarOpcionSi()
		windowOperarElemento.esperarProceso()
		windowOperarElemento.checkPopUpNoReclamosAbiertos()
		windowOperarElemento.clickSalir()
	}

	def inicioProceso(){
		new Window_Documento().clickEncabezado()
		new Window_Documento().clickOperar()
		windowOperarElemento.seleccionarElementoClienteGrafico()
	}

	def finalizarProceso(){
		windowOperarElemento.clickAceptar()
		windowOperarElemento.esperarProceso()
		windowOperarElemento.clickSalir()
	}
}

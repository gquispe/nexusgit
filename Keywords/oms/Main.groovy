package oms

import java.awt.*

import org.sikuli.script.App
import org.sikuli.script.Key
import org.sikuli.script.KeyModifier
import org.sikuli.script.Screen

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.util.KeywordUtil

import helper.CodReclamos
import helper.DocUtils
import helper.ScreenDriver
import internal.GlobalVariable

public class Main  extends helper.UIContext{

	protected static Screen screen = ScreenDriver.getIntance()
	private static final String TITULO_VENTANA = "OMS: Outage Management System"
	protected static final String IMAGENES_OMS =  GlobalVariable.IMAGENES + "OMS\\"
	private static final String IMG_ICO_OMS = IMAGENES_OMS + "OMS-Barra.PNG"
	private static final String IMG_TITLE = IMAGENES_OMS + "titulo-label.png"
	private static final String IMG_WINDOW_ERROR = IMAGENES_OMS + "window_main.PNG"
	private static final String IMG_PANEL_COMPRIMIDO = IMAGENES_OMS + "Panel_Comprimido.PNG"
	private static final String IMG_PANEL_DOCUMENTOS = IMAGENES_OMS + "Panel_Documentos.PNG"
	private static final String IMG_PANEL_HERRAMIENTAS = IMAGENES_OMS + "Panel_Herramientas.PNG"
	private static final String IMG_SITUACION_MONITOR = IMAGENES_OMS + "Situacion-Monitor.PNG"
	private App omsApp = new App("C:\\4DataLink\\4DL-OMS\\OMSApplication.exe")
	private static final long TIEMPO_MINIMO_DE_CARGA = 45

	private static String IMG_MENU_NUEVO_DOCUMENTO = GlobalVariable.IMAGENES + "OMS\\Menu_NuevoDocumento.png"

	private static final String IMG_PANEL_DOCUMENTOS_ABIERTOS_PROG_BT = IMAGENES_OMS + "AbiertosProgBT.PNG"
	private static final String IMG_PANEL_DOCUMENTOS_COLUMNA_NUMERO = IMAGENES_OMS + "DocumentosColumnaNumero.PNG"

	private static final String IMG_ICO_VER_DOCUMENTO = GlobalVariable.IMAGENES + "OMS\\icon_ver_documento.PNG"
	private static final String IMG_DOCUMENTOS_PROGRAMABLES = IMAGENES_OMS + "DocumentosProgramables.PNG"
	private static final String IMG_DOCUMENTOS_PROGRAMABLES_SELECCIONADO= IMAGENES_OMS + "ProgramablesSeleccionado.PNG"
	private static final String IMG_TITULO_SIN_DATOS= IMAGENES_OMS + "titulodoscumentossindatos.PNG"

	@Keyword
	def focus() {
		screen.type("d", KeyModifier.WIN);
		screen.wait(IMG_ICO_OMS,GlobalVariable.MAXIMUM_WAIT)
		App.focus(TITULO_VENTANA)
	}

	@Keyword
	def clickTitle(){
		clickElement(IMG_TITLE)
	}

	@Keyword
	def verificarInicioDeOMS(){
		checkThatIsVisibleWithTime(IMG_TITLE,"OMS TITULO VENTANA",GlobalVariable.MINIMUM_WAIT)
	}


	@Keyword
	def clickHerramientas(){
		comprimirPanel()
		checkThatIsVisibleWithTime(IMG_PANEL_HERRAMIENTAS,"Opcion Herramientras",GlobalVariable.MINIMUM_WAIT)
		screen.doubleClick(IMG_PANEL_HERRAMIENTAS)
	}

	@Keyword
	def openSituacionMonitor(){
		checkThatIsVisibleWithTime(IMG_SITUACION_MONITOR,"Opcion Situacion Monitor",GlobalVariable.MINIMUM_WAIT)
		screen.click(IMG_SITUACION_MONITOR)
	}


	def verificarTiempoDeCarga(){
		Long elapasedTime = (System.currentTimeMillis() - GlobalVariable.START_TIME)/1000
		KeywordUtil.logInfo(">>> Tiempo de carga: " + String.valueOf(elapasedTime) + " segundos")
		checkThatLessThan(elapasedTime, TIEMPO_MINIMO_DE_CARGA, "Tiempo de carga es menor a 45 segundos")
	}

	@Keyword
	def comprimirPanel(){
		if (!verifyElementIsVisible(IMG_PANEL_COMPRIMIDO,"Panel Comprimido",GlobalVariable.MINIMUM_WAIT)){
			screen.doubleClick(IMG_PANEL_DOCUMENTOS)
		}
	}

	@Keyword
	def clickMenuNuevoDocumento(){
		clickElement(IMG_TITULO_SIN_DATOS)
		clickElement(IMG_MENU_NUEVO_DOCUMENTO,GlobalVariable.MINIMUM_WAIT)
	}

	@Keyword
	def clickAbiertosPrgBT(){
		clickElement(IMG_PANEL_DOCUMENTOS_ABIERTOS_PROG_BT,GlobalVariable.MINIMUM_WAIT)
	}

	@Keyword
	def clickColumnaNumero(){
		clickElement(IMG_PANEL_DOCUMENTOS_COLUMNA_NUMERO,GlobalVariable.MINIMUM_WAIT)
	}

	@Keyword
	def selecionarPrimerDocumento(){
		screen.type(Key.HOME)
	}

	@Keyword
	def clickVerDetalles() {
		clickElement(IMG_ICO_VER_DOCUMENTO)
		Thread.sleep(2000)
	}

	@Keyword
	def checkCodigoReclamo(String reclamo){
		KeywordUtil.logInfo(reclamo)
		if (reclamo[0]!="R"){
			KeywordUtil.markWarning("Codigo de Reclamo Invalido")
			KeywordUtil.markFailedAndStop("")
		}
	}

	@Keyword
	def selecionarUltimoDocumento(){
		Thread.sleep(6000)
		screen.type(Key.END)
		screen.type(Key.END)
	}

	@Keyword
	def buscarDocumento(doc){
		clickTitle()
		KeywordUtil.logInfo(">>> Buscando Documento: " + doc)
		Thread.sleep(1000)
		tab(2)
		screen.type(doc)
		screen.type(Key.ENTER)
		Thread.sleep(3000)
	}

	@Keyword
	def clickDocumentosProgramables(){
		comprimirPanel()
		checkThatIsVisibleWithTime(IMG_DOCUMENTOS_PROGRAMABLES,"Documentos programables",GlobalVariable.MINIMUM_WAIT)
		screen.doubleClick(IMG_DOCUMENTOS_PROGRAMABLES)
	}

	@Keyword
	def clickDocumentosProgramables2Pendientes(){
		clickDocumentosProgramables()
		screen.type(Key.DOWN)
		screen.type(Key.DOWN)
		screen.type(Key.DOWN)
		clickElement(IMG_DOCUMENTOS_PROGRAMABLES_SELECCIONADO)
		Thread.sleep(5000)
	}

	@Keyword
	def buscarReclamo(rec){
		clickTitle()
		KeywordUtil.logInfo(">>> Buscando reclamo: " + rec)
		Thread.sleep(1000)
		tab(4)
		screen.type(rec)
		screen.type(Key.ENTER)
	}

	@Keyword
	def buscarDocumentoGuardado(path, docSheet, row, column){
		String doc = DocUtils.readCell(path, docSheet, row, column)
		clickTitle()
		KeywordUtil.logInfo(">>> Buscando Documento: " + doc)
		Thread.sleep(1000)
		tab(2)
		driver.type("a",KeyModifier.CTRL)
		screen.type(doc)
		screen.type(Key.ENTER)
		Thread.sleep(3000)
	}

	@Keyword
	def buscarReclamo(path, docSheet, row, column){
		Thread.sleep(1000)
		String rec = DocUtils.readCell(path, docSheet, row, column)
		clickTitle()
		KeywordUtil.logInfo(">>> Buscando reclamo: " + rec)
		Thread.sleep(1000)
		tab(4)
		screen.type(rec)
		screen.type(Key.ENTER)
	}
}

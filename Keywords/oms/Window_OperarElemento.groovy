package oms

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import helper.ExcelData

import org.sikuli.api.API
import org.sikuli.script.App
import org.sikuli.script.Button
import org.sikuli.script.Region
import org.sikuli.script.Screen
import org.sikuli.script.FindFailed
import org.sikuli.script.Key
import org.sikuli.script.KeyModifier
import org.sikuli.script.Match
import org.sikuli.script.Pattern
import internal.GlobalVariable
import java.text.SimpleDateFormat
import java.util.Date

public class Window_OperarElemento extends Window_SeleccionTopologica{

	protected static final String IMAGENES_OPERAR	 = GlobalVariable.IMAGENES + "OMS\\Windows\\OperarElemento\\"
	private static final String IMG_UTILS_DROPDOWN	 = GlobalVariable.IMAGENES + "Utils\\Dropdown.PNG"

	private static final String IMG_OPTION_MANIOBRA  = IMAGENES_OPERAR + "Maniobra.PNG"
	private static final String IMG_DROPDOWN_ACCION  = IMAGENES_OPERAR + "Accion.PNG"
	private static final String IMG_BUTTON_ACEPTAR   = IMAGENES_OPERAR + "Aceptar.PNG"
	private static final String IMG_BUTTON_SALIR     = IMAGENES_OPERAR + "Salir.PNG"
	private static final String IMG_POPUP_ESPERAR    = IMAGENES_OPERAR + "PopUp_Esparar.PNG"
	private static final String IMG_POPUP_NORECLAMOS = IMAGENES_OPERAR + "NoMasReclamosAbiertos.PNG"
	private static final String IMG_OPTION_NO 		 = IMAGENES_OPERAR + "No.PNG"
	private static final String IMG_OPTION_SI 		 = IMAGENES_OPERAR + "Si.PNG"
	private static final String IMG_OPTION_FECHAHORA = IMAGENES_OPERAR + "FechaHora.PNG"
	private static final String IMG_SET_FECHAHOY 	 = IMAGENES_OPERAR + "FechaHoy.PNG"
	private static final String IMG_OPTION_ABRIR	 = IMAGENES_OPERAR + "Abrir.PNG"
	private static final String IMG_OPERAR 		 	 = GlobalVariable.IMAGENES + "OMS\\Documento\\" + "MenuBotonOperar.PNG"

	private String timeStamp = new SimpleDateFormat("HHmmssSSS").format(new Date())

	private ExcelData excelData = new ExcelData()

	@Keyword
	def seleccionarManiobra(){
		clickElement(IMG_OPTION_MANIOBRA)
	}

	@Keyword
	def seleccionarAccionAbrir(){
		seleccionarAccion("Abrir")
	}

	@Keyword
	def seleccionarAccionCerrar(){
		seleccionarAccion("Cerrar")
	}

	@Keyword
	def setFechaHora(){
		Region fechaHora = getRightRegion(IMG_OPTION_FECHAHORA)
		fechaHora.doubleClick()
		driver.type(getTime())
	}

	@Keyword
	def getTime(){
		return new SimpleDateFormat("ddMMyyyyHHmm").format(new Date())
	}

	def seleccionarAccion(int index){
		clickElement(IMG_DROPDOWN_ACCION)
		seleccionarDropDown(index)
		driver.type(Key.ENTER)
	}

	def seleccionarAccion(String accion){
		clickElement(IMG_DROPDOWN_ACCION)
		driver.type(accion)
		driver.type(Key.ENTER)
	}

	def clickAceptar(){
		clickElement(IMG_BUTTON_ACEPTAR)
	}

	def esperarProceso(){
		driver.wait(IMG_POPUP_ESPERAR)
	}

	def seleccionarElementoAbierto(){
		clickElement(IMG_OPTION_ABRIR)
	}

	def operarElemento(){
		clickElement(IMG_OPERAR)
	}

	def seleccionarOpcionSi(){
		clickElement(IMG_OPTION_SI)
	}

	def checkPopUpNoReclamosAbiertos(){
		if(verifyElementIsVisible(IMG_POPUP_NORECLAMOS)){
			driver.click(IMG_OPTION_NO)
		}
		//		else{
		//			takeScreenshot("Error Pop Up de reclamos cerrados")
		//			KeywordUtil.markFailedAndStop("Pop up de Reclamos cerrados no visible")
		//		}
	}

	def clickSalir(){
		clickElement(IMG_BUTTON_SALIR)
	}
}

package oms

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.sikuli.script.App

import internal.GlobalVariable
import com.kms.katalon.core.util.KeywordUtil

public class ProcesoAfectacionRestauracion{

	private Window_Documento windowDocumento				     = new Window_Documento()
	private PopUp_ModoDeSeleccion windowModoDeSeleccion		 = new PopUp_ModoDeSeleccion()
	private Window_SeleccionTopologica windowSeleccionTopologica = new Window_SeleccionTopologica()
	private Window_AfectarDocumento windowAfectarDocumento 	     = new Window_AfectarDocumento()

	@Keyword
	def inicioAfectacion(String tipoSeleccion){
		windowDocumento.clickAfectar()
		inicioSeleccion(tipoSeleccion)
	}

	@Keyword
	def inicioRestauracion(String tipoSeleccion){
		windowDocumento.clickRestauracion()
		inicioSeleccion(tipoSeleccion)
	}

	def inicioSeleccion(String tipoSeleccion){
		windowModoDeSeleccion.seleccionarTipoDeAfectacion(tipoSeleccion)
		windowSeleccionTopologica.seleccionarElementoClienteGrafico()
	}

	@Keyword
	def finalizarAfectacion(){
		finSeleccion()
	}

	@Keyword
	def finalizarRestauracion(){
		finSeleccion()
	}

	def finSeleccion(){
		windowSeleccionTopologica.clickAceptarElementoSeleccionado()
		windowAfectarDocumento.clickAceptarAfectacionDocumento()
	}
	
	@Keyword
	def esperarAfectacion(secs){
		Thread.sleep(secs * 1000)
	}
}

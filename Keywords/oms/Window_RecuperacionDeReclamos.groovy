package oms

import java.text.SimpleDateFormat

import org.sikuli.script.Key

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.util.KeywordUtil

import internal.GlobalVariable

public class Window_RecuperacionDeReclamos extends helper.UIContext{

	protected static final String IMG_RECUPERACION_DE_RECLAMOS =  GlobalVariable.IMAGENES + "OMS\\Windows\\RecuperacionDeReclamos\\"
	private static final String IMG_ACEPTAR   	 = IMG_RECUPERACION_DE_RECLAMOS + "aceptar.PNG"

	@Keyword
	def restarMinutos(min){
		Calendar calendar = Calendar.getInstance();
		KeywordUtil.logInfo(">>> Getting time: " + calendar.getTime());
		calendar.add(Calendar.MINUTE, Integer.parseInt(min) * (-1));
		KeywordUtil.logInfo(">>> Decreasing time by " + min + " = "+ calendar.getTime());
		SimpleDateFormat omsFormat = new SimpleDateFormat("ddMMyyyyhhmm");
		String formatted = omsFormat.format(calendar.getTime());
		KeywordUtil.logInfo(">>> Formating time for entry: " + formatted);
		driver.type(Key.BACKSPACE)
		driver.type(formatted)
	}

	@Keyword
	def clickAceptar(){
		clickElement(IMG_ACEPTAR)
		Thread.sleep(2000)
	}
	
	@Keyword
	def aceptarPopUp(){
		enter(1)
	}
}

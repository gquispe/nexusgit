package oms

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class Window_Documento_Tab_EquiposMoviles extends helper.UIContext{

	protected static final String IMG_OMS_EM = GlobalVariable.IMAGENES + "OMS\\Documento\\TAB_EquiposMoviles\\"
	private static final String IMG_DESPACHADO	 	  = IMG_OMS_EM + "despachado.PNG"
	private static final String IMG_CANCELAR 	  = IMG_OMS_EM + "cancelar.PNG"
	private static final String IMG_CANCELADO 	  = IMG_OMS_EM + "cancelado.PNG"
	private static final String IMG_SI	  = IMG_OMS_EM + "si.PNG"
	private static final String IMG_DESPACHAR	  = IMG_OMS_EM + "despachar.PNG"
	private static final String IMG_MULTIPLE_DESPACHO  = IMG_OMS_EM + "multipledespacho.PNG"

	@Keyword
	def clickDespachado(){
		clickElement(IMG_DESPACHADO)
	}

	@Keyword
	def clickCancelar(){
		clickElement(IMG_CANCELAR)
		Thread.sleep(1000)
	}

	@Keyword
	def aceptar(){
		enter(1)
	}

	@Keyword
	def verificarCancelado(){
		checkThatIsVisibleWithTime(IMG_CANCELADO, "Equipo Movil Cancelado", 5)
	}

	@Keyword
	def clickSi(){
		clickElement(IMG_SI)
		Thread.sleep(1000)
	}

	@Keyword
	def clickDespachar(){
		clickElement(IMG_DESPACHAR)
	}

	@Keyword
	def verificarMultipleDespacho(){
		checkThatIsVisibleWithTime(IMG_MULTIPLE_DESPACHO, "Multiple Despacho", 5)
	}

	@Keyword
	def verificarDespacho(){
		checkThatIsVisibleWithTime(IMG_DESPACHADO, "Despachado", 5)
	}
}

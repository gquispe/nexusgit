package oms

import org.sikuli.script.Key

import com.kms.katalon.core.annotation.Keyword

import helper.DocUtils
import internal.GlobalVariable

public class Window_ApliqueUnFiltroYSeleccionUnDocumento extends helper.UIContext{

	protected static final String IMGAPLIQUE_UN_FILTRO_Y_SELECCIONE_UN_DOCUMENTO = GlobalVariable.IMAGENES + "OMS\\Windows\\ApliqueUnFiltroYSeleccioneUnDocumento\\"
	private static final String IMG_BUSCAR_TEXTBOX	 	   = IMGAPLIQUE_UN_FILTRO_Y_SELECCIONE_UN_DOCUMENTO + "buscartextbox.PNG"
	private static final String IMG_ACEPTAR 	   = IMGAPLIQUE_UN_FILTRO_Y_SELECCIONE_UN_DOCUMENTO + "aceptar.PNG"
	
	@Keyword
	def buscarDocumento(path, docSheet, row, column){
		clickElement(IMG_BUSCAR_TEXTBOX)
		String doc = DocUtils.readCell(path, docSheet, row, column)
		driver.type(doc)
		driver.type(Key.ENTER)
		Thread.sleep(1000)
	}
	
	@Keyword
	def clickAceptar(){
		clickElement(IMG_ACEPTAR)
		Thread.sleep(2000)
	}
}

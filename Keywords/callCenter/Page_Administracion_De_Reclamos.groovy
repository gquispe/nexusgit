package callCenter

import java.awt.*

import org.sikuli.script.App
import org.sikuli.script.Key
import org.sikuli.script.Region
import org.sikuli.script.Screen

import com.kms.katalon.core.annotation.Keyword

import helper.DocUtils
import helper.ScreenDriver
import internal.GlobalVariable
import junit.framework.Assert


public class Page_Administracion_De_Reclamos extends helper.UIContext{
	protected static Screen screen = ScreenDriver.getIntance()
	protected static Region region = new Region()
	private static App callCenter = new App("C:\\4DataLink\\Call Center\\DsCallCenter.exe")
	private Array_Reclamos_Motivos_Submotivos motivosSubmotivos = new Array_Reclamos_Motivos_Submotivos()

	private static final String TITULO_VENTANA = "Administración de Reclamos"
	private static final String IDENTIFICADOR_CLIENTE = "2568773356"
	private static final String IMAGENES_ADM_RECLAMOS =  GlobalVariable.IMAGENES + "callCenter\\"
	private static final String IMG_ICO_CALLCENTER = IMAGENES_ADM_RECLAMOS + "icono.PNG"
	private static final String IMG_BTN_BUSCAR = IMAGENES_ADM_RECLAMOS + "btn_buscar.PNG"
	private static final String IMG_BTN_ACEPTAR = IMAGENES_ADM_RECLAMOS + "btn_aceptar.PNG"
	private static final String IMG_BTN_NUEVO= IMAGENES_ADM_RECLAMOS + "btn_nuevo.PNG"
	private static final String IMG_BTN_NUEVO_RECLAMO = IMAGENES_ADM_RECLAMOS + "btn_nuevo_reclamo.PNG"
	private static final String IMG_SELECTALL = GlobalVariable.IMAGENES + "\\Utils\\SeleccionarTodo.png"
	private static final String IMG_RECLAMOS = IMAGENES_ADM_RECLAMOS + "cod_reclamo.PNG"
	private static final String IMG_HEADER_ADMINISTRACION_DE_RECLAMOS = IMAGENES_ADM_RECLAMOS + "Header_AdministracionDeReclamos.PNG"
	private static final String IMG_BTN_LIMPIAR = IMAGENES_ADM_RECLAMOS + "btn_limpiar.PNG"
	private static final String IMG_WINDOW_RECLAMOS = IMAGENES_ADM_RECLAMOS + "Ventana_Reclamos.PNG"
	private static final String IMG_TIPO = IMAGENES_ADM_RECLAMOS + "tipo.PNG"

	private static final String IMG_AREAS_OPERATIVAS = IMAGENES_ADM_RECLAMOS + "areasoperativas.PNG"
	private static final String IMG_PARTIDOS = IMAGENES_ADM_RECLAMOS + "partidos.PNG"
	private static final String IMG_LOCALIDADES = IMAGENES_ADM_RECLAMOS + "localidades.PNG"
	private static final String IMG_CALLE = IMAGENES_ADM_RECLAMOS + "calle.PNG"
	private static final String IMG_NUMERO = IMAGENES_ADM_RECLAMOS + "numero.PNG"
	private static final String IMG_BUSQUEDA = IMAGENES_ADM_RECLAMOS + "busqueda.PNG"
	private static final String IMG_CALLES_PUNTOS = IMAGENES_ADM_RECLAMOS + "callespuntos.PNG"


	@Keyword
	def focus() {
		App.focus(TITULO_VENTANA)
		App.focus(TITULO_VENTANA)
		focusWindow(IMG_HEADER_ADMINISTRACION_DE_RECLAMOS,IMG_ICO_CALLCENTER)
	}

	@Keyword
	def ingresarReclamo (String tipoReclamo, String userId, String motivo, String submotivo){
		ingresarReclamo(tipoReclamo,userId)
		verificarVentanaReclamo()
	}

	@Keyword
	def ingresarNumeroDeCuenta(String numeroCuenta){
		ingresarReclamo("C",numeroCuenta)
	}

	@Keyword
	def ingresarReclamo(String tipoReclamo, String numeroCuenta){
		focus()
		seleccionarTipoReclamo(tipoReclamo)
		ingresarCuenta(numeroCuenta)
		buscarReclamo()
	}

	def seleccionarTipoReclamo(String tipo){
		screen.type(tipo)
	}

	@Keyword
	def ingresarCuenta(String numeroCuenta){
		screen.type(Key.TAB)
		screen.type(numeroCuenta)
	}

	@Keyword
	def buscarReclamo(){
		clickElement(IMG_BTN_BUSCAR)
	}

	@Keyword
	def limpiar(){
		clickElement(IMG_BTN_LIMPIAR)
	}

	@Keyword
	def existsBtnAceptar(){
		Assert.assertTrue(screen.exists(IMG_BTN_ACEPTAR, 10))
	}

	@Keyword
	def cerrarApp(){
		eliminarProceso("DsCallCenter")
	}

	@Keyword
	def verificarNuevaVentana(){
		screen.exists(IMG_HEADER_ADMINISTRACION_DE_RECLAMOS,15)
		String title = callCenter.getWindow()
		System.out.println(title)
	}

	@Keyword
	def verificarVentanaReclamo(){
		if(screen.exists(IMG_BTN_ACEPTAR, 2)==null) {
			if(screen.exists(IMG_BTN_NUEVO, 2)){
				screen.click(IMG_BTN_NUEVO)
			}
			if(screen.exists(IMG_BTN_NUEVO_RECLAMO, 2)){
				screen.click(IMG_BTN_NUEVO_RECLAMO)
			}
		}
	}

	@Keyword
	def seleccionarTipoReclamo(index){
		seleccionDropDown(IMG_TIPO, index)
	}

	@Keyword
	def ingresarReclamo(String tipoReclamo, String path, int sheet, int row, int column){
		focus()
		seleccionarTipoReclamo(tipoReclamo)
		ingresarCuenta(DocUtils.readCell(path, sheet, row, column))
		buscarReclamo()
	}

	@Keyword
	def seleccionarAreasOperativas(index){
		//		seleccionDropDown(IMG_AREAS_OPERATIVAS, index)
		clickElement(IMG_AREAS_OPERATIVAS)
		up(10)
		down(index)
	}

	@Keyword
	def seleccionarPartidos(index){
		//		seleccionDropDown(IMG_PARTIDOS, index)
		clickElement(IMG_PARTIDOS)
		up(10)
		down(index)
	}

	@Keyword
	def seleccionarLocalidades(index){
		//		seleccionDropDown(IMG_LOCALIDADES, index)
		clickElement(IMG_LOCALIDADES)
		up(10)
		down(index)
	}

	@Keyword
	def seleccionarCalle(index){
		//		seleccionDropDown(IMG_CALLE, index)
		clickCallesPuntos()
		down(index)
		clickCallesPuntos()
	}

	@Keyword
	def seleccionarNumero(numero){
		tab(3)
		screen.type(numero)
	}

	@Keyword
	def seleccionarBusqueda(index){
		//		seleccionDropDown(IMG_BUSQUEDA, index)
		clickElement(IMG_BUSQUEDA)
		down(index)
	}

	@Keyword
	def clickCallesPuntos(){
		clickElement(IMG_CALLES_PUNTOS)
	}
}

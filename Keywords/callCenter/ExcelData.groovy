package callCenter

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.awt.Toolkit
import java.awt.datatransfer.*
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import helper.DocUtils

import internal.GlobalVariable

public class ExcelData extends DocUtils{
	private static final EXCEL_FILENAME = "C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\Call Center Smoke.xlsx"

	@Keyword
	public saveCodigoReclamo(int fila){
		Toolkit toolkit = Toolkit.getDefaultToolkit()
		Clipboard clipboard = toolkit.getSystemClipboard()
		String clipActual = (String) clipboard.getData(DataFlavor.stringFlavor)
		KeywordUtil.logInfo(clipActual)
		saveCodigoReclamo(clipActual,fila)
	}

	@Keyword
	public void saveCodigoReclamo(String codigoReclamo, int fila){
		escribirHojaCuentas (fila,3,codigoReclamo)
	}

	@Keyword
	public String getCodigoReclamo(int i){
		return leerHojaCuentas(i, 3)
	}

	@Keyword
	def saveTipoDocumentoObtenido(int i, String tipoDocumentoObtenido){
		writeCell(EXCEL_FILENAME, 0, i+1, 7, tipoDocumentoObtenido)
	}

	def leerHojaCuentas(int row, int column){
		return leerExcelSmokeCallCenter(1,row,column)
	}

	def leerExcelSmokeCallCenter(int sheet, int row, int column){
		return readCell(EXCEL_FILENAME,sheet,row, column)
	}

	def escribirHojaCuentas(int row, int column, String value){
		return escribirExcelSmokeCallCenter(1, row, column, value)
	}

	def escribirExcelSmokeCallCenter(int sheet, int row, int column, String value){
		return writeOldCell(EXCEL_FILENAME,sheet,row, column,value)
	}
}

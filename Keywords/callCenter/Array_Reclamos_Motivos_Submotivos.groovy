package callCenter

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.util.ArrayList

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class Array_Reclamos_Motivos_Submotivos {

	private ArrayList<String> motivos = new ArrayList<String>()
	private ArrayList<ArrayList> submotivos = new ArrayList<ArrayList>()
	private ArrayList<String> tipos = new ArrayList<String>()

	private ArrayList<String>  listaCajaMedidor = new ArrayList<String>()
	private ArrayList<String>  listaCajaDeToma = new ArrayList<String>()
	private ArrayList<String>  listaCajaDist = new ArrayList<String>()
	private ArrayList<String>  listaCentroTransfAereo = new ArrayList<String>()
	private ArrayList<String>  listaChoqueAutomovilistico = new ArrayList<String>()
	private ArrayList<String>  listaCTANivel = new ArrayList<String>()
	private ArrayList<String>  listaCTSubterraneo = new ArrayList<String>()
	private ArrayList<String>  listaDistElectricaPeligrosa = new ArrayList<String>()
	private ArrayList<String>  listaElementoElectrizado = new ArrayList<String>()
	private ArrayList<String>  listaHabitualesCS = new ArrayList<String>()
	private ArrayList<String>  listaHabitualesSS = new ArrayList<String>()
	private ArrayList<String>  listaLineaAereaBT = new ArrayList<String>()
	private ArrayList<String>  listaLineaSubterraneaBT = new ArrayList<String>()
	private ArrayList<String>  listaPilarDeConexion = new ArrayList<String>()
	private ArrayList<String>  listaPosteORienda = new ArrayList<String>()
	private ArrayList<String>  listaTendidoDeRamalProvisorio = new ArrayList<String>()
	private ArrayList<String>  listaTrabajosEnLaViaPublica = new ArrayList<String>()
	private ArrayList<String>  listaCajaTableroPrinc = new ArrayList<String>()
	private ArrayList<String>  listaColumnaDeLuminaria = new ArrayList<String>()
	private ArrayList<String>  listaSemaforo = new ArrayList<String>()

	public Array_Reclamos_Motivos_Submotivos(){
		inicializarArrays()
	}

	def inicializarArrays(){
		inicializarTipos()
		inicializarMotivos()
		inicializarSubmotivos()
		inicializarListaCajaMedidor()
		inicializarListaCajaDeToma()
		inicializarListaCajaDist()
		inicializarListaCentroTransfAereo()
		inicializarListaChoqueAutomovilistico()
		inicializarListaCTANivel()
		inicializarListaCTSubterraneo()
		inicializarListaDistElectricaPeligrosa()
		inicializarListaElementoElectrizado()
		inicializarListaHabitualesCS()
		inicializarListaHabitualesSS()
		inicializarListaLineaAereaBT()
		inicializarListaLineaSubterraneaBT()
		inicializarListaPilarDeConexion()
		inicializarListaPosteORienda()
		inicializarListaTendidoDeRamalProvisorio()
		inicializarListaTrabajosEnLaViaPublica()
	}
	def inicializarTipos(){
		tipos.add("Con Suministro")
		tipos.add("Con Suministro - SVP")
		tipos.add("Sin Suministro")
		tipos.add("Sin Suministro - SVP")
	}
	def inicializarMotivos(){
		motivos.add("Caja de Medidor");
		motivos.add("Caja de Toma");
		motivos.add("Caja Dist. (Buzón o Caja Esq)")
		motivos.add("Caja Tablero Princ del Usuario")
		motivos.add("Centro de Transformación Aéreo")
		motivos.add("Choque Automomovilístico")
		motivos.add("Columna de Luminaria")
		motivos.add("CT a Nivel")
		motivos.add("CT Subterráneo o Tipo Pozo")
		motivos.add("Distancia Eléctrica Peligrosa")
		motivos.add("Elemento Electrizado")
		motivos.add("Línea Aérea (BT)")
		motivos.add("Línea Subterránea BT")
		motivos.add("Pilar de Conexión")
		motivos.add("Poste o Rienda")
		motivos.add("Semáforos")
		motivos.add("Tendido de Ramal Provisorio")
		motivos.add("Trabajos en la Vía Pública")
	}
	def inicializarSubmotivos(){
		submotivos.add(listaCajaMedidor)
		submotivos.add(listaCajaDeToma)
		submotivos.add(listaCajaDist)
		submotivos.add(listaCajaTableroPrinc)
		submotivos.add(listaCentroTransfAereo)
		submotivos.add(listaChoqueAutomovilistico)
		submotivos.add(listaColumnaDeLuminaria)
		submotivos.add(listaCTANivel)
		submotivos.add(listaCTSubterraneo)
		submotivos.add(listaDistElectricaPeligrosa)
		submotivos.add(listaElementoElectrizado)
		submotivos.add(listaLineaAereaBT)
		submotivos.add(listaLineaSubterraneaBT)
		submotivos.add(listaPilarDeConexion)
		submotivos.add(listaPosteORienda)
		submotivos.add(listaSemaforo)
		submotivos.add(listaTendidoDeRamalProvisorio)
		submotivos.add(listaTrabajosEnLaViaPublica)
		submotivos.add(listaHabitualesCS)
		submotivos.add(listaHabitualesSS)
	}

	def inicializarListaCajaMedidor(){
		listaCajaMedidor.add("Cierre tapa faltante")
		listaCajaMedidor.add("Equipo de medidor suelto")
		listaCajaMedidor.add("Falta tapa")
		listaCajaMedidor.add("Inst precaria (de obra)")
		listaCajaMedidor.add("Instalacion con averias")
		listaCajaMedidor.add("Quemada/Fuego")
		listaCajaMedidor.add("Tapa abierta")
		listaCajaMedidor.add("Tapa con intersticios")
		listaCajaMedidor.add("Tapa rota")
		listaCajaMedidor.add("Vidrio roto")
	}
	def inicializarListaCajaDeToma(){
		listaCajaDeToma.add("Cierre de tapa faltante")
		listaCajaDeToma.add("Con averias")
		listaCajaDeToma.add("Quemada/Fuego")
		listaCajaDeToma.add("Tapa abierta")
		listaCajaDeToma.add("Tapa con intersticios")
		listaCajaDeToma.add("Tapa faltante")
		listaCajaDeToma.add("Tapa rota")
	}
	def inicializarListaCajaDist(){
		listaCajaDist.add("Buzón estruct deteriorada")
		listaCajaDist.add("Buzon falta puerta")
		listaCajaDist.add("Buzon puerta abierta")
		listaCajaDist.add("Buzon puerta intersticios")
		listaCajaDist.add("Buzon puerta rota")
		listaCajaDist.add("Buzon puerta sin cierre")
		listaCajaDist.add("Buzón vent obstruida")
		listaCajaDist.add("Caja esq estruct deterio")
		listaCajaDist.add("Caja esq interior c/agua")
		listaCajaDist.add("Caja esq tapa abierta")
		listaCajaDist.add("Caja esq tapa desnivel")
		listaCajaDist.add("Caja esq tapa intersticio")
		listaCajaDist.add("Caja esquinera sin tapa")
		listaCajaDist.add("Caja esquinera tapa rota")
		listaCajaDist.add("Con Fuego")
	}
	def inicializarListaCentroTransfAereo(){
		listaCentroTransfAereo.add("Con Fuego")
		listaCentroTransfAereo.add("Estructura con averias")
		listaCentroTransfAereo.add("Falta cartel de señal")
		listaCentroTransfAereo.add("Falta prot mec subida cab")
		listaCentroTransfAereo.add("Prot elect cond pat falta")
		listaCentroTransfAereo.add("Prot mec cond pat falta")
		listaCentroTransfAereo.add("Ptos energ dist antirreg")
		listaCentroTransfAereo.add("Puesta a tierra faltante")
		listaCentroTransfAereo.add("Sopor mnor dist lin edif")
		listaCentroTransfAereo.add("Soportes deteriorados")
		listaCentroTransfAereo.add("Trafo perdida de aceite")
	}
	def inicializarListaChoqueAutomovilistico(){
		listaChoqueAutomovilistico.add("Choque auto: inst averia")
	}
	def inicializarListaCTANivel(){
		listaCTANivel.add("Cerradura pta falta/defec")
		listaCTANivel.add("Con Fuego")
		listaCTANivel.add("Falta puerta")
		listaCTANivel.add("Puerta abierta")
		listaCTANivel.add("Puerta rota")
		listaCTANivel.add("Transformador con averias")
		listaCTANivel.add("Ventilacion obstruida")
	}
	def inicializarListaCTSubterraneo(){
		listaCTSubterraneo.add("Agua en la camara")
		listaCTSubterraneo.add("Averias en el trafo")
		listaCTSubterraneo.add("Cierre reja defect/falta")
		listaCTSubterraneo.add("Con Fuego")
		listaCTSubterraneo.add("Desnivel tapa/acera")
		listaCTSubterraneo.add("Falta candado tapa o reja")
		listaCTSubterraneo.add("Falta tapa")
		listaCTSubterraneo.add("Tapa rota")
	}
	def inicializarListaDistElectricaPeligrosa(){
		listaDistElectricaPeligrosa.add("Inst cables a la mano")
		listaDistElectricaPeligrosa.add("Linea aerea cerca o/serv")
		listaDistElectricaPeligrosa.add("Linea aerea cont o/serv")
	}
	def inicializarListaElementoElectrizado(){
		listaElementoElectrizado.add("Caja buzon electrificada")
		listaElementoElectrizado.add("Caja de medidor electr")
		listaElementoElectrizado.add("Caja toma prim electr")
		listaElementoElectrizado.add("Col de luminaria electr")
		listaElementoElectrizado.add("Col de semaforo electr")
		listaElementoElectrizado.add("Col metalica electrizada")
		listaElementoElectrizado.add("Pared electrificada")
		listaElementoElectrizado.add("Pilar electrizado")
		listaElementoElectrizado.add("Reja o alambrado electr")
		listaElementoElectrizado.add("Rienda electrizada")
		listaElementoElectrizado.add("Tablero princ usu elect")
	}
	def inicializarListaHabitualesCS(){
		listaHabitualesCS.add("Baja Tensión")
		listaHabitualesCS.add("Denuncia accidente")
		listaHabitualesCS.add("Denuncia cortes reiterados")
		listaHabitualesCS.add("Denuncia enganches clandestino")
		listaHabitualesCS.add("Oscilaciones")
		listaHabitualesCS.add("Recierre")
		listaHabitualesCS.add("Rehabilitación suspendidos")
		listaHabitualesCS.add("Requiere corte por arreglo")
		listaHabitualesCS.add("Requiere reconexión")
		listaHabitualesCS.add("Requiere retiro")
		listaHabitualesCS.add("Requiere verificación inst")
		listaHabitualesCS.add("Sobretensión")
	}
	def inicializarListaHabitualesSS(){
		listaHabitualesSS.add("Solo avería")
		listaHabitualesSS.add("Sin fuerza motriz")
		listaHabitualesSS.add("Sin fase")
	}
	def inicializarListaLineaAereaBT(){
		listaLineaAereaBT.add("A menor altura s/terreno")
		listaLineaAereaBT.add("A menor dist carteles")
		listaLineaAereaBT.add("A menor dist linea edif")
		listaLineaAereaBT.add("Cab cort caida rama/arbol")
		listaLineaAereaBT.add("Cab poco tensado/se tocan")
		listaLineaAereaBT.add("Cables con chispas/fuego")
		listaLineaAereaBT.add("Cables cortados cuelgan")
		listaLineaAereaBT.add("Instalaciones con averias")
		listaLineaAereaBT.add("Interferencia con ramas")
	}
	def inicializarListaLineaSubterraneaBT(){
		listaLineaSubterraneaBT.add("A menor dist o/servicios")
		listaLineaSubterraneaBT.add("Profundidad inadecuada")
		listaLineaSubterraneaBT.add("Prot mec falta/inadecuada")
	}
	def inicializarListaPilarDeConexion(){
		listaPilarDeConexion.add("Acometida en mal estado")
		listaPilarDeConexion.add("Caño pilar malas condic")
		listaPilarDeConexion.add("Pilar con pipeta rota")
		listaPilarDeConexion.add("Pilar mamposteria deterio")
		listaPilarDeConexion.add("Quemada/Fuego")
	}
	def inicializarListaPosteORienda(){
		listaPosteORienda.add("Col met:falta pues tierra")
		listaPosteORienda.add("Col metalica inclinada")
		listaPosteORienda.add("Columna metalica deterio")
		listaPosteORienda.add("Cruc lin aerea aislad det")
		listaPosteORienda.add("Cruceta lin aerea suelta")
		listaPosteORienda.add("Cruceta linea aerea incli")
		listaPosteORienda.add("Cruceta linea aerea rota")
		listaPosteORienda.add("Pos/col mnor dist li edi")
		listaPosteORienda.add("Poste de madera inclinado")
		listaPosteORienda.add("Poste de madera podrido")
		listaPosteORienda.add("Poste de madera quebrado")
		listaPosteORienda.add("Poste h.a. inclinado")
		listaPosteORienda.add("Poste h.a. quebrado")
		listaPosteORienda.add("Poste/col caido")
		listaPosteORienda.add("Rienda floja")
		listaPosteORienda.add("Rienda suelta o cortada")
		listaPosteORienda.add("Rienda y aislad deterior")
	}
	def inicializarListaTendidoDeRamalProvisorio(){
		listaTendidoDeRamalProvisorio.add("Conexion clandest.")
		listaTendidoDeRamalProvisorio.add("Ramal en mal estado")
		listaTendidoDeRamalProvisorio.add("Ramal precario")
		listaTendidoDeRamalProvisorio.add("Ramal tapa c/intersticio")
		listaTendidoDeRamalProvisorio.add("Ramal tendido en el suelo")
	}
	def inicializarListaTrabajosEnLaViaPublica(){
		listaTrabajosEnLaViaPublica.add("Calzada planchuela chica")
		listaTrabajosEnLaViaPublica.add("Cartel < 1 x 0.7m")
		listaTrabajosEnLaViaPublica.add("Cartel <0.4m suelo")
		listaTrabajosEnLaViaPublica.add("Cartel instal elect falta")
		listaTrabajosEnLaViaPublica.add("Cartel zanja abierta falt")
		listaTrabajosEnLaViaPublica.add("Encintado faltante")
		listaTrabajosEnLaViaPublica.add("Indica rojo calzada falta")
		listaTrabajosEnLaViaPublica.add("Parrilla de madera falta")
		listaTrabajosEnLaViaPublica.add("Ramal pelig cualq varied")
		listaTrabajosEnLaViaPublica.add("Señal calz no visi 100 m")
		listaTrabajosEnLaViaPublica.add("Tierra parcial suelta")
		listaTrabajosEnLaViaPublica.add("Tierra suelta")
		listaTrabajosEnLaViaPublica.add("Vallado c/abert > 0,5 m")
		listaTrabajosEnLaViaPublica.add("Vallado c/altura < 1,2 m")
		listaTrabajosEnLaViaPublica.add("Vallado clor no normal")
		listaTrabajosEnLaViaPublica.add("Vallado exterior faltante")
		listaTrabajosEnLaViaPublica.add("Vallado faltante")
		listaTrabajosEnLaViaPublica.add("Vallado parcial")
		listaTrabajosEnLaViaPublica.add("Vallado s/disposit cierre")
		listaTrabajosEnLaViaPublica.add("Vallado s/resistencia mec")
		listaTrabajosEnLaViaPublica.add("Vallado sin estabilidad")
		listaTrabajosEnLaViaPublica.add("Vallado: cierre c/alambre")
	}
}

package callCenter

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class Tipos_Reclamo {
	private String tipo
	private String codigoCliente
	private String motivo
	private String submotivo
	private String tipoDocumento
	private String codigoReclamo


	public Tipos_Reclamo(String _tipo, String _codigoCliente, String _motivo, String _submotivo){
		tipo 		  = _tipo
		codigoCliente = _codigoCliente
		motivo 		  = _motivo
		submotivo 	  = _submotivo
	}

	public Tipos_Reclamo(String _tipoDoc, String _codReclamo){
		tipoDocumento = _tipoDoc
		codigoReclamo = _codReclamo
	}

	public Tipos_Reclamo(String _tipo, String _codigoCliente, String _motivo, String _submotivo,String _tipoDoc, String _codReclamo){
		tipo 		  = _tipo
		codigoCliente = _codigoCliente
		motivo 		  = _motivo
		submotivo 	  = _submotivo
		tipoDocumento = _tipoDoc
		codigoReclamo = _codReclamo
	}

	public String tipo(){
		return tipo
	}

	public String codigoCliente(){
		return codigoCliente
	}

	public String motivo(){
		return motivo
	}

	public String submotivo(){
		return submotivo
	}

	public String tipoDocumento(){
		return tipoDocumento
	}

	public String codigoReclamo(){
		return codigoReclamo
	}
}

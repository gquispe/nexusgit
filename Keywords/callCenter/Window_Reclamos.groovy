package callCenter

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable
import org.sikuli.script.App
import org.sikuli.script.Screen
import org.sikuli.script.Region
import org.sikuli.script.KeyModifier
import org.sikuli.script.Key
import callCenter.Page_Administracion_De_Reclamos
import java.awt.*
import helper.CustomScreenshot
import com.kms.katalon.core.configuration.RunConfiguration
import helper.ScreenDriver


public class Window_Reclamos extends helper.UIContext{

	private Array_Reclamos_Motivos_Submotivos motivosSubmotivos = new Array_Reclamos_Motivos_Submotivos()
	protected static Screen screen = ScreenDriver.getIntance()

	private static final String IMG_CALLCENTER = GlobalVariable.IMAGENES + "callCenter\\Reclamos\\"
	private static final String IMG_ACEPTAR = IMG_CALLCENTER + "BotonAceptar.PNG"
	private static final String IMG_WINDOW_RECLAMOS = IMG_CALLCENTER + "Ventana_Reclamos.PNG"
	private static final String IMG_SUBMOTIVOS = IMG_CALLCENTER + "submotivos.PNG"
	private static final String IMG_PELIGROSIDAD = IMG_CALLCENTER + "peligrosidad.PNG"
	private static final String IMG_TIPO = IMG_CALLCENTER + "tipo.PNG"
	private static final String IMG_MOTIVO = IMG_CALLCENTER + "motivo.PNG"

	@Keyword
	def ingresarMotivoSubmotivoDeReclamo(String tipoReclamo, String userId,String motivo,String submotivo){
		checkThatIsVisibleWithTime(IMG_WINDOW_RECLAMOS,"Ventana de Reclamos", GlobalVariable.DEFAULT_WAIT)
		ingresarMotivoSubmotivo(tipoReclamo,motivo,submotivo)
		takeScreenshot("Cliente_" + userId + "_Reclamo_Motivo_Submotivo")
		clickAceptar()
	}

	@Keyword
	def clickAceptar(){
		Thread.sleep(1000)
		clickElement(IMG_ACEPTAR)
	}

	def ingresarMotivoSubmotivo(String tipoReclamo,String motivo, String submotivo){
		int indexTipoReclamo=motivosSubmotivos.tipos.indexOf(tipoReclamo)
		if (indexTipoReclamo == 0 || indexTipoReclamo == 2)
			ingresarMotivoSubmotivoHabituales(indexTipoReclamo,motivo,submotivo)
		else
			ingresarMotivoSubmotivoSVP(motivo,submotivo)
	}

	def ingresarMotivoSubmotivoHabituales(int indexTipoReclamo,String motivo, String submotivo) {
		screen.type(Key.TAB)
		screen.type(Key.TAB)
		int indexSubmotivo
		if (indexTipoReclamo == 0){
			indexSubmotivo=motivosSubmotivos.listaHabitualesCS.indexOf(submotivo)
			seleccionarComboBox(indexSubmotivo-1)
		}
		else{
			indexSubmotivo=motivosSubmotivos.listaHabitualesSS.indexOf(submotivo)
			ingresarSubmotivoSS(indexSubmotivo)
		}
	}

	def ingresarMotivoSubmotivoSVP(String motivo, String submotivo){
		screen.type(Key.TAB)
		int indexMotivo = motivosSubmotivos.motivos.indexOf(motivo)
		seleccionarComboBox(indexMotivo)
		screen.type(Key.TAB)
		int indexSubmotivo=motivosSubmotivos.submotivos.get(indexMotivo).indexOf(submotivo)
		seleccionarComboBox(indexSubmotivo)
	}

	def seleccionarComboBox(int index){
		for (int i = 0; i<index+1 ;i++){
			screen.type(Key.DOWN)
		}
	}

	def ingresarSubmotivoSS(int index){
		for (int i = 0; i<index ;i++){
			screen.type(Key.UP)
		}
	}

	@Keyword
	def seleccionaSubMotivo(index){
		seleccionDropDown(IMG_SUBMOTIVOS,index)
	}

	@Keyword
	def aceptarFormulario(){
		Thread.sleep(1000)
		screen.type(Key.ENTER)
	}

	@Keyword
	def seleccionaPeligrosidad(index){
		seleccionDropDown(IMG_PELIGROSIDAD,index)
	}

	@Keyword
	def seleccionarTipo(index){
		seleccionDropDown(IMG_TIPO,index)
	}

	@Keyword
	def seleccionarMotivo(index){
		seleccionDropDown(IMG_MOTIVO,index)
	}
}

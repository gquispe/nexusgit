package callCenter

import org.sikuli.script.Key
import org.sikuli.script.KeyModifier

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.util.KeywordUtil

import helper.DocUtils
import internal.GlobalVariable
public class Window_OperacionExitosa extends helper.UIContext{

	private static final String IMG_CALLCENTER = GlobalVariable.IMAGENES + "callCenter\\OperacionExitosa\\"
	private static final String IMG_EXITO = IMG_CALLCENTER + "MsgExito.PNG"
	private static final String IMG_SELECTALL = GlobalVariable.IMAGENES + "\\Utils\\SeleccionarTodo.png"
	private static final String IMG_RECLAMOS = IMG_CALLCENTER + "cod_reclamo.PNG"
	private static final String IMG_RECLAMO = IMG_CALLCENTER + "reclamo.PNG"

	@Keyword
	def verificarOperacionExitosa(){
		checkThatIsVisibleWithTime(IMG_EXITO, "Operacion Exitosa", 5)
	}

	@Keyword
	def copiarCodigoReclamo(){
		driver.wait(IMG_RECLAMOS,10)
		driver.type(Key.TAB)
		driver.type(Key.F10, Key.SHIFT)
		driver.click(IMG_SELECTALL)
		driver.type("c",KeyModifier.CTRL)
	}

	@Keyword
	def aceptarOperacion(){
		clickElement(IMG_RECLAMOS)
		tab(3)
		driver.type(Key.ENTER)
		Thread.sleep(3000)
	}

	@Keyword
	def guardarCodigoDeReclamo(path, sheet){
		driver.rightClick(IMG_RECLAMO)
		driver.click(IMG_SELECTALL)
		driver.type("c",KeyModifier.CTRL)
		KeywordUtil.logInfo(">>> Codigo de reclamo: " + getClipboard())
		DocUtils.writeOldCell(path, sheet, 1, 1, getClipboard())
	}

	@Keyword
	def guardarCodigoDeReclamo(path, sheet, row, column){
		driver.rightClick(IMG_RECLAMO)
		driver.click(IMG_SELECTALL)
		driver.type("c",KeyModifier.CTRL)
		KeywordUtil.logInfo(">>> Codigo de reclamo: " + getClipboard())
		DocUtils.writeCell(path, sheet, row, column, getClipboard())
	}

	@Keyword
	def guardarCodigoDeReclamoOverwrite(path, sheet, row, column){
		driver.rightClick(IMG_RECLAMO)
		driver.click(IMG_SELECTALL)
		driver.type("c",KeyModifier.CTRL)
		KeywordUtil.logInfo(">>> Codigo de reclamo: " + getClipboard())
		DocUtils.writeOldCell(path, sheet, row, column, getClipboard())
	}
}

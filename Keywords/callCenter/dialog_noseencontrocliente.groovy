package callCenter

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.sikuli.script.App
import org.sikuli.script.Region
import org.sikuli.script.Screen

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class dialog_noseencontrocliente extends helper.UIContext{

	private static final String IMAGENES_CC_DIAG =  GlobalVariable.IMAGENES + "callCenter\\diag_noseencontrocliente\\"
	private static final String BTN_SI = IMAGENES_CC_DIAG + "boton_si.PNG"
	private static final String DIAG = IMAGENES_CC_DIAG + "noseencontrocliente.PNG"
	
	@Keyword
	def clickSi(){
		clickElement(BTN_SI)
	}

	@Keyword
	def noSeEncontroClienteAparece(){
		checkThatIsVisibleWithTime(DIAG, "Alerta presente", 5)
	}
}

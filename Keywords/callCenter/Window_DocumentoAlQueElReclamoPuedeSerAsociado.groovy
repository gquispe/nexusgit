package callCenter

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class Window_DocumentoAlQueElReclamoPuedeSerAsociado extends helper.UIContext{

	protected static final String IMAGENES_CALL_CENTER =  GlobalVariable.IMAGENES + "CallCenter\\DocumentoAlQueElReclamoPuedeSerAsociado\\"
	private static final String IMG_ASOCIAR = IMAGENES_CALL_CENTER + "BotonAsociar.PNG"
	private static final String IMG_CALLCENTER = GlobalVariable.IMAGENES + "callCenter\\Reclamos\\"
	private static final String IMG_ACEPTAR = IMG_CALLCENTER + "BotonAceptar.PNG"

	@Keyword
	def clickAsociar(){
		verifyAndClick(IMG_ASOCIAR)
	}
}

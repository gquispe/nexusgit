package callCenter

import java.util.concurrent.ThreadLocalRandom

import com.kms.katalon.core.annotation.Keyword

import internal.GlobalVariable

public class Window_ReclamosActivosDelCliente extends helper.UIContext{

	private static final String IMAGENES_ADM_RECLAMOS_ACTIVOS_DEL_CLIENTE =  GlobalVariable.IMAGENES + "callCenter\\ReclamosActivosDelCliente\\"
	private static final String IMG_REITERAR = IMAGENES_ADM_RECLAMOS_ACTIVOS_DEL_CLIENTE + "reiterar.PNG"
	private static final String IMG_CANCELAR = IMAGENES_ADM_RECLAMOS_ACTIVOS_DEL_CLIENTE + "cancelar.PNG"
	private static final String IMG_NUEVO2 = IMAGENES_ADM_RECLAMOS_ACTIVOS_DEL_CLIENTE + "nuevo2.PNG"
	private static final String IMG_NUEVO1 = IMAGENES_ADM_RECLAMOS_ACTIVOS_DEL_CLIENTE + "nuevo1.PNG"
	
	/* Telefonos by GDV */
	private static final String IMG_IMPRIMIR  = IMAGENES_ADM_RECLAMOS_ACTIVOS_DEL_CLIENTE + "imprimir.PNG"
	private static final String IMG_DDETCLIE  = IMAGENES_ADM_RECLAMOS_ACTIVOS_DEL_CLIENTE + "datosdetclie.PNG"
	private static final String IMG_OTELEFON  = IMAGENES_ADM_RECLAMOS_ACTIVOS_DEL_CLIENTE + "otelefonos.PNG"
	private static final String IMG_TELNUEVO  = IMAGENES_ADM_RECLAMOS_ACTIVOS_DEL_CLIENTE + "telnuevo.PNG"
	private static final String IMG_TELCIERRA = IMAGENES_ADM_RECLAMOS_ACTIVOS_DEL_CLIENTE + "telcerrar.PNG"
	private static final String IMG_PREVERROR = IMAGENES_ADM_RECLAMOS_ACTIVOS_DEL_CLIENTE + "ErrorMsgAceptar.PNG"
	private static final String IMG_PRVCERRAR = IMAGENES_ADM_RECLAMOS_ACTIVOS_DEL_CLIENTE + "cerrarpreview.PNG"

	private static final String IMG_SELTIPTEL = IMAGENES_ADM_RECLAMOS_ACTIVOS_DEL_CLIENTE + "teltiposel.PNG"
	private static final String IMG_INGRESTEL = IMAGENES_ADM_RECLAMOS_ACTIVOS_DEL_CLIENTE + "telingresanro.PNG"
	private static final String IMG_TELCHKCTO = IMAGENES_ADM_RECLAMOS_ACTIVOS_DEL_CLIENTE + "telcheckcto.PNG"
	private static final String IMG_TELACEPTA = IMAGENES_ADM_RECLAMOS_ACTIVOS_DEL_CLIENTE + "telaceptacamb.PNG"
	private static final String IMG_TELACPMSG = IMAGENES_ADM_RECLAMOS_ACTIVOS_DEL_CLIENTE + "telaceptarnuevo.PNG"

	@Keyword
	def clickReiterar(){
		verifyAndClick(IMG_REITERAR)
	}

	@Keyword
	def clickCancelar(){
		clickElement(IMG_CANCELAR)
	}

	@Keyword
	def clickNuevo1(){
		verifyAndClick(IMG_NUEVO1)
	}

	@Keyword
	def clickNuevo2(){
		verifyAndClick(IMG_NUEVO2)
	}

	@Keyword
	def verificarReiteracionDisponible(){
		checkThatIsVisibleWithTime(IMG_REITERAR, "Boton Reiterar", 5)
	}
	
	/* Telefonos by GDV ~~~~~~~~~~~~~~~~~~~ */
	@Keyword
	def clickImprimir(){
		clickElement(IMG_IMPRIMIR)
	}

	@Keyword
	def clickDetaDatCliente(){
		clickElement(IMG_DDETCLIE)
	}

	@Keyword
	def clickOtrosTelef(){
		clickElement(IMG_OTELEFON)
	}

	@Keyword
	def clickCerrarTelef(){
		clickElement(IMG_TELCIERRA)
		Thread.sleep(1000)
	}

	@Keyword
	def clickNuevoTelef(){
		clickElement(IMG_TELNUEVO)
	}

	@Keyword
	def clickTelAceptarError(){
		clickElement(IMG_PREVERROR)
	}

	@Keyword
	def clickTelCerrarPreview(){
		clickElement(IMG_PRVCERRAR)
	}

	@Keyword
	def clickSelTipoTel(){
		clickElement(IMG_SELTIPTEL)
	}
	
	@Keyword
	def clickIngresoNroTel(){
		clickElement(IMG_INGRESTEL)
	}

	@Keyword
	def clickCheckCtoTel(){
		clickElement(IMG_TELCHKCTO)
	}

	@Keyword
	def clickTelAcepta(){
		clickElement(IMG_TELACEPTA)
	}

	@Keyword
	def clickTelAceptaMsg(){
		clickElement(IMG_TELACPMSG)
		Thread.sleep(1000)
	}

	@Keyword
	def seleccionarTipoTel(index){
		seleccionDropDown(IMG_SELTIPTEL, index)
	}
			
	@Keyword
	def ingresarTelefono(String numeroTelefono){
		// screen.type(Key.TAB)
		clickIngresoNroTel()
		driver.type(numeroTelefono)
	}
		
	@Keyword
	def randomNumber(int minimum, int maximum) {
		def randomNumber = ThreadLocalRandom.current().nextInt(minimum, maximum + 1)
		return (String.valueOf(randomNumber))
	}
		
	/* Fin nuevos by GDV ~~~~~~~~~~~~~~~~~~~ */

}

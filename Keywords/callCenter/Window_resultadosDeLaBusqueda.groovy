package callCenter

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.sikuli.script.App
import org.sikuli.script.Region
import org.sikuli.script.Screen

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class Window_resultadosDeLaBusqueda extends helper.UIContext{
	
	private static final String IMAGENES_RESULTADOS_DE_LA_BUSQUEDA =  GlobalVariable.IMAGENES + "callCenter\\ResultadosDeLaBusqueda\\"
	private static final String IMG_ACEPTAR = IMAGENES_RESULTADOS_DE_LA_BUSQUEDA + "aceptar.PNG"
	
	@Keyword
	def clickAceptar(){
		clickElement(IMG_ACEPTAR)
	}
	
}

package sikuli

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.sikuli.script.App
import org.sikuli.script.Button
import org.sikuli.script.FindFailed
import org.sikuli.script.Key
import org.sikuli.script.KeyModifier
import org.sikuli.script.Match
import org.sikuli.script.Pattern
import org.sikuli.script.Screen
import org.sikuli.script.App.Clipboard
import org.omg.PortableServer.THREAD_POLICY_ID
import org.osgi.framework.hooks.bundle.FindHook
import org.sikuli.api.API
import org.sikuli.hotkey.Keys
import org.sikuli.script.Region
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import internal.GlobalVariable

public class Utils {
	protected static Screen screen = new Screen()

	@Keyword
	def Esperar(Screen s, String imagen ){
		int cont = 0
		while (s.exists(imagen) == false) {
			cont = cont + 1;
			Thread.sleep(20)
			if (cont == 10) {
				break
			}
		}
	}
	@Keyword
	def SeleccionarTodo(Screen s ,  String imagen){
		try {
			s.rightClick(imagen)
			s.click("C:\\Katalon Studio\\Nexus\\imagenes\\Utils\\SeleccionarTodo.png")
		}
		catch(FindFailed e){
			e.printStackTrace()
		}
	}

	@Keyword
	def GetData(){
		String data = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
		return data
	}
}

package sikuli

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.tigervnc.rfb.screenTypes

import org.sikuli.script.App
import org.sikuli.script.Button;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;
import org.sikuli.script.KeyModifier
import org.sikuli.script.Match;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.sikuli.api.API;
import org.sikuli.script.App
import internal.GlobalVariable


public class Workstation {
	protected static Screen screen = new Screen();

	/**
	 * Click en la imagen que indiques
	 * @param region, Ruta de imagen
	 */

	@Keyword
	def AbrirWorkstation() {

		try {
			screen.type("r", KeyModifier.WIN);
			screen.paste("C:\\4DataLink\\4DL-Workstation-Core\\certa-workstation-core.exe");
			screen.type(Key.ENTER);

			this.LoginWS(screen);
			KeywordUtil.logInfo("Se abrio CERTA y se Logeo")
		}
		catch (FindFailed e) {
			e.printStackTrace()
			//throw new com.kms.katalon.core.exception.StepErrorException("Error - Abrir workstation ")
		}
	}
	@Keyword
	def AbrirOMS(){
		try{
			screen.click("C:\\Katalon Studio\\Nexus\\imagenes\\Workstation\\operacion-icon.png",50);
			screen.click("C:\\Katalon Studio\\Nexus\\imagenes\\Workstation\\OMS-menu.png");
			screen.wait("C:\\Katalon Studio\\Nexus\\imagenes\\Workstation\\titulo-label.png",30*3)
			screen.click("C:\\Katalon Studio\\Nexus\\imagenes\\Workstation\\titulo-label.png")

			App.focus('OMS: Outage Management System')
			screen.click("C:\\Katalon Studio\\Nexus\\imagenes\\Workstation\\titulo-label.png")
			Thread.sleep(5)
			screen.click("C:\\Katalon Studio\\Nexus\\imagenes\\OMS\\Regiones.png")
			Thread.sleep(5)
			screen.click("C:\\Katalon Studio\\Nexus\\imagenes\\OMS\\GuardarYSalir.png")
			//screen.type(Key.ENTER)
			KeywordUtil.logInfo("Se abrio OMS correctamente")
		}
		catch(FindFailed e) {

			e.printStackTrace();
			//throw new com.kms.katalon.core.exception.StepErrorException("Error - Abrir OMS ")
		}
	}

	def LoginWS(Screen screen){
		try{
			screen.wait("C:\\Katalon Studio\\Nexus\\imagenes\\Workstation\\Login-Certa.png",30*60);
			screen.click(findTestObject('Workstation/Certa'));
			screen.type(Key.TAB + Key.TAB + Key.TAB)
			screen.type("oms_des_cent");
			screen.type(Key.TAB);
			screen.type("Edenor2016");
			screen.type(Key.TAB);
			screen.type(Key.ENTER);
			screen.wait("C:\\Katalon Studio\\Nexus\\imagenes\\Workstation\\operacion-icon.png",50);
		}
		catch (FindFailed e) {
			e.printStackTrace();
			throw new com.kms.katalon.core.exception.StepErrorException("Error - Abrir OMS ")
		}
	}
}

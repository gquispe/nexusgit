package sikuli

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.exception.StepErrorException
import java.awt.Toolkit
import java.awt.datatransfer.DataFlavor
import java.awt.geom.Arc2D.Double

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.tigervnc.rfb.screenTypes

import groovy.util.logging.Commons

import org.sikuli.script.App
import org.sikuli.script.Button
import org.sikuli.script.FindFailed
import org.sikuli.script.Key
import org.sikuli.script.KeyModifier
import org.sikuli.script.Match
import org.sikuli.script.Pattern
import org.sikuli.script.Screen
import org.sikuli.script.App.Clipboard
import org.omg.PortableServer.THREAD_POLICY_ID
import org.osgi.framework.hooks.bundle.FindHook
import org.sikuli.api.API
import org.sikuli.hotkey.Keys
import org.sikuli.script.Region
import internal.GlobalVariable


public class Oms {
	protected static Screen screen = new Screen()
	protected static Utils util = new sikuli.Utils()


	@Keyword
	def  EnfocarOMS(){
		try{
			screen.click("C:\\Katalon Studio\\Nexus\\imagenes\\OMS\\OMS-Barra.png")
			App.focus("OMS: Outage Management System")
		}
		catch(FindFailed e) {
			throw new com.kms.katalon.core.exception.StepErrorException("Error - Abrir OMS ")
			e.printStackTrace()
		}
	}

	@Keyword
	def GenerarDocProgmadoBT (String fechaCorte){
		try {
			this.BotonNuevoDoc(screen)
			this.CompletarNuevoDoc(screen,'programadoBT',fechaCorte)
			Thread.sleep(1000)
			screen.click("C:\\Katalon Studio\\Nexus\\imagenes\\OMS\\Aceptar-Doc.png")
			screen.wait("C:\\Katalon Studio\\Nexus\\imagenes\\OMS\\Abrir-Doc.png", 60)
			screen.click("C:\\Katalon Studio\\Nexus\\imagenes\\OMS\\Abrir-Doc.png")
			Region pantalla = Region.create(429,74,29,13)
			pantalla.click()
			screen.type("c",KeyModifier.CTRL)
			String r = util.GetData()
			KeywordUtil.logInfo("Se creo documento: " + r)
			Thread.sleep(1000)
		}
		catch (FindFailed e){
			e.printStackTrace()
			//throw new com.kms.katalon.core.exception.StepErrorException("Error - CrearDocumento Programado BT");

		}
	}

	@Keyword
	def AfectarElementoBT(){
		try{

			this.DarFocoDocCreado(screen)
			this.BotonAfectar(screen)
			this.DarFocoMapa(screen)
			WebUI.delay(10)
			WebUI.waitForElementPresent(findTestObject('Page_CROMO Versin 1.01/AfectacionSalidaCT'),20)
			WebUI.click(findTestObject('Page_CROMO Versin 1.01/AfectacionSalidaCT'))
			WebUI.delay(5)
			WebUI.waitForElementPresent(findTestObject('Page_CROMO Versin 1.01/BotonAceptarSeleccion'), 10)
			WebUI.click(findTestObject('Page_CROMO Versin 1.01/BotonAceptarSeleccion'))
			Thread.sleep(2005)
			this.DarFocoDocCreado(screen)
			screen.click("C:\\Katalon Studio\\Nexus\\imagenes\\OMS\\Aceptar-Doc.png")
			Thread.sleep(1010)
			screen.click("C:\\Katalon Studio\\Nexus\\imagenes\\OMS\\Aceptar-Doc.png")
			Thread.sleep(50000)
			util.Esperar(screen, "C:\\Katalon Studio\\Nexus\\imagenes\\OMS\\Afectados.png")
			util.SeleccionarTodo(screen,"C:\\Katalon Studio\\Nexus\\imagenes\\OMS\\Afectados.png")
			screen.type("c",KeyModifier.CTRL)

			String afectados = util.GetData()
			KeywordUtil.logInfo(afectados)
		}
		catch (Exception e){
			e.printStackTrace()
			throw new com.kms.katalon.core.exception.StepErrorException("Error - al afectar elemento")
		}
	}

	def BotonAfectar(Screen s){
		try{
			Thread.sleep(10)
			util.Esperar(s,"C:\\Katalon Studio\\Nexus\\imagenes\\OMS\\Afectar.png")
			s.click("C:\\Katalon Studio\\Nexus\\imagenes\\OMS\\Afectar.png")
			Thread.sleep(10)
			util.Esperar(s, "C:\\Katalon Studio\\Nexus\\imagenes\\OMS\\Topologica.png")
			s.click("C:\\Katalon Studio\\Nexus\\imagenes\\OMS\\Topologica.png")
			Thread.sleep(5)
			s.click("C:\\Katalon Studio\\Nexus\\imagenes\\OMS\\MapaTopologico.png")
		}
		catch(FindFailed f){
			f.printStackTrace()
			throw new com.kms.katalon.core.exception.StepErrorException("Error - al afectar elemento" + f.printStackTrace())
		}
	}

	def BotonNuevoDoc (Screen s){
		s.click("C:\\Katalon Studio\\Nexus\\imagenes\\OMS\\documento-nuevo.png")
		s.click("C:\\Katalon Studio\\Nexus\\imagenes\\OMS\\tipoDoc.png")
	}




	def CompletarNuevoDoc(Screen s, String tipoD,String fechaCorte){
		s.click(findTestObject('OMS/TipoDoc'))
		switch (tipoD){
			case 'programadoBT':
				s.click("C:\\Katalon Studio\\Nexus\\imagenes\\OMS\\programadoBT.png")
				s.doubleClick("C:\\Katalon Studio\\Nexus\\imagenes\\OMS\\FechaDeCorte.png")
				s.type(fechaCorte)
				s.click("C:\\Katalon Studio\\Nexus\\imagenes\\OMS\\tiempoEstimado.png")
				s.type("0")
				s.type(Keys.ENTER)
				s.type(Keys.TAB)
				s.click("C:\\Katalon Studio\\Nexus\\imagenes\\OMS\\Motivo.png")
				s.type(Keys.ENTER)
				s.click("C:\\Katalon Studio\\Nexus\\imagenes\\OMS\\MotivoSelec.png")
				s.click("C:\\Katalon Studio\\Nexus\\imagenes\\OMS\\Mapa.png")
				this.DarFocoMapa(s)

				try{
					WebUI.click(findTestObject('Page_CROMO Versin 1.01/svg_SeleccionMapa'))
					WebUI.setText(findTestObject('Page_CROMO Versin 1.01/input_Edicin_input-search'),'18126')
					WebUI.click(findTestObject('Page_CROMO Versin 1.01/i_Edicin_fa fa-search'))
					WebUI.delay(1)
					WebUI.click(findTestObject('Object Repository/Page_CROMO Versin 1.01/SeleccionMapa/Page_CROMO Versin 1.0/a_10281467'))
					WebUI.delay(10)
					Region pantalla = Region.create(631, 441, 38, 17)
					pantalla.click()
				}
				catch (Exception e){
					e.printStackTrace()
					throw new com.kms.katalon.core.exception.StepErrorException("Error al seleccion un punto en el mapa")
				}

				break
		}
	}
	def DarFocoMapa(Screen s){
		s.click("C:\\Katalon Studio\\Nexus\\imagenes\\Chrome.png")
	}



	def DarFocoDocCreado(Screen s){
		try{
			s.click("C:\\Katalon Studio\\Nexus\\imagenes\\Workstation\\OMS-Barra.png")
			s.click("C:\\Katalon Studio\\Nexus\\imagenes\\OMS\\DocumentoCreado.png")
		}
		catch(FindFailed f){
			f.printStackTrace()
		}
	}
}





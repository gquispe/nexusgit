package situacionMonitor

import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.sikuli.script.*

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.util.KeywordUtil

import helper.ScreenDriver
import helper.UIContext
import internal.GlobalVariable
import helper.ExcelData
import helper.CustomScreenshot

public class Main extends UIContext{
	protected static Screen screen = ScreenDriver.getIntance()

	protected static final String IMAGENES_SITUACION_MONITOR =  GlobalVariable.IMAGENES + "Situacion Monitor\\"
	private static final String IMG_HEADER 	= IMAGENES_SITUACION_MONITOR + "Header.png"
	private static final String IMG_HEADER_RED 	= IMAGENES_SITUACION_MONITOR + "Red_Header.png"
	private static final String IMG_HEADER_SELECCIONADO 	= IMAGENES_SITUACION_MONITOR + "HeaderSeleccionado.png"
	private static final String EXCEL_PATH = "C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\DatosAnteriores.xlsx"
	private static final String IMG_LLAMADAS_SALIENTES = IMAGENES_SITUACION_MONITOR + "Llamadas_Salientes.png"
	private static final String IMG_HEADER_DOCPROGBT = IMAGENES_SITUACION_MONITOR + "Header_DocumentoProgramadoBT.png"
	private static final String IMG_HEADER_DOCFORZBT = IMAGENES_SITUACION_MONITOR + "Documentos Forzados Agrupados BT.png"
	private static final String IMG_HEADER_DOCFORZMT = IMAGENES_SITUACION_MONITOR + "Documentos Forzados Agrupados MT.png"
	private static final String IMG_COLUMN_ZEROFOUR = IMAGENES_SITUACION_MONITOR + "ColumnHead_0-4.png"
	private static final String IMG_ICON_CHROME 	=IMAGENES_SITUACION_MONITOR + "Chrome_Icon"
	private static final String IMG_WINDOW_OPTIONS 	=IMAGENES_SITUACION_MONITOR + "Windows_Options"
	private ExcelData excelData = new ExcelData()

	@Keyword
	def verificacionVentanaAbierta(){
		checkThatIsVisibleWithTime(IMG_HEADER,"Panel de Situación Monitor",GlobalVariable.MINIMUM_WAIT)
		checkExactPatternIsVisibleWithTime(IMG_HEADER_DOCFORZBT,"Header Documento Forzado BT",GlobalVariable.MAXIMUM_WAIT)
		takeScreenshot("Verificacion_SituacionMonitor")
		//		checkExactPatternIsVisibleWithTime(IMG_HEADER_DOCFORZMT,"Header Documento Forzado MT",GlobalVariable.MAXIMUM_WAIT)
	}

	@Keyword
	def verificarReclamo(){
		Thread.sleep(15000)
		driver.type("a", Key.CTRL);
		driver.type("c", Key.CTRL);
		driver.type("a", Key.CTRL);
		driver.type("c", Key.CTRL);
		String clipboard = Env.getClipboard()
		String reclamos = clipboard.substring(clipboard.lastIndexOf("\t")+1)
		KeywordUtil.logInfo(">>> TOTAL de reclamos: " + reclamos)
		FileInputStream file = new FileInputStream (new File(EXCEL_PATH))
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		XSSFSheet sheet = workbook.getSheetAt(0);
		String reclamosAnteriores=sheet.getRow(2).getCell(1).getStringCellValue();
		KeywordUtil.logInfo(">>> Reclamos anteriores: " + reclamosAnteriores)
		sheet.getRow(2).createCell(1).setCellValue(reclamos);
		file.close();
		FileOutputStream outFile =new FileOutputStream(new File(EXCEL_PATH));
		workbook.write(outFile);
		outFile.close();
		checkThatNotEquals(reclamosAnteriores, reclamos, "Reclamos totales")
	}

	@Keyword
	def clickLlamadasSalientes(){
		clickElement(IMG_LLAMADAS_SALIENTES)
	}

	@Keyword
	def getNewBTDocuments(){
		driver.type(Key.END,Key.CTRL)
		for(int i = 0 ; i < 3 ; i++){
			if(!verifyElementIsVisible(IMG_HEADER_DOCPROGBT,GlobalVariable.MINIMUM_WAIT))
				driver.type(Key.END,Key.CTRL)
		}
		Region column = driver.find(IMG_HEADER_DOCPROGBT).find(IMG_COLUMN_ZEROFOUR)
		column.highlight(2)
		column.setY(column.getY() + column.getH())
		column.setY(column.getY() + column.getH())
		column.highlight(2)
	}

	@Keyword
	def getAfectadosActual(){
		String clipboard = copyData()
		for(int i = 0; i < GlobalVariable.MAXIMO_INTENTOS;i++){
			if (clipboard==null){
				clipboard = copyData()
			}
		}
		String documentoProgramadoBT = getDocumentoProgramadoData(clipboard)
		KeywordUtil.logInfo(documentoProgramadoBT)
		String clientesAfectadosActuales = getClientesAfectadosActualesData(documentoProgramadoBT)
		KeywordUtil.logInfo("Clientes Afectados de 0-4: " + clientesAfectadosActuales)
		return clientesAfectadosActuales
	}

	@Keyword
	def getAfectadosTotal(){
		String clipboard
		int indexInicial,indexFinal
		for (int i = 0 ; i < 3 ; i++){
			clipboard = copyData()
			indexInicial = clipboard.indexOf("V")
			KeywordUtil.logInfo("Index de Fin de Header: " + indexInicial.toString())
			indexFinal	 = clipboard.indexOf("Documentos Forzados")
			KeywordUtil.logInfo("Index de Comienzo Tablas: " + indexFinal.toString())
			if(indexInicial != -1 &&  indexFinal != -1)
				break
		}
		clipboard = clipboard.substring(0, indexInicial) + clipboard.substring(indexFinal)
		KeywordUtil.logInfo(clipboard)
		String documentoProgramadoBT = getDocumentoProgramadoData(clipboard)
		KeywordUtil.logInfo(documentoProgramadoBT)
		String clientesAfectadosTotal = getClientesAfectadosTotalData(documentoProgramadoBT)
		clientesAfectadosTotal = clientesAfectadosTotal.replace('.', '')
		KeywordUtil.logInfo("Clientes Afectados Total: " + clientesAfectadosTotal)
		return clientesAfectadosTotal
	}


	@Keyword
	def verificarClientesAfectados(){
		refreshPage()
		waitForRegionRefresh("Tiempo_Inicial_Afectados")
		//		getNewBTDocuments()
		int cantidadAfectados = Integer.parseInt(excelData.getCantidadDeAfectadosInicial()) + excelData.cantidadNumerosCuenta()
		checkThatEquals(getAfectadosTotal(),cantidadAfectados.toString(),"Comparando Afectados")
	}

	@Keyword
	def verificarClientesRestaurados(){
		refreshPage()
		waitForRegionRefresh("Tiempo_Inicial_Restaurados")
		//		getNewBTDocuments()
		String cantidadAfectadosInicial = excelData.getCantidadDeAfectadosInicial()
		checkThatEquals(getAfectadosTotal(),cantidadAfectadosInicial,"Comparando Afectados")
	}

	@Keyword
	def guardarAfectadosActuales(){
		verificacionVentanaAbierta()
		Thread.sleep(20000)
		String cantidadDeAfectadosTotal = getAfectadosTotal()
		KeywordUtil.logInfo("Cantidad de afectados total: " + cantidadDeAfectadosTotal)
		excelData.guardarCantidadAfectadosInicial(cantidadDeAfectadosTotal)
		driver.click(IMG_HEADER_RED)
	}

	@Keyword
	def focus(){
		Region iconChrome = driver.find(IMG_ICON_CHROME)
		driver.click(iconChrome)
		driver.wait(IMG_HEADER,GlobalVariable.MINIMUM_WAIT)
		driver.click(IMG_HEADER)
	}

	def maximizarVentana(){
		driver.click(IMG_WINDOW_OPTIONS)
	}

	@Keyword
	def getTime(){
	}

	@Keyword
	def copyData(){
		driver.type("a", Key.CTRL);
		Thread.sleep(8000)
		driver.type("c", Key.CTRL);
		driver.type("c", Key.CTRL);
		Thread.sleep(8000)
		String clipboard = Env.getClipboard()
		Thread.sleep(3000)
		return clipboard
	}

	def getDocumentoProgramadoData(String clipboard){
		int indexInicial = clipboard.indexOf("Documentos Programados")
		KeywordUtil.logInfo("Index de Documentos Programados BT: " + indexInicial.toString())
		int indexFinal	 = clipboard.indexOf("Documentos Forzados Agrupados MT")
		KeywordUtil.logInfo("Index de Documentos Forzados Agrupados MT: " + indexFinal.toString())
		return clipboard.substring(indexInicial,indexFinal)
	}

	public String getClientesAfectadosActualesData(String documentoProgramadoBT){
		String clientesAfectados = documentoProgramadoBT.substring(documentoProgramadoBT.indexOf("Clientes Afectados"))
		int indexClientesAfectados = clientesAfectados.lastIndexOf("s")+2
		String clientesAfectadosActuales = clientesAfectados.substring(indexClientesAfectados,indexClientesAfectados+1)
		return clientesAfectadosActuales
	}


	def getClientesAfectadosTotalData(String documentoProgramadoBT){
		int indexInicial = documentoProgramadoBT.lastIndexOf("\t") + 1
		String documentoAux = documentoProgramadoBT.substring(indexInicial)
		return documentoAux.trim()
	}

	def getTimeData(String clipboard){
		int indexInicial = clipboard.indexOf("-")
		int indexFinal = clipboard.indexOf("V")
	}

	@Keyword
	def waitForRegionRefresh(String filename){
		Region time
		String path
		Pattern timeExactPattern
		for(int i = 0 ; i < 3 ; i++){
			time = getRightRegion(IMG_HEADER)
			path = takeScreenshot(time,filename)
			timeExactPattern = new Pattern (path).similar(0.95)
			KeywordUtil.logInfo(getHours(time.text()))
			if(getHours(time.text())!=null || getHours(time.text())!= ""){
				break
			}
		}
		for(int i = 1 ; i <= 20 ; i++){
			if(verifyElementIsNotVisible(timeExactPattern)){
				break
			}
			else{
				KeywordUtil.logInfo(mensajeIntentos(i))
				time.highlight(1,"blue")
			}
			if (i==20){
				KeywordUtil.markFailedAndStop("Time out. Han pasado 10 minutos y la pagina no se actualizo.")
			}
		}
		takeScreenshot(time,"Tiempo_Refresh")
	}

	public String getHours(String time){
		return time.substring(time.lastIndexOf("9") + 1).trim()
	}

	@Keyword
	def checkPattern(){
		String path = "C:\\Automatizaciones\\Scripts\\nexus\\Reports\\2019\\CC\\Circuitos Criticos\\20190701_113820\\Time.jpg"
		Pattern timeExactPattern = new Pattern (path).similar(0.95)
		int i = 1
		while(verifyElementIsVisible(timeExactPattern)){
			if (i>10){
				KeywordUtil.markFailedAndStop("Time out. Han pasado 5 minutos y la pagina no se actualizo.")
			}
			Thread.sleep(30000)
			KeywordUtil.logInfo(mensajeIntentos(i))
			i++
		}
	}

	public String mensajeIntentos(int i){
		"Esperando por refresh. " + i * 30 + " segundos."
	}

	@Keyword
	def refreshPage(){
		driver.type(Key.F5,Key.CTRL)
		driver.waitVanish(IMG_HEADER_DOCFORZBT,GlobalVariable.DEFAULT_WAIT)
		KeywordUtil.logInfo("DOC HEADER VANISH")
		verificacionVentanaAbierta()
	}
}


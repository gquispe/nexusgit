package workstation

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.util.KeywordUtil

import internal.GlobalVariable

public class Configuration {
	private static final String WORKSTATION_CONFIG = "C:\\4DataLink\\4DL-Workstation-Core\\4dl-workstation-core.conf"
	private static final String QA2_PORT  = "tapp4l.pro.edenor:2809"
	private static final String QA1_PORT1 = "tapp5l.pro.edenor:2809"
	private static final String QA1_PORT2 = "tapp3l.pro.edenor:2809"

	@Keyword
	public void verifyConfigurationFile(String ambiente){

		GlobalVariable.ENVIRONMENT = ambiente

		List <String> puertos = conseguirPuertoDesdeArchivo()
		String puertoAmbiente = conseguirPuertoAmbiente(ambiente)

		KeywordUtil.logInfo("Puerto actual: " + puertos)
		KeywordUtil.logInfo("Puerto del Ambiente esperado: " + puertoAmbiente)

		if (verificarPuertoCorrecto(ambiente,puertos))
			KeywordUtil.markPassed("Puerto Correcto")
		else
			KeywordUtil.markFailedAndStop("Puerto Incorrecto")
	}

	public List <String> conseguirPuertoDesdeArchivo(){
		FileReader fileReader = new FileReader(WORKSTATION_CONFIG)
		String linea = null
		BufferedReader bufferedReader = new BufferedReader(fileReader)
		List <String> puertosObtenidos = new ArrayList <String>()

		while((linea = bufferedReader.readLine()) != null) {
			if (contieneDireccionPuerto(linea))
				puertosObtenidos.add(conseguirPuerto(linea))
		}
		return puertosObtenidos
	}

	public boolean verificarPuertoCorrecto(String ambiente,List <String> puertos){
		if (ambiente == "QA2")
			return puertos.contains(QA2_PORT)
		else if (ambiente == "QA1")
			return puertos.contains(QA1_PORT1) || puertos.contains(QA1_PORT2)
	}

	public String conseguirPuertoAmbiente(String ambiente){
		if (ambiente == "QA2")
			return QA2_PORT
		else if (ambiente == "QA1")
			return  QA1_PORT1 + " y " + QA1_PORT2
	}

	public boolean contieneDireccionPuerto(String linea){
		return linea.contains("server_address") && !linea.contains("#")
	}

	public String conseguirPuerto(String linea){
		int indexInicioPuerto = linea.indexOf('=') + 2
		return linea.substring(indexInicioPuerto)
	}
}

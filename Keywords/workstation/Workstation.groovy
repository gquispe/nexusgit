package workstation

import java.awt.Toolkit
import java.awt.datatransfer.*

import org.sikuli.script.App
import org.sikuli.script.FindFailed
import org.sikuli.script.Key
import org.sikuli.script.KeyModifier
import org.sikuli.script.Screen

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.util.KeywordUtil

import helper.CodReclamos
import helper.ScreenDriver
import internal.GlobalVariable
import helper.UIContext
import helper.CustomScreenshot


public class Workstation extends UIContext{
	/**
	 * Click en la imagen que indiques
	 * @param region, Ruta de imagen
	 */

	protected static Screen screen = ScreenDriver.getIntance()
	private ArrayList<String> codigosReclamo = CodReclamos.getIntance()
	private static final String TITULO_VENTANA = "Navigator -"

	private static final String IMAGENES_WORKSTATION =  GlobalVariable.IMAGENES + "Workstation\\"
	private static final String IMG_LOGIN = IMAGENES_WORKSTATION + "Login-Certa.png"
	private static final String ICON_WORKSTATION = IMAGENES_WORKSTATION + "Icon_main.PNG"

	private static final String IMG_LOGO_MAIN = IMAGENES_WORKSTATION + "Logo_Main.png"
	private static final String IMG_DROPDOWN_NEXUS = IMAGENES_WORKSTATION + "Dropdown_Proyecto_Nexus.png"
	private static final String IMG_LOGO_LLAVE = IMAGENES_WORKSTATION + "Logo_Llave.png"

	private static final String IMG_ICON_OPERACION = IMAGENES_WORKSTATION + "operacion-icon.png"
	private static final String IMG_MENU_CALLCENTER = IMAGENES_WORKSTATION + "CallCenter-menu.png"
	private static final String IMG_MENU_OMS = IMAGENES_WORKSTATION + "OMS-menu.png"
	private static final String IMG_MENU_SVP = IMAGENES_WORKSTATION + "SVP_Menu.png"

	private static final String IMG_ICON_CONTROL = IMAGENES_WORKSTATION + "control_icon.png"
	private static final String IMG_CDP_MENU = IMAGENES_WORKSTATION + "CDP_Menu.png"

	protected static final String IMAGENES_SVP =  GlobalVariable.IMAGENES + "SVP\\"
	private static final String IMG_ICO_SVP = IMAGENES_SVP + "SVP_Barra.PNG"

	protected static final String IMAGENES_CDP =  GlobalVariable.IMAGENES + "CDP\\"
	private static final String IMG_ICO_CDP = IMAGENES_CDP + "CDP_Barra.PNG"
	private static final String IMG_CARTO = IMAGENES_WORKSTATION + "cartografia-icon.png"
	private static final String IMG_TCA = IMAGENES_WORKSTATION + "tca-icon.png"

	@Keyword
	def startWorkstationCore() {
		ProcessBuilder certaApplication = new ProcessBuilder(GlobalVariable.CERTA)
		certaApplication.directory(new File("C:\\4DataLink\\4DL-Workstation-Core"))
		certaApplication.start()
	}

	@Keyword
	def login(String user, String password){
		checkThatIsVisibleWithTime(IMG_LOGIN,"Logo Login",10)
		screen.type(Key.TAB + Key.TAB + Key.TAB)
		KeywordUtil.logInfo("Usuario ingresado: " + user)
		screen.type(user);
		screen.type(Key.TAB);
		KeywordUtil.logInfo("Contraseña ingresada: " + password)
		screen.type(password)
		screen.type(Key.TAB)
		screen.type(Key.ENTER)
		verificacionNavigatorAbierto(user, password)
	}

	def openOperaciones(){
		focusNavigator()
		clickElement(IMG_ICON_OPERACION,GlobalVariable.MINIMUM_WAIT)
	}
	
	@Keyword
	def clickEnCartografia() {
		App.focus(TITULO_VENTANA)
		screen.click(IMG_CARTO)
		screen.click(IMG_TCA)
	}
	
	@Keyword
	def login2(String user,String password){
		checkThatIsVisibleWithTime(IMG_LOGIN,"Logo Login",10)
		screen.type(Key.TAB + Key.TAB + Key.TAB)
		KeywordUtil.logInfo("Usuario ingresado: " + user)
		screen.paste(user);
		screen.type(Key.TAB);
		KeywordUtil.logInfo("Contraseña ingresada: " + password)
		screen.paste(password)
		screen.type(Key.TAB)
		screen.type(Key.ENTER)
	}

	@Keyword
	def openOMS(){
		openOperaciones()
		if(checkThatIsVisible(IMG_MENU_OMS,"Opcion OMS")){
			screen.click(IMG_MENU_OMS)
			GlobalVariable.START_TIME = System.currentTimeMillis()
		}
		else
			openOMS()
	}

	@Keyword
	def openCallCenter(){
		openOperaciones()
		if(checkThatIsVisible(IMG_MENU_CALLCENTER,"Opcion Call Center"))
			screen.click(IMG_MENU_CALLCENTER)
		else
			openCallCenter()
	}

	@Keyword
	def openSVP(){
		openOperaciones()
		if(checkThatIsVisible(IMG_MENU_SVP,"Opcion SVP"))
			screen.click(IMG_MENU_SVP)
		else
			openOMS()
		screen.wait(IMG_ICO_SVP,90)
	}

	@Keyword
	def openCDP(ambiente){
		if (ambiente=="QA1"){
			App.focus(TITULO_VENTANA)
			screen.wait(IMG_ICON_CONTROL,30);
			screen.click(IMG_ICON_CONTROL,30);
			screen.click(IMG_CDP_MENU);
			screen.wait(IMG_ICO_CDP,90)
		}
		else
			KeywordUtil.markFailedAndStop("Error: CDP solo se puede ejecutar en QA1. Ambiente actual: " + ambiente)
	}

	@Keyword
	def cerrarApps(){
		eliminarProceso('chrome')
		eliminarProceso('SVP_ENRE')
		eliminarProceso('OMSApplication')
		eliminarProceso('DsCallCenter')
		eliminarProceso('NavigatorApp')
		eliminarProceso('certa-workstation-core')
		eliminarProceso('4dl-workstation-svc')
		eliminarProceso('CalidadProductoNET')
		Thread.sleep(1000)
	}

	def eliminarProceso(String proceso) {
		Runtime.getRuntime().exec("taskkill /F /IM " + proceso + '.exe')
	}


	@Keyword
	def verificacionNavigatorAbierto(user,password){
		int i
		for (i = GlobalVariable.LOGINTRY; i < GlobalVariable.MAXIMO_INTENTOS; i++){
			if(!loginCorrecto()){
				def bool = loginCorrecto()
				KeywordUtil.logInfo(bool.toString() + " "+i.toString())
				cerrarApps()
				startWorkstationCore()
				login2(user,password)
			}
			else
				break
		}
		if (i==5)
			KeywordUtil.markFailedAndStop("Error en el login. " + GlobalVariable.MAXIMO_INTENTOS.toString() + " intentos.")
	}

	private loginCorrecto(){
		return verifyElementIsVisible(IMG_DROPDOWN_NEXUS, "Dropdown Proyecto Nexus",GlobalVariable.MINIMUM_WAIT)
	}


	@Keyword
	def loginCorrecto2(){
		if (verifyElementIsVisible(IMG_DROPDOWN_NEXUS, "Dropdown Proyecto Nexus",GlobalVariable.DEFAULT_WAIT))
			KeywordUtil.logInfo("Login Correcto")
		else
			KeywordUtil.logInfo("Login Incorrecto")
	}

	@Keyword
	def saveCodReclamo(){
		Toolkit toolkit = Toolkit.getDefaultToolkit()
		Clipboard clipboard = toolkit.getSystemClipboard()
		String clipActual = (String) clipboard.getData(DataFlavor.stringFlavor)
		KeywordUtil.logInfo(clipActual)
		if(clipActual[0]=="R"){
			codigosReclamo.add(clipActual)
			System.out.println(codigosReclamo);
			KeywordUtil.logInfo('Codigo de Reclamo:' + clipActual)
		}
		else{
			KeywordUtil.logInfo("Codigo de Reclamo no valido: " + clipActual)
		}
	}
	
	@Keyword
	def focusNavigator(){
		focusWindow(IMG_DROPDOWN_NEXUS,ICON_WORKSTATION)
	}
}

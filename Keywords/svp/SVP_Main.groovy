package svp

import org.sikuli.script.App
import org.sikuli.script.KeyModifier
import org.sikuli.script.Screen

import com.kms.katalon.core.annotation.Keyword

import helper.ScreenDriver
import internal.GlobalVariable

public class SVP_Main extends helper.UIContext{

	protected static Screen screen = ScreenDriver.getIntance()

	private static final String TITULO_VENTANA = "Monitor de reclamos SVP/ENRE"
	protected static final String IMAGENES_SVP =  GlobalVariable.IMAGENES + "SVP\\"
	private static final String IMG_ICO_SVP = IMAGENES_SVP + "SVP_Barra.PNG"
	private static final String IMG_SVP_MAIN_ACCIONES = IMAGENES_SVP + "SVP_AccionesTitle.png"
	private static final String IMG_SVP_MAIN_VISTAS = IMAGENES_SVP + "SVP_VistasTitle.png"
	private App svpApp = new App("C:\\4DataLink\\4DL-SVP_ENRE\\SVP_ENRE.exe")

	@Keyword
	def focus() {
//		screen.type("d", KeyModifier.WIN);
		screen.wait(IMG_ICO_SVP,GlobalVariable.MAXIMUM_WAIT)
		App.focus(TITULO_VENTANA)
		screen.wait(IMG_SVP_MAIN_ACCIONES,GlobalVariable.MAXIMUM_WAIT)
	}

	@Keyword
	def verificarInicioDeSVP(){
		checkThatEquals(TITULO_VENTANA, svpApp.getWindow(), "Verificando Titulo de ventana")
		checkThatIsVisible(IMG_SVP_MAIN_ACCIONES, "Se visualizan las secciones Acciones")
		checkThatIsVisible(IMG_SVP_MAIN_VISTAS, "Se visualizan las secciones Vistas")
	}
}

package nexus

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.util.KeywordUtil

import java.util.List

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import java.awt.Toolkit
import java.awt.datatransfer.*
import callCenter.Tipos_Reclamo
import internal.GlobalVariable
import helper.DocUtils

public class Script705_ExcelData {

	private static final EXCEL_FILENAME = "C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\Script_705.xlsx"


	@Keyword
	public List<Tipos_Reclamo> getTiposReclamo(int cantidadReclamos){
		List<Tipos_Reclamo> tiposReclamo = new ArrayList<Tipos_Reclamo>()
		String tipo,idCliente,motivo,submotivo
		for (int i = 0 ; i < cantidadReclamos; i++){
			idCliente = DocUtils.readCellNoLog(EXCEL_FILENAME,0,i+1,0)
			tipo 	  = DocUtils.readCellNoLog(EXCEL_FILENAME,0,i+1,2)
			motivo	  = DocUtils.readCellNoLog(EXCEL_FILENAME,0,i+1,3)
			submotivo = DocUtils.readCellNoLog(EXCEL_FILENAME,0,i+1,4)
			tiposReclamo.add(new Tipos_Reclamo(tipo,idCliente,motivo,submotivo))
		}
		return tiposReclamo
	}

	@Keyword
	public List<Tipos_Reclamo> getCodReclamosAndTipoDocumento(int cantidadReclamos){
		List<Tipos_Reclamo> tiposReclamo = new ArrayList<Tipos_Reclamo>()
		String tipoDocumento, codReclamo
		for (int i = 0 ; i < cantidadReclamos; i++){
			tipoDocumento = DocUtils.readCellNoLog(EXCEL_FILENAME,0,i+1,5)
			codReclamo 	  = DocUtils.readCellNoLog(EXCEL_FILENAME,0,i+1,6)
			tiposReclamo.add(new Tipos_Reclamo(tipoDocumento,codReclamo))
		}
		return tiposReclamo
	}

	@Keyword
	public List<Tipos_Reclamo> getAllData(int cantidadReclamos){
		List<Tipos_Reclamo> tiposReclamo = new ArrayList<Tipos_Reclamo>()
		String tipo,idCliente,motivo,submotivo,tipoDocumento, codReclamo
		for (int i = 0 ; i < cantidadReclamos; i++){
			idCliente 	  = DocUtils.readCellNoLog(EXCEL_FILENAME,0,i+1,0)
			tipo 	  	  = DocUtils.readCellNoLog(EXCEL_FILENAME,0,i+1,2)
			motivo	  	  = DocUtils.readCellNoLog(EXCEL_FILENAME,0,i+1,3)
			submotivo	  = DocUtils.readCellNoLog(EXCEL_FILENAME,0,i+1,4)
			tipoDocumento = DocUtils.readCellNoLog(EXCEL_FILENAME,0,i+1,5)
			codReclamo 	  = DocUtils.readCellNoLog(EXCEL_FILENAME,0,i+1,6)
			tiposReclamo.add(new Tipos_Reclamo(tipo,idCliente,motivo,submotivo,tipoDocumento,codReclamo))
		}
		return tiposReclamo
	}

	@Keyword
	def saveCodReclamo(int i){
		Toolkit toolkit = Toolkit.getDefaultToolkit()
		Clipboard clipboard = toolkit.getSystemClipboard()
		String clipActual = (String) clipboard.getData(DataFlavor.stringFlavor)
		KeywordUtil.logInfo(clipActual)
		if(clipActual[0]=="R"){
			DocUtils.writeCell(EXCEL_FILENAME, 0, i+1, 6, clipActual)
			KeywordUtil.logInfo('Codigo de Reclamo: ' + clipActual)
		}
		else{
			KeywordUtil.logInfo("Codigo de Reclamo no valido: " + clipActual)
		}
		StringSelection selection = new StringSelection("ERROR");
		clipboard.setContents(selection, selection);
	}

	@Keyword
	public String getCodigoReclamo(int i){
		return DocUtils.readCell(EXCEL_FILENAME, 0, i+1, 6)
	}

	@Keyword
	def saveTipoDocumentoObtenido(int i, String tipoDocumentoObtenido){
		DocUtils.writeCell(EXCEL_FILENAME, 0, i+1, 7, tipoDocumentoObtenido)
	}
}

 package nexus

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class Main {

	private TestCase workstation 	  	   = findTestCase('Workstation Core/Workstation_Inicio')
	private TestCase callCenter  	  	   = findTestCase('Call Center/CallCenter_Inicio')
	private TestCase callCenterAbrir 	   = findTestCase('Call Center/CallCenter_Abrir')
	private TestCase oms		 	  	   = findTestCase('OMS/OMS_Inicio')
	private TestCase omsAbrir		  	   = findTestCase('OMS/OMS_Abrir')
	private TestCase situacionMonitor 	   = findTestCase('Situacion Monitor/SituacionMonitor_Inicio')
	private TestCase situacionMonitorAbrir = findTestCase('Situacion Monitor/SituacionMonitor_Abrir')
	private TestCase cromo		 	  	   = findTestCase('Cromo/Cromo_Inicio')
	private TestCase aplicacionesNexus	   = findTestCase('CC/Abrir Aplicaciones Nexus')
	private TestCase omsCromo			   = findTestCase('Test Cases/Nexus/Abrir OMS con Cromo')
	
	@Keyword
	def inicioWorkstation(String usuario, String password, String ambiente){
		callTestCase(workstation,usuario,password,ambiente)
	}

	@Keyword
	def inicioCallCenter(String usuario, String password, String ambiente){
		callTestCase(callCenter,usuario,password,ambiente)
	}

	@Keyword
	def inicioCromo(String usuario, String password, String ambiente){
		callTestCase(cromo,usuario,password,ambiente)
	}

	@Keyword
	def inicioOMS(String usuario, String password, String ambiente){
		callTestCase(oms,usuario,password,ambiente)
	}

	@Keyword
	def inicioSituacionMonitor(String usuario,String password, String ambiente){
		callTestCase(situacionMonitor,usuario,password,ambiente)
	}

	@Keyword
	def abrirOMS(){
		WebUI.callTestCase(omsAbrir, [:])
	}

	@Keyword
	def abrirSituacionMonitor(){
		WebUI.callTestCase(situacionMonitorAbrir, [:])
	}

	@Keyword
	def abrirCallCenter(){
		WebUI.callTestCase(callCenterAbrir, [:])
	}

	@Keyword
	def abrirAplicacionesNexus(String usuario,String password, String ambiente){
		callTestCase(aplicacionesNexus,usuario,password,ambiente)
	}
	
	@Keyword
	def abrirOMSConCromo(String usuario,String password, String ambiente){
		callTestCase(omsCromo,usuario,password,ambiente)
	}
	
	def callTestCase(TestCase testCase,String usuario, String password, String ambiente){
		WebUI.callTestCase(testCase,variablesDefault(usuario,password,ambiente))
	}

	def variablesDefault(String usuario, String password, String ambiente){
		return [('usuario') : usuario, ('password') : password, ('ambiente') : ambiente]
	}

	def ejemplo(){
		WebUI.callTestCase(findTestCase('Situacion Monitor/SituacionMonitor_Inicio'), [('usuario') : 'OMS_DES_NORTE', ('password') : 'Edenor2014'
			, ('ambiente') : 'QA2', ('testcaseid') : ''], FailureHandling.STOP_ON_FAILURE)
	}
}

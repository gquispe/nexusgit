package nexus

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.util.KeywordUtil

import internal.GlobalVariable
import helper.ExcelData
import helper.UIContext

public class CircuitoCritico1 {


	private TestCase paso1  = findTestCase('CC/Unificado/01 - Abrir Aplicaciones Nexus')
	private TestCase paso1B = findTestCase('CC/Unificado/01.5 - ObtenerAfectadosIniciales')
	private TestCase paso2  = findTestCase('CC/Unificado/02 - OMS_DocumentoProgramadoBT_Creacion')
	private TestCase paso3  = findTestCase('CC/Unificado/03 - OMS_DocumentoProgramadoBT_Afectacion')
	private TestCase paso4  = findTestCase('CC/Unificado/04 - OMS_DocumentoProgramadoBT_DespachoMovil')
	private TestCase paso5  = findTestCase('CC/Unificado/05 - CallCenter_AltaDeReclamos_AsociarADocProgBT')
	private TestCase paso6  = findTestCase('CC/Unificado/06 - OMS_SituacionMonitor_VerificacionDeDocumentos')
	private TestCase paso7  = findTestCase('CC/Unificado/07 - OMS_SituacionMonitor_LlamadasSalientes')
	private TestCase paso8  = findTestCase('CC/Unificado/08 - OMS_DocumentoProgramadoBT_Restauracion')
	private TestCase paso9  = findTestCase('CC/Unificado/09 - OMS_SituacionMonitor_VerificacionDeClientesRestaurados')
	private TestCase paso10 = findTestCase('CC/Unificado/10 - OMS_DocumentoProgramadoBT_CierreDeReclamos')
	private TestCase paso11A=findTestCase('CC/Unificado/11 - OMS_DocumentoProgramado_OperarElemento_Apertura - Setup')
	private TestCase paso11 = findTestCase('CC/Unificado/11 - OMS_DocumentoProgramado_OperarElemento_Apertura')
	private TestCase paso12 = findTestCase('CC/Unificado/12 - CallCenter_AltaDeReclamos_AsociarDocProgBT')
	private TestCase paso13 = findTestCase('CC/Unificado/13 - OMS_DocumentoProgramadoBT_OperarElemento_Cierre')
	private TestCase paso14 = findTestCase('CC/Unificado/14 - OMS_DocumentoProgramadoBT_CierreProvisoroDeDocumento')
	private TestCase paso15 = findTestCase('CC/Unificado/15 - OMS_DocumentoProgramadoBT_CambioTipoDeRegistracion')
	private TestCase paso16 = findTestCase('CC/Unificado/16 - OMS_DocumentoProgramadoBT_CierreDeUnDocumentoPostOperacion')
	private TestCase postCondiciones = findTestCase('CC/Unificado/PostCondiciones')
	private ExcelData excelData = new ExcelData()

	@Keyword
	def paso1AbrirAplicacionesNexus(String usuario, String password, String ambiente){
		WebUI.callTestCase(paso1, [('usuario') : usuario, ('password') : password, ('ambiente') : ambiente])
	}

	@Keyword
	def paso2CreacionDocumentoProgramadoBT(String tipoDocumento, String minutos, String coordenadas){
		WebUI.callTestCase(paso2, [('tipo_documento') : tipoDocumento
			, ('minutos') : minutos, ('coordenadas') : coordenadas])
	}

	@Keyword
	def paso3AfectacionDocumentoProgramadoBT(String tipoAfectacion,String idLocalidad, String idSuministro){
		WebUI.callTestCase(paso3, [('tipo_afectacion') : tipoAfectacion, ('id_localidad') : idLocalidad,
			('id_suministro') : idSuministro])
	}

	@Keyword
	def paso4DespachoMovilDocumentoProgramado(String equipoMovil, String horasRestauracion){
		WebUI.callTestCase(paso4, [('equipo_movil') : equipoMovil, ('horas_restauracion') : horasRestauracion],
		)
	}

	@Keyword
	def paso5AltaDeReclamosAsociarADocProgBT(){
		callTestCaseIteracionesPorCuenta(paso5, excelData.getListaNumeroCuentas())
	}

	@Keyword
	def paso6VerificacionDeDocumentosSituacionMonitor(){
		WebUI.callTestCase(paso6, [:])
	}

	@Keyword
	def paso7LlamadasSalientesSituacionMonitor(String ambiente){
		WebUI.callTestCase(paso7, [('usuario') : 'OMS_JEFES_DBT', ('password') : 'Edenor2014'
			, ('ambiente') : ambiente])
	}

	@Keyword
	def paso8RestauracionDocumentoProgramadoBT(String usuario, String password, String ambiente, String idSuministro){
		String numeroDocumento = excelData.getNumeroDocumento()
		WebUI.callTestCase(paso8, [('usuario') : usuario, ('password') : password, ('ambiente') : ambiente,
			('numero_documento') : numeroDocumento, ('tipo_seleccion') : 'Topologica',
			('id_localidad') : '369', ('id_suministro') : idSuministro])
	}

	@Keyword
	def paso9VerificarClientesRestaurados(){
		WebUI.callTestCase(paso9, [:])
	}

	@Keyword
	def paso10CerrarReclamosDocProgramadoBT(){
		int cantidadDeReclamos = excelData.cantidadNumerosCuenta()
		for (int i=0; i<cantidadDeReclamos ; i++){
			WebUI.callTestCase(paso10, [('numero_reclamo') : i.toString()])
		}
	}

	@Keyword
	def paso11ManiobraAperturaDocumento(String usuario, String password, String ambiente){
		String numeroDocumento = excelData.getNumeroDocumento()
//		WebUI.callTestCase(paso11A, [('usuario') : usuario, ('password') : password, ('ambiente') : ambiente, ('numero_documento') : numeroDocumento])
		callTestCaseIteracionesPorCuenta(paso11,excelData.getListaNumeroCuentasSensibles())
	}

	@Keyword
	def paso12DarDeAltaReclamosClienteSensibles(){
		callTestCaseIteracionesPorCuenta(paso12,excelData.getListaNumeroCuentasSensibles())
	}

	@Keyword
	def paso13ManiobraCierreDocumento(){
		callTestCaseIteracionesPorCuenta(paso13,excelData.getListaNumeroCuentasSensibles())
	}

	@Keyword
	def paso14CierreProvisorioDeDocumento(){
		WebUI.callTestCase(paso14, [:])
	}

	@Keyword
	def paso15CambioTipoRegistracion(String ambiente){
		String numeroDocumento = excelData.getNumeroDocumento()
		WebUI.callTestCase(paso15, [('usuario') : 'OMS_SUP_CENT'
			, ('password') : 'Edenor2015', ('ambiente') : ambiente, ('numero_documento') : numeroDocumento])
	}

	@Keyword
	def paso16CierreDeDocumentoPostOperacion(){
		WebUI.callTestCase(paso16, [:])
	}

	@Keyword
	def resultadoEsperado(){
		WebUI.callTestCase(postCondiciones, [:])
	}

	def callTestCaseIteracionesPorCuenta(TestCase testCase, List <String> excelList){
		List <String> numerosCuenta = excelList
		KeywordUtil.logInfo("Numero de Iteraciones: " + numerosCuenta.size())
		numerosCuenta.each{ numeroCuenta ->
			KeywordUtil.logInfo("ID del Cliente: " + numeroCuenta)
			WebUI.callTestCase(testCase, [('id_cliente') : numeroCuenta])
		}
	}

	@Keyword
	def paso1B_guardarAfectadosActuales(){
		WebUI.callTestCase(paso1B, [:])
	}
}

package nexus
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.util.KeywordUtil

import internal.GlobalVariable
import helper.CodReclamos
import callCenter.Tipos_Reclamo
import callCenter.Page_Administracion_De_Reclamos

public class Script705 {

	private TestCase caso1 = findTestCase('Script 705/Generar Reclamo')
	private TestCase caso2 = findTestCase('Script 705/Verificar Tipo de Documento')
	private Script705_ExcelData data = new Script705_ExcelData()
	private String tipo,idCliente,motivo,submotivo,tipoDocumento,codigoReclamo
	private Page_Administracion_De_Reclamos callCenter = new Page_Administracion_De_Reclamos()

	@Keyword
	def caso1_GenerarReclamo(int cantidadReclamos){
		List <Tipos_Reclamo> tiposReclamo = data.getTiposReclamo(cantidadReclamos)
		int i = 0
		tiposReclamo.each{ tipoReclamo ->
			tipo 	  = tipoReclamo.tipo()
			idCliente = tipoReclamo.codigoCliente()
			motivo	  = tipoReclamo.motivo()
			submotivo = tipoReclamo.submotivo()
			KeywordUtil.logInfo("======================= Iteracion " 	  + (i+1).toString() + " =======================")
			KeywordUtil.logInfo("Tipo Reclamo: " 	  + tipo)
			KeywordUtil.logInfo("ID del Cliente: " 	  + idCliente)
			KeywordUtil.logInfo("Motivo Reclamo: "    + motivo)
			KeywordUtil.logInfo("Submotivo Reclamo: " + submotivo)

			WebUI.callTestCase(caso1, [('tipo') : tipo , ('motivo') : motivo
				, ('sub_motivo') : submotivo, ('id_cliente') : idCliente, ('usuario') : 'OMS_DES_CENT'
				, ('password') : 'Edenor2016'])
			data.saveCodReclamo(i)
			callCenter.cerrarApp()
			i++
		}
	}

	@Keyword
	def caso2_VerificarTipoDeDocumento(int cantidadReclamos){
		List <Tipos_Reclamo> tiposReclamo = data.getAllData(cantidadReclamos)
		int i = 0
		tiposReclamo.each{ tipoReclamo ->
			tipo 	  = tipoReclamo.tipo()
			idCliente = tipoReclamo.codigoCliente()
			motivo	  = tipoReclamo.motivo()
			submotivo = tipoReclamo.submotivo()
			tipoDocumento = tipoReclamo.tipoDocumento()
			codigoReclamo = tipoReclamo.codigoReclamo()
			KeywordUtil.logInfo("======================= Iteracion " + (i+1).toString() + " =======================")
			KeywordUtil.logInfo("Tipo Reclamo: " 	  + tipo)
			KeywordUtil.logInfo("ID del Cliente: " 	  + idCliente)
			KeywordUtil.logInfo("Motivo Reclamo: "    + motivo)
			KeywordUtil.logInfo("Submotivo Reclamo: " + submotivo)
			KeywordUtil.logInfo("Tipo de Documento Esperado: " + tipoDocumento)
			KeywordUtil.logInfo("Codigo Reclamo ingresado: "   + codigoReclamo)

			WebUI.callTestCase(caso2, [('tipo_forzado') : tipoDocumento, ('codigo_reclamo') : codigoReclamo,
				('iteracion') : i.toString()])
			i++
		}
	}
}

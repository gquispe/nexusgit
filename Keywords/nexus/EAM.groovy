package nexus

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

public class EAM {

	private TestCase creacionAnomalia  = findTestCase('EAM/Subcaso/Creacion Anomalia')
	private TestCase verificarNumeroOT = findTestCase('EAM/Subcaso/Verificar Numero OT Generado')
	
	@Keyword
	def crearAnomaliaConDocumento(String zona, String partido, String localidad, String idElemento, String calle, String entreCalle1, String instalacion,
			String tipoAnomalia, String clasificacion, String prioridad){
		WebUI.callTestCase(creacionAnomalia,
				[('zona') : zona, ('partido') : partido, ('localidad') : localidad, ('calle') : calle,
					('entreCalle1') : entreCalle1, ('instalacion') : instalacion, ('tipoAnomalia') : tipoAnomalia,
					('clasificacion') : clasificacion, ('prioridad') : prioridad, ('idElemento') : idElemento])
	}
			
	@Keyword
	def verificarNumeroOTGenerado(int index){
		WebUI.callTestCase(verificarNumeroOT, [('index_anomalia') : index],FailureHandling.CONTINUE_ON_FAILURE)
		}
}

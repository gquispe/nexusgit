package helper

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable
import junit.framework.Assert
import junit.framework.AssertionFailedError

public class WebUIContext {

	@Keyword
	def startCromo(){
		WebUI.openBrowser('')
		WebUI.navigateToUrl(GlobalVariable.CROMO_URL)
		WebUI.maximizeWindow()
		WebUI.waitForPageLoad(GlobalVariable.DEFAULT_WAIT)
	}

	def setText(element, text){
		KeywordUtil.logInfo(">>> Ingresando texto '" + text + "' en " + element)
		WebUI.clearText(element)
		WebUI.setText(element, text)
	}

	def click(element){
		KeywordUtil.logInfo(">>> Clickeando en " + element)
		WebUI.click(element)
	}

	def checkThatIsVisible(element, message){
		KeywordUtil.logInfo(">>> Checkeando visibilidad de " + element)
		try{
			Assert.assertTrue(WebUI.verifyElementVisible(element))
		} catch (AssertionFailedError e){
			WebUI.takeScreenshot()
			throw new StepFailedException(message)
		}
		WebUI.takeScreenshot()
	}

	protected void checkThatTextIsPresent(String text, String message){
		KeywordUtil.logInfo(">>> Checkeando presencia de texto  '" + text + "'")
		try{
			Assert.assertTrue(WebUI.verifyTextPresent(text, false))
		} catch (AssertionFailedError e){
			WebUI.takeScreenshot()
			throw new StepFailedException(message)
		}
		WebUI.takeScreenshot()
	}

	protected void checkThatObjectContainsText(TestObject object, String text, String message){
		KeywordUtil.logInfo(">>> Checkeando presencia de texto  '" + text + "' en objeto " + object)
		try{
			Assert.assertTrue(WebUI.getText(object).contains(text))
		} catch (AssertionFailedError e){
			WebUI.takeScreenshot()
			throw new StepFailedException(message)
		}
		WebUI.takeScreenshot()
	}

	protected void checkThatIsNotVisible(TestObject object, String message){
		KeywordUtil.logInfo(">>> Checkeando no visibilidad de " + object)
		try{
			Assert.assertFalse(WebUI.verifyElementVisible(object))
		} catch (AssertionFailedError e){
			WebUI.takeScreenshot()
			throw new StepFailedException(message)
		}
		WebUI.takeScreenshot()
	}

	def popUpPresent() {
		KeywordUtil.logInfo(">>> Checkeando presencia de PopUp")
		try {
			WebUI.switchToWindowIndex(1)
			return true
			KeywordUtil.logInfo(">>> PopUp Presente")
		}
		catch (StepFailedException Ex) {
			return false
			KeywordUtil.logInfo(">>> No PopUp")
		}
	}

	@Keyword
	def startGI(){
		WebUI.openBrowser('')
		WebUI.navigateToUrl(GlobalVariable.GI_URL)
		WebUI.maximizeWindow()
		WebUI.waitForPageLoad(GlobalVariable.DEFAULT_WAIT)
	}

	@Keyword
	def startCromo(ambiente){
		WebUI.openBrowser('')
		if(ambiente == "QA1"){
			KeywordUtil.logInfo(">>> NAVEGANDO A " + ambiente + " URL: " + GlobalVariable.CROMO_QA1_URL);
			WebUI.navigateToUrl(GlobalVariable.CROMO_QA1_URL)
		}
		if(ambiente == "QA2"){
			KeywordUtil.logInfo(">>> NAVEGANDO A " + ambiente + " URL: " + GlobalVariable.CROMO_QA2_URL);
			WebUI.navigateToUrl(GlobalVariable.CROMO_QA2_URL)
		}
		WebUI.maximizeWindow()
		WebUI.waitForPageLoad(GlobalVariable.DEFAULT_WAIT)
	}

	@Keyword
	def startSituacionMonitor(){
		WebUI.openBrowser('')
		WebUI.maximizeWindow()
		WebUI.waitForPageLoad(GlobalVariable.DEFAULT_WAIT)
		WebUI.delay(90)
	}

	def checkThatIsVisibleMultiCheck(element, times, wait, message){
		KeywordUtil.logInfo(">>> Checkeando " + times + " veces cada " + wait + " segundos la visibilidad de " + element)
		boolean result = false;
		int i = 0;
		while(i < times) {
			try{
				Assert.assertTrue(WebUI.verifyElementVisible(element))
				WebUI.takeScreenshot()
			} catch (AssertionFailedError e){
				WebUI.takeScreenshot()
				throw new StepFailedException(message)
				WebUI.takeScreenshot()
			}
			WebUI.delay(wait)
			i++;
		}
	}
}

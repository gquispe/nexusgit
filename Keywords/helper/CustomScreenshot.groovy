package helper

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import java.io.File
import javax.imageio.ImageIO
import internal.GlobalVariable
import java.text.SimpleDateFormat
import java.util.Date
import org.sikuli.script.Screen
import org.sikuli.script.Region
import org.sikuli.script.KeyModifier
import org.sikuli.script.Key
import com.kms.katalon.core.configuration.RunConfiguration
import helper.ConfigHelper
import com.kms.katalon.core.util.KeywordUtil
import helper.ScreenDriver

public class CustomScreenshot {
	private static final String IMAGE_FORMAT = "jpg"
	private String timeStamp = new SimpleDateFormat("HHmmssSSS").format(new Date())
	private String path = new ConfigHelper().getCompletePath2()

	public CustomScreenshot(){
	}

	public CustomScreenshot(Screen screen, String filename){

		String completeFilename =	path + filename + "_" + timeStamp +"." + IMAGE_FORMAT
		KeywordUtil.logInfo('Imagen ' + completeFilename + ' creada con exito.')
		ImageIO.write(screen.capture().getImage(), IMAGE_FORMAT, new File(completeFilename))
		Thread.sleep(1000)
	}

	public CustomScreenshoot(Screen screen,Region region, String filename){
		String completeFilename =	path + filename + "_" + timeStamp +"." + IMAGE_FORMAT
		KeywordUtil.logInfo('Imagen ' + completeFilename + ' creada con exito.')
		ImageIO.write(screen.capture(region).getImage(), IMAGE_FORMAT, new File(completeFilename))
		Thread.sleep(1000)
	}

	public String capture(Screen screen,Region region, String filename){
		String completeFilename =	path + filename + "." + IMAGE_FORMAT
		KeywordUtil.logInfo('Imagen ' + completeFilename + ' creada con exito.')
		ImageIO.write(screen.capture(region).getImage(), IMAGE_FORMAT, new File(completeFilename))
		Thread.sleep(1000)
		return completeFilename
	}
}

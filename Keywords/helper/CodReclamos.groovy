package helper

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.util.ArrayList

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable
import org.sikuli.script.Screen as Screen
import org.sikuli.api.API
import org.sikuli.script.App
import org.sikuli.script.Button
import org.sikuli.script.FindFailed
import org.sikuli.script.Key
import org.sikuli.script.KeyModifier
import org.sikuli.script.Match
import org.sikuli.script.Pattern
import java.awt.datatransfer.*
import java.awt.Toolkit
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import com.kms.katalon.core.util.KeywordUtil

public class CodReclamos {

	public static ArrayList<String> listaCodigoReclamos;
	protected static Screen screen = ScreenDriver.getIntance()
	private static final IMG_TITLE_WINDOW_EXCEL = "C:\\Katalon Studio\\Nexus\\imagenes\\Utils\\title.PNG"
	private static final IMG_EXCEL_HEADER = "C:\\Katalon Studio\\Nexus\\imagenes\\Utils\\EXCEL_HEADER.PNG"
	private static final TEXT_FILANAME = "C:\\Robots\\Script 705\\CodigosDeReclamoGenerados.txt"

	public static ArrayList<String> getIntance(){
		if (listaCodigoReclamos == null)
			createDriver();
		return listaCodigoReclamos;
	}

	public getCurrentCodigoReclamo(){
		String currentCodigoReclamo = listaCodigoReclamos.get(GlobalVariable.CONTADOR)
		System.out.println(currentCodigoReclamo)
		GlobalVariable.CONTADOR++
		return currentCodigoReclamo
	}

	private static void createDriver(){
		listaCodigoReclamos = new  ArrayList<String>();
	}

	public static ArrayList<String> getLast(){
		return getIntance().get(getIntance().size() - 1)
	}

	@Keyword
	public void saveData(){
		openExcel()
		listaCodigoReclamos.each{codigoReclamo ->
			screen.paste(codigoReclamo)
			screen.type(Key.ENTER)
		}
		screen.type(Key.'S',KeyModifier.CTRL)
		screen.type(Key.F4,KeyModifier.ALT)
		screen.type(Key.ENTER)
	}

	@Keyword
	public void saveTextData(){
		FileWriter fileWriter =	new FileWriter(TEXT_FILANAME);
		BufferedWriter bufferedWriter =      new BufferedWriter(fileWriter);
		listaCodigoReclamos.each{codigoReclamo ->
			bufferedWriter.write(codigoReclamo)
			bufferedWriter.newLine();
		}
		bufferedWriter.close();
	}

	@Keyword
	public void getTextData(){
		getIntance()
		FileReader fileReader = new FileReader(TEXT_FILANAME);
		String line = null;
		BufferedReader bufferedReader = new BufferedReader(fileReader);

		while((line = bufferedReader.readLine()) != null) {
			listaCodigoReclamos.add(line)
		}
		System.out.println(listaCodigoReclamos)
	}

	private openExcel() {
		screen.type("r", KeyModifier.WIN)
		screen.paste("C:\\Automatizaciones\\Scripts\\Nexus_V2\\Datos de Entrada\\auxiliar.xlsx")
		screen.type(Key.ENTER)
		screen.wait(IMG_TITLE_WINDOW_EXCEL,30)
		while(!screen.exists(IMG_EXCEL_HEADER))
			screen.type(Key.UP,KeyModifier.CTRL)
		screen.click(IMG_EXCEL_HEADER)
		screen.type(Key.DOWN)
	}

	@Keyword
	public static void quit(){
		if(listaCodigoReclamos != null){
			listaCodigoReclamos.finalize();
			listaCodigoReclamos = null;
		}
	}

	@Keyword
	public void getData(){
		createDriver()
		openExcel()
		screen.type("c", KeyModifier.CTRL)
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Clipboard clipboard = toolkit.getSystemClipboard();
		String result = (String) clipboard.getData(DataFlavor.stringFlavor)

		while(result!="") {
			listaCodigoReclamos.add(result)
			screen.type(Key.DOWN)
			screen.type("c", KeyModifier.CTRL)
			toolkit = Toolkit.getDefaultToolkit();
			clipboard = toolkit.getSystemClipboard();
			result = (String) clipboard.getData(DataFlavor.stringFlavor)
		}
	}
}
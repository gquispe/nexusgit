package helper

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.util.KeywordUtil
import helper.DocUtils

import internal.GlobalVariable

public class ExcelData {

	private static final String EXCEL_DATOS_ANTERIORES = "C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\DatosAnteriores.xlsx"
	private static final String EXCEL_CLIENTES_SENSIBLES = "C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\CC.xlsx"

	@Keyword
	public List<String> getListaNumeroCuentas(String Circuito){

		List <String> numerosCuenta = new  ArrayList<String>()
		int cantidadNumerosCuenta = cantidadNumerosCuenta()
		for(int i = 0; i < cantidadNumerosCuenta; i++){
			numerosCuenta.add(DocUtils.readCell(EXCEL_DATOS_ANTERIORES, 1, i+1, 0))
		}
		return numerosCuenta
	}

	public int cantidadNumerosCuenta(){
		return DocUtils.readIntCell(EXCEL_DATOS_ANTERIORES,1,1,1)
	}

	@Keyword
	public String getNumeroDocumento(){
		return DocUtils.readCell(EXCEL_DATOS_ANTERIORES, 1, 1, 2)
	}

	public String getCantidadDeAfectadosInicial(){
		return DocUtils.readCell(EXCEL_DATOS_ANTERIORES, 1, 1, 3)
	}

	@Keyword
	public testLeerCantidadAfectadosInicial(){
		KeywordUtil.logInfo(getCantidadDeAfectadosInicial().toString())
	}

	@Keyword
	public void guardarCantidadDeClientes(List<WebElement> listaDeClientes){
		DocUtils.writeOldCell(EXCEL_DATOS_ANTERIORES, 1, 1, 1, listaDeClientes.size())
	}

	@Keyword
	public void guardarNumeroDeDocumento(String numeroDocumento){
		DocUtils.writeOldCell(EXCEL_DATOS_ANTERIORES, 1, 1, 2, numeroDocumento)
	}

	@Keyword
	public void guardarCantidadAfectadosInicial(String cantidadAfectados){
		cantidadAfectados = cantidadAfectados.replace('.', '')
		DocUtils.writeOldCell(EXCEL_DATOS_ANTERIORES, 1, 1, 3, cantidadAfectados)
	}

	@Keyword
	public void guardarFechaHoraManiobraApertura(String fechaHora){
		DocUtils.writeOldCell(EXCEL_DATOS_ANTERIORES, 1, 1, 3, fechaHora)
	}

	@Keyword
	public List <String> getListaNumeroCuentasSensibles(){
		List <String> numerosCuenta = new  ArrayList<String>()
		for(int i = 0; i < 2; i++){
			numerosCuenta.add(DocUtils.readCell(EXCEL_CLIENTES_SENSIBLES, 1, i+1, 0))
		}
		return numerosCuenta
	}
}

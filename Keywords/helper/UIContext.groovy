package helper

import java.awt.Toolkit
import java.awt.datatransfer.Clipboard
import java.awt.datatransfer.StringSelection

import org.sikuli.script.Env
import org.sikuli.script.Key
import org.sikuli.script.KeyModifier
import org.sikuli.script.Pattern
import org.sikuli.script.Region
import org.sikuli.script.Screen as Screen

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.util.KeywordUtil

import internal.GlobalVariable
import junit.framework.Assert
import junit.framework.AssertionFailedError

public class UIContext extends ScreenDriver{

	Boolean check;
	Screen driver = getIntance()

	protected boolean checkThatIsVisible(String imgPath, String msg){
		return checkThatIsVisibleWithTime(imgPath,msg,GlobalVariable.MAXIMUM_WAIT)
	}

	protected void checkThatEquals(String text1, String text2, String msg){
		try{
			check = text1 == text2;
			KeywordUtil.logInfo(">>> " + msg + " - CHECKING FOR A MATCH: " + text1 + " == " + text2 + " - RESULT: " + Boolean.toString(check));
			Assert.assertTrue(check);
		}
		catch(AssertionFailedError e){
			KeywordUtil.markFailedAndStop("Error: " + msg + " NO MATCH")
		}
	}

	protected void checkThatLessThan(long num1, long num2, String msg){
		try{
			check = num1 < num2;
			KeywordUtil.logInfo(">>> " + msg + " - CHECKING LESS THAN: " + num1 + " < " + num2 + " - RESULT: " + Boolean.toString(check));
			Assert.assertTrue(check);
		}
		catch(AssertionFailedError e){
			KeywordUtil.markFailedAndStop("Error: " + msg + " GREATER THAN EXPECTED")
		}
	}

	protected boolean checkThatIsVisibleWithTime(String imgPath, String msg){
		checkThatIsVisibleWithTime(imgPath,msg,GlobalVariable.DEFAULT_WAIT)
	}

	protected boolean checkThatIsVisibleWithTime(String imgPath, String msg,int seconds){
		try{
			Boolean check = getIntance().exists(imgPath,seconds).onScreen
			KeywordUtil.logInfo(">>> " + msg + " - RESULT: " + Boolean.toString(check));
			Assert.assertTrue(check);
			return check
		}
		catch(Exception e){
			KeywordUtil.markFailedAndStop("Error: " + msg + " no encontrado")
		}
	}
	protected boolean checkExactPatternIsVisibleWithTime(String imgPath, String msg,int seconds){
		try{
			Pattern timeExactPattern = new Pattern (imgPath).similar(0.95)
			Boolean check = getIntance().exists(timeExactPattern,seconds).onScreen
			KeywordUtil.logInfo(">>> " + msg + " - RESULT: " + Boolean.toString(check));
			Assert.assertTrue(check);
			return check
		}
		catch(Exception e){
			KeywordUtil.markFailedAndStop("Error: " + msg + " no encontrado")
		}
	}
	protected boolean verifyElementIsVisible(String imgPath, String msg,int seconds){
		return verifyElementIsVisible(imgPath, seconds)
	}

	protected boolean verifyElementIsVisible(String imgPath){
		return verifyElementIsVisible(imgPath,GlobalVariable.DEFAULT_WAIT)
	}

	protected boolean verifyElementIsVisible(String imgPath,int seconds){
		return getIntance().exists(imgPath,seconds)!=null;
	}

	protected boolean verifyElementIsVisible(Pattern imgPath){
		return verifyElementIsVisible(imgPath,GlobalVariable.DEFAULT_WAIT)
	}

	protected boolean verifyElementIsVisible(Pattern imgPath,int seconds){
		return getIntance().exists(imgPath,seconds)!=null;
	}

	protected boolean verifyElementIsNotVisible(Pattern imgPath){
		return verifyElementIsNotVisible(imgPath,GlobalVariable.DEFAULT_WAIT)
	}

	protected boolean verifyElementIsNotVisible(Pattern imgPath,int seconds){
		return getIntance().waitVanish(imgPath,seconds);
	}

	@Keyword
	def killAllApps(){
		eliminarProceso('chrome')
		eliminarProceso('SVP_ENRE')
		eliminarProceso('OMSApplication')
		eliminarProceso('DsCallCenter')
		eliminarProceso('NavigatorApp')
		eliminarProceso('certa-workstation-core')
		eliminarProceso('4dl-workstation-svc')
		eliminarProceso('CalidadProductoNET')
		Thread.sleep(2000)
	}

	def eliminarProceso(String proceso) {
		Runtime.getRuntime().exec("taskkill /F /IM " + proceso + '.exe')
	}

	def tab(int times) {
		for (int i=0; i < times;  i++){
			driver.type(Key.TAB);
		}
	}

	protected void checkThatLessThan(int num1, int num2, String msg){
		try{
			check = num1 < num2;
			KeywordUtil.logInfo(">>> " + msg + " - CHECKING LESS THAN: " + num1 + " < " + num2 + " - RESULT: " + Boolean.toString(check));
			Assert.assertTrue(check);
		}
		catch(AssertionFailedError e){
			KeywordUtil.markFailedAndStop("Error: " + msg + " GREATER THAN EXPECTED")
		}
	}

	protected void checkThatNotEquals(String text1, String text2, String msg){
		try{
			check = text1 != text2;
			KeywordUtil.logInfo(">>> " + msg + " - CHECKING FOR NOT A MATCH: " + text1 + " == " + text2 + " - RESULT: " + Boolean.toString(check));
			Assert.assertTrue(check);
		}
		catch(AssertionFailedError e){
			KeywordUtil.markFailedAndStop("Error: " + msg + " MATCH")
		}
	}

	def clickElement(String element){
		clickElement(element,GlobalVariable.DEFAULT_WAIT)
	}

	def clickElement(String element, int wait){
		driver.wait(element,wait)
		driver.click(element)
	}

	def focusWindow(String imageWindow, String icon){
		for (int i = GlobalVariable.LOGINTRY;  i < GlobalVariable.MAXIMO_INTENTOS;i++){
			if(!verifyElementIsVisible(imageWindow,GlobalVariable.MINIMUM_WAIT)){
				minimizarVentanas()
				clickElement(icon)
				break
			}
		}
	}

	def takeScreenshot(String filename){
		new CustomScreenshot(driver, filename)
	}

	public String takeScreenshot(Region region, String filename){
		return new CustomScreenshot().capture(driver,region,filename)
	}


	def minimizarVentanas(){
		driver.type("d", KeyModifier.WIN)
	}

	def seleccionarDropDown(int index){
		for (int i = 0; i<index ;i++){
			driver.type(Key.DOWN)
		}
	}

	def backTab(int times) {
		for (int i=0; i < times;  i++){
			driver.type(Key.SHIFT + Key.TAB);
		}
	}

	def getClipboard(){
		return Env.getClipboard()
	}

	def seleccionDropDown(img, index){
		clickElement(img)
		driver.type(Key.HOME)
		for (int i = 0; i<index ;i++){
			driver.type(Key.DOWN)
		}
		enter(1)
	}

	def clearTypeDropDown(String image,String text){
		driver.doubleClick(image)
		driver.type(Key.DELETE)
		typeDropDown(image,text)
	}

	def typeDropDown(image,text){
		clickElement(image)
		driver.type(Key.HOME)
		driver.type(text)
		enter(1)
	}

	def enter(int times) {
		for (int i=0; i < times;  i++){
			driver.type(Key.ENTER);
		}
	}

	def end(int times) {
		for (int i=0; i < times;  i++){
			driver.type(Key.END);
		}
	}

	def down(int times) {
		for (int i=0; i < times;  i++){
			driver.type(Key.DOWN);
		}
	}

	public Region selectElementFromList(String region, int index){
		driver.wait(region)
		return selectElementFromList(driver.find(region),index)
	}
	
	public Region selectElementFromList(Region region, int index){
		for(int i = 0; i < index + 1; i++){
			region.setY(region.getY() + region.getH())
		}
		return region
	}

	public Region getRightRegion(String zone){
		driver.wait(zone,GlobalVariable.MINIMUM_WAIT)
		Region region = driver.find(zone)
		return getCustomRightRegion(zone,region.getW())
	}

	public Region getCustomRightRegion(String zone, int width){
		driver.wait(zone,GlobalVariable.MINIMUM_WAIT)
		Region region = driver.find(zone)
		region.setX(region.getX() + width)
		region.highlight(2,"blue")
		return region
	}

	public Region getNorthRegion(String zone){
		driver.wait(zone,GlobalVariable.MINIMUM_WAIT)
		Region region = driver.find(zone)
		region.setY(region.getY() - region.getH())
		return region
	}

	public String copiarSeleccion(){
		driver.type("c", Key.CTRL);
		return Env.getClipboard()
	}

	public void  verifyAndClick(path){
		if (verifyElementIsVisible(path,GlobalVariable.MINIMUM_WAIT))
			driver.click(path)
	}

	def up(int times) {
		for (int i=0; i < times;  i++){
			driver.type(Key.UP);
		}
	}

	@Keyword
	def getCount() {
		return GlobalVariable.COUNT
		KeywordUtil.logInfo(">>> GETTING COUNTER AT:" + GlobalVariable.COUNT);
	}

	@Keyword
	def updateCount() {
		GlobalVariable.COUNT = GlobalVariable.COUNT + 1
		KeywordUtil.logInfo(">>> UPDATING COUNT TO:" + GlobalVariable.COUNT);
	}

	@Keyword
	def resetCount() {
		GlobalVariable.COUNT = 0
		KeywordUtil.logInfo(">>> RESETING COUNT TO:" + GlobalVariable.COUNT);
	}

	def doubleClickElement(String element){
		doubleClickElement(element,GlobalVariable.DEFAULT_WAIT)
	}

	def doubleClickElement(String element, int wait){
		driver.wait(element,wait)
		driver.doubleClick(element)
	}
	
	
	def typeText(String text){
		if (Env.isLockOn(Key.C_CAPS_LOCK))
			driver.type(Key.CAPS_LOCK)
		driver.type(text)
	}
	
	def typeKey(String key){
		driver.type(key)
	}
	
	def clickRightRegion(String imageRegion){
		driver.click(getRightRegion(imageRegion))
	}
	
	def clickElement(Region element){
		clickElement(element,GlobalVariable.MINIMUM_WAIT)
	}
	
	def clickElement(Region element, int wait){
		driver.wait(element,wait)
		driver.click(element)
	}
	
	@Keyword
	def selectField(){
		if(Env.isLockOn(Key.C_NUM_LOCK))
			driver.type(Key.NUM_LOCK)
		driver.type(Key.END)
		driver.type(Key.HOME,KeyModifier.SHIFT)
	}
	
	def copy(){
		driver.type("c", Key.CTRL);
	}
	
	public Region getBottomRegion(Region region){
		region.setY(region.getY() + region.getH())
		return region
	}
	
	public Region getBottomRegion(String region){
		driver.wait(region)
		Region zone = driver.find(region)
		return getBottomRegion(zone)
	}

	public Region getBottomRegion(Region region, int customHeight){
		region.setY(region.getY() + region.getH())
		return region

	}
	
	public void clearClipboard(){
		StringSelection selection = new StringSelection("");
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(selection, selection)
	}
}

package helper

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import java.awt.*
import java.io.File
import javax.imageio.ImageIO
import internal.GlobalVariable
import org.sikuli.script.Screen
import java.time.LocalDateTime
import com.kms.katalon.core.configuration.RunConfiguration
import java.text.SimpleDateFormat
import java.util.Date
import org.sikuli.script.Region
import org.sikuli.script.KeyModifier
import org.sikuli.script.Key

public class ConfigHelper {
	private String timeStamp = new SimpleDateFormat("HHmmssSSS").format(new Date())
	private String completePath = RunConfiguration.getReportFolder() + "\\" + "Test Numero " + GlobalVariable.CONTADOR + "\\"
	private String completePath2 = RunConfiguration.getReportFolder() + "\\"

	def getFolderName() {
		return "\\" + timeStamp.toString() + "\\"
	}

	def getCompletePath(){
		return completePath
	}
	def getCompletePath2(){
		return completePath2
	}

	@Keyword
	def crearCarpetaTest(String path){
		new File(completePath).mkdirs();
	}

	@Keyword
	def incrementarContador(){
		GlobalVariable.CONTADOR++
	}
}
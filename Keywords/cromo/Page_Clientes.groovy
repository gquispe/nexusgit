package cromo

import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import helper.DocUtils

public class Page_Clientes extends Page_Base{

	private static final String EXCEL_PATH = "C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\DatosAnteriores.xlsx"

	private static final String IDS_CLIENTES_XPATH = "//*[@class='w2ui-marker']"
	private static final String IDS_CERRAR_XPATH = "//*[@id='clientes_window_header']/span"

	@Keyword
	def guardarClientes(){
		WebUI.delay(10)
		List<WebElement> ids_list = findElements(IDS_CLIENTES_XPATH)
		KeywordUtil.logInfo(">>> Clientes encontrados: " + ids_list.size())
		for(int i = 0; i < ids_list.size(); i++){
			DocUtils.writeOldCell(EXCEL_PATH, 1, i+1, 0, ids_list.get(i).getText())
			KeywordUtil.logInfo(">>> Cliente: " + ids_list.get(i).getText())
		}
	}

	@Keyword
	def guardarClientes(path, sheet){
		WebUI.delay(10)
		List<WebElement> ids_list = findElements(IDS_CLIENTES_XPATH)
		KeywordUtil.logInfo(">>> Clientes encontrados: " + ids_list.size())
		for(int i = 0; i < ids_list.size(); i++){
			DocUtils.writeCell(path, sheet, i+1, 0, ids_list.get(i).getText())
			KeywordUtil.logInfo(">>> Cliente: " + ids_list.get(i).getText())
		}
	}
	
	@Keyword
	def guardarClientesAfectados(path, sheet, column){
		WebUI.delay(10)
		List<WebElement> ids_list = findElements(IDS_CLIENTES_XPATH)
		KeywordUtil.logInfo(">>> Clientes encontrados: " + ids_list.size())
		for(int i = 0; i < ids_list.size(); i++){
			DocUtils.writeOldCell(path, sheet, i+1, column, ids_list.get(i).getText())
			KeywordUtil.logInfo(">>> Cliente: " + ids_list.get(i).getText())
		}
	}
	
	@Keyword
	def clickCerrarVentana(){
		WebElement cross = findElementXpath(IDS_CERRAR_XPATH)
		cross.click()
	}
}

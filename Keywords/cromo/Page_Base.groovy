package cromo

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import com.kms.katalon.core.webui.driver.DriverFactory


public class Page_Base extends helper.WebUIContext{

	protected WebDriver driver = DriverFactory.getWebDriver()

	Page_Base(){
		WebUI.switchToWindowIndex(0)
		WebUI.waitForPageLoad(GlobalVariable.DEFAULT_WAIT)
	}

	protected List<WebElement> findElements(String xpathElements){
		List<WebElement> elementList = driver.findElements(By.xpath(xpathElements));
		return elementList;
	}
	
	protected WebElement findElementXpath(String xpathElement){
		WebElement element = driver.findElement(By.xpath(xpathElement));
		return element;
	}

}

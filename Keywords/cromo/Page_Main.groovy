package cromo

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class Page_Main extends Page_Base{

	private TestObject labelUsuario       = findTestObject('Object Repository/Cromo/Page_Main/label_nombreUsuario')
	private TestObject mapa 			  = findTestObject('Object Repository/Cromo/Page_Main/mapa')
	private TestObject buttonHome 		  = findTestObject('Object Repository/Cromo/Page_Main/button_home')
	private TestObject areaActiva 		  = findTestObject('Object Repository/Cromo/Page_Main/area_activa')
	private TestObject aceptarSeleccion   = findTestObject('Object Repository/Cromo/Page_Main/button_aceptar')
	private TestObject inputBusqueda      = findTestObject('Object Repository/Cromo/Page_Main/input_buscar')
	private TestObject iconoBuscar        = findTestObject('Object Repository/Cromo/Page_Main/icon_buscar')
	private TestObject idSuministro       = findTestObject('Object Repository/Cromo/Page_Main/id_suministro')
	private TestObject verClientes 	      = findTestObject('Object Repository/Cromo/Page_Main/icon_verClientes')
	private TestObject areaDeConsecion    = findTestObject('Object Repository/Cromo/Page_Main/span_areaConcesion')
	private TestObject seleccionZona  	  = findTestObject('Object Repository/Cromo/Page_Main/seleccion_zona')
	private TestObject seleccionPartido	  = findTestObject('Object Repository/Cromo/Page_Main/seleccion_partido')
	private TestObject seleccionLocalidad = findTestObject('Object Repository/Cromo/Page_Main/seleccion_localidad')
	private TestObject buttonIrASeleccion = findTestObject('Object Repository/Cromo/Page_Main/button_irASeleccion')
	private TestObject popUpZoom = findTestObject('Object Repository/Cromo/Page_Main/popup_zoom')
	private TestObject dinamicElement = new TestObject()
	
	@Keyword
	def verificarNombreDeUsuarioVisible(){
		checkThatIsVisible(labelUsuario, "Nombre de usuario no visible")
	}

	@Keyword
	def verificarMapaVisible(){
		WebUI.delay(3)
		checkThatIsVisible(mapa, "Mapa no visible")
	}

	@Keyword
	def seleccionarAnomalia(String zona, String partido, String localidad, String idElemento){
		seleccionarElementoPorId(zona, partido, localidad, idElemento)
	}

	@Keyword
	def seleccionarAreaActiva(String zona, String partido, String localidad){
		clickAreaDeConsecion()
		ingresarZona(zona)
		ingresarPartido(partido)
		ingresarLocalidad(localidad)
		clickIrASeleccion()
	}

	@Keyword
	def clickAreaDeConsecion(){
		WebUI.click(areaDeConsecion)
	}

	@Keyword
	def ingresarZona(String zona){
		WebUI.sendKeys(seleccionZona, zona)
	}

	@Keyword
	def ingresarPartido(String partido){
		WebUI.sendKeys(seleccionPartido, partido)
	}

	@Keyword
	def ingresarLocalidad(String localidad){
		WebUI.sendKeys(seleccionLocalidad, localidad)
	}

	@Keyword
	def clickIrASeleccion(){
		WebUI.click(buttonIrASeleccion)
	}

	@Keyword
	def seleccionarElementoAfectado(String idLocalidad, String idSuministro){
		home()
		irALocalidad(idLocalidad)
		seleccionarSuministro(idSuministro)
		aceptarElemento()
	}

	@Keyword
	def home(){
		WebUI.switchToDefaultContent()
		if (GlobalVariable.ENVIRONMENT == "QA2"){
			WebUI.executeJavaScript("CROMO_MAP.onZoom(0)", null)
			WebUI.delay(5)
		}
	}

	@Keyword
	def irALocalidad(String idLocalidad){
		WebUI.executeJavaScript("fetch(" + idLocalidad + ",undefined,undefined,undefined)", null)
		WebUI.delay(5)
	}

	@Keyword
	def seleccionarSuministro(String suministro){
		if (GlobalVariable.ENVIRONMENT == "QA1"){
			WebUI.executeJavaScript("on_click(0," + suministro +")", null)
		}
		if (GlobalVariable.ENVIRONMENT == "QA2"){
			zoomSuministro(suministro)
			WebUI.delay(10)
			clickSuministro(suministro)
		}
	}

	@Keyword
	def zoomSuministro(String idSuministro){
		WebUI.executeJavaScript("CROMO_MAP.zoomObject(" + idSuministro + ")", null)
		//CROMO_MAP.zoomObject(6899033)
	}

	@Keyword
	def clickSuministro(String idSuministro){
		clickElemento(idSuministro)
	}

	@Keyword
	def clickElemento(String idElemento){
		WebUI.executeJavaScript("on_click(0," + idElemento +")", null)
		//on_click(0,6899033)
	}

	@Keyword
	def aceptarElemento(){
		WebUI.waitForElementClickable(aceptarSeleccion, 5)
		WebUI.click(aceptarSeleccion)
	}

	@Keyword
	def seleccionarCliente(String idCliente){
		WebUI.switchToWindowIndex(0)
		WebUI.switchToDefaultContent()
		buscarCliente(idCliente)
		WebUI.delay(5)
		clickSuministro(getIdSuministro())
		aceptarElemento()
	}

	@Keyword
	def buscarCliente(String idCliente){
		WebUI.setText(inputBusqueda, idCliente)
		WebUI.click(iconoBuscar)
	}

	@Keyword
	def getIdSuministro(){
		String textoIdSuministro = WebUI.getText(idSuministro)
		KeywordUtil.logInfo("Texto: " +textoIdSuministro)
		return textoIdSuministro
	}

	@Keyword
	def clickVerClientes(){
		WebUI.delay(3)
		WebUI.click(verClientes)
	}

	@Keyword
	def buscarSuministro(suministro){
		WebUI.setText(inputBusqueda, suministro)
		WebUI.click(iconoBuscar)
	}

	@Keyword
	def verificarpopUpZoom(){
//		checkThatIsVisibleMultiCheck(popUpZoom, 10, 1, "Zoom")
		WebUI.delay(7)
		checkThatIsVisible(popUpZoom, "Zoom")
	}
	
	
	@Keyword
	def seleccionarElementoPorId(String zona, String partido, String localidad, String idElemento){
		home()
		seleccionarAreaActiva(zona, partido, localidad)
		clickElemento(idElemento)
		aceptarElemento()
	}
	
	@Keyword
	def seleccionarCoordenadasElemento(String zona, String partido, String localidad, String idElemento){
		home()
		seleccionarAreaActiva(zona, partido, localidad)
		WebUI.delay(1)
		zoomSuministro(idElemento)
		WebUI.delay(1)
		clickDinamicElement(idElemento)
	}
	
	@Keyword
	def clickDinamicElement(String dynamicId){
	String xpath = '//*[@id="' + dynamicId + '"]'
	dinamicElement.addProperty("xpath", ConditionType.EQUALS, xpath)
	WebUI.click(dinamicElement)
	}
}

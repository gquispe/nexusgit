package cdp


import org.sikuli.script.App
import org.sikuli.script.KeyModifier
import org.sikuli.script.Screen

import com.kms.katalon.core.annotation.Keyword

import helper.ScreenDriver
import internal.GlobalVariable

public class CDP_Main extends helper.UIContext{

	protected static Screen screen = ScreenDriver.getIntance()

	private static final String TITULO_VENTANA = "Módulo de Calidad de Producto"
	protected static final String IMAGENES_CDP =  GlobalVariable.IMAGENES + "CDP\\"
	private static final String IMG_ICO_CDP = IMAGENES_CDP + "CDP_Barra.PNG"
	private static final String IMG_CDP_CRONOGRAMAS = IMAGENES_CDP + "CDP_Cronogramas.PNG"
	private App cdpApp = new App("C:\\4DataLink\\4DL-CalidadDeProducto\\CalidadProductoNET.exe")

	@Keyword
	def focus() {
		screen.type("d" + KeyModifier.WIN);
		screen.wait(IMG_ICO_CDP,300)
		App.focus(TITULO_VENTANA)
		screen.click(IMG_ICO_CDP)
		screen.wait(IMG_CDP_CRONOGRAMAS,300)
	}

	@Keyword
	def verificarInicioDeCDP(){
		checkThatIsVisible(IMG_CDP_CRONOGRAMAS, "Se abre una nueva pantalla 'Módulo de Calidad de Producto' sin mostrar mensajes de error.")
	}
}

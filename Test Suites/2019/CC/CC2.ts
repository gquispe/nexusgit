<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>CC2</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-07-25T11:24:44</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1f730570-dc3b-4377-9a39-d67940d7e2d6</testSuiteGuid>
   <testCaseLink>
      <guid>bccd540f-a833-4494-af80-17195e1f1878</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CC/CC2/CC2_01 Apertura OMS CC Cromo</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>ddec29a1-4c23-4a57-96b8-b06f276b8d2e</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Ambiente</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>bee5f2b6-df74-4f3b-8a41-bff85bf6004e</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/CC/CC2/CC2_UserData</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>bee5f2b6-df74-4f3b-8a41-bff85bf6004e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>usuario</value>
         <variableId>2b8f119c-3fd6-4d3e-9fed-04ce8f75f60c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>bee5f2b6-df74-4f3b-8a41-bff85bf6004e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>36ead10c-9388-41cb-9cf2-69d1b12b2ac4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ddec29a1-4c23-4a57-96b8-b06f276b8d2e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>ambiente</value>
         <variableId>55b0083e-ce7a-429e-9be7-eaad07faf621</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>413b79e4-4b48-49ce-8312-4c2f28877123</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CC/CC2/CC2_02 Generar un documento forzado BT</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>b645d85a-1634-4050-89d7-cde60b381126</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/CC/CC2/CC2_UserData</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>3b6ffc31-0e7f-4f8c-9d81-eec3be073ec0</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Ambiente</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>b645d85a-1634-4050-89d7-cde60b381126</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>usuario</value>
         <variableId>f9ad84d2-5708-48fa-a161-e0fd6fa78fd5</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>b645d85a-1634-4050-89d7-cde60b381126</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>a99f6e04-13c8-4ed9-af92-ff846bc79fa1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>3b6ffc31-0e7f-4f8c-9d81-eec3be073ec0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>ambiente</value>
         <variableId>c23f7597-cb01-4e19-acb6-8fbf5ec4757c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>b645d85a-1634-4050-89d7-cde60b381126</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>coordenadas</value>
         <variableId>a34aa77b-4ed1-4849-b914-460c8c7705d9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>b645d85a-1634-4050-89d7-cde60b381126</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>minutos</value>
         <variableId>ed88f088-8980-4298-ad63-998a342a9965</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>b645d85a-1634-4050-89d7-cde60b381126</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>tipo_documento</value>
         <variableId>aa2cdc74-47d5-40bd-a79f-f570993ac69b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>b645d85a-1634-4050-89d7-cde60b381126</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>tipo_afectacion</value>
         <variableId>b09cbabe-a748-4fff-9358-4aaff041b1b4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>b645d85a-1634-4050-89d7-cde60b381126</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>id_localidad</value>
         <variableId>d8657a44-0ff9-4d84-9d39-3d4f73529a58</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>b645d85a-1634-4050-89d7-cde60b381126</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>id_suministro</value>
         <variableId>47568c38-b005-4697-95a7-956a229e7ed5</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>b645d85a-1634-4050-89d7-cde60b381126</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>equipo_movil</value>
         <variableId>5f385ccc-d69b-4061-b755-cfb31af8145f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>b645d85a-1634-4050-89d7-cde60b381126</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>horas_restauracion</value>
         <variableId>81cb3a9b-e010-4879-a2e0-4544f1f73c85</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b793a3d9-4c54-4281-b9df-5c0dadc9ddeb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CC/CC2/CC2_03 Realizar Una Afectacion</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>b035d03a-4a74-448b-bae5-84cc6b3c3559</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/CC/CC2/CC2_UserData</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>b035d03a-4a74-448b-bae5-84cc6b3c3559</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>tipo_afectacion</value>
         <variableId>2c8eb629-297f-4e38-bfd0-b60ee2380d35</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>b035d03a-4a74-448b-bae5-84cc6b3c3559</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>id_localidad</value>
         <variableId>1c97b940-c8b7-4b6e-a5c0-de3beb6bc5fe</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>b035d03a-4a74-448b-bae5-84cc6b3c3559</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>id_suministro</value>
         <variableId>2b8886bf-e320-4cc0-87b1-dc8c773afbdc</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>83e85c5f-a290-4ec3-9431-19898b0f8347</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CC/CC2/CC2_04 Call Center del icono Operaciones</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a6ac67fe-97b6-445e-856d-2c6b90d78544</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CC/CC2/CC2_05 Generar reclamos y asociarlos</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>2bf63844-6185-40a9-87dd-58307c758a7c</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/CC/CC2/CC2_ClientesAfectados</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>33d29636-5c27-456d-a46e-884e5b64c8ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CC/CC2/CC2_06 Call Center Alta de Reclamo Gestion Programable</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>6693e62c-6bf5-43b3-93d5-92ce41b348c2</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/CC/CC2/CC2_ClientesAfectados</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f4cf4f85-8fca-405c-9b9e-5b327c42c8a1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CC/CC2/CC2_07 Verificar que el documento generado</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3a361a3a-951e-4d6b-8a53-c4e95138df31</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CC/CC2/CC2_08 Generar el despacho de un movil a WOM</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e9af55fe-9103-445e-9310-aad55b5c2d84</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CC/CC2/CC2_09 CC2_10 Call Center y crear reclamo de gestion programable</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>5295cb8e-6d2e-4b5d-a0f9-71f55249deb5</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/CC/CC2/CC2_ClientesAfectados</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>0598a6a1-10d7-4f52-8fbf-da07e53b77c5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CC/CC2/CC2_11 Generar Anomalia</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>415ab903-cf22-4f36-9ac4-7bd48dde397b</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/CC/CC2/CC2_UserData</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>2cebadaa-2d00-4c19-8070-a2ec0e0b3079</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/CC/CC2/CCS_Documentos</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>415ab903-cf22-4f36-9ac4-7bd48dde397b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>elemento</value>
         <variableId>3ec57c42-8615-412a-9633-547842b3937e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>2cebadaa-2d00-4c19-8070-a2ec0e0b3079</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>documento</value>
         <variableId>e3775cf8-3bb7-4079-b328-f228ddea1136</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>415ab903-cf22-4f36-9ac4-7bd48dde397b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>id_localidad</value>
         <variableId>8699c9b0-5d55-4cd6-9e13-77a2d19a891d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>415ab903-cf22-4f36-9ac4-7bd48dde397b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>id_suministro</value>
         <variableId>35efd9f1-a4c6-4098-877f-c1088e382f58</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a8a7b9e8-2feb-4cea-82ee-a91cf704aac0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CC/CC2/CC2_12 Anular Anomalia</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>eddbe87e-c2ba-480f-bbff-c5c20815bcac</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/CC/CC2/CCS_Documentos</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>eddbe87e-c2ba-480f-bbff-c5c20815bcac</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>documento</value>
         <variableId>a823904c-0e9c-47a8-a7f3-01519738a469</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>59006db4-4efb-4511-9995-de8b1cc8004f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CC/CC2/CC2_13 Despachar un equipo movil</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>6c1ddcb4-7948-4be2-b055-475437982d1d</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/CC/CC2/CCS_Documentos</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>6c1ddcb4-7948-4be2-b055-475437982d1d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>documento</value>
         <variableId>51415da4-458e-48ec-a06d-d67081693781</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>506c6489-876e-46a5-9833-5fb4d7366b32</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CC/CC2/CC2_14 Restaurar afectaciones</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>02b8ee64-100d-4815-b11d-ab23b3c2bde1</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/CC/CC2/CCS_Documentos</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>46291207-0d34-471b-bd36-12898fc5707c</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/CC/CC2/CC2_UserData</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>02b8ee64-100d-4815-b11d-ab23b3c2bde1</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>documento</value>
         <variableId>55a989ba-a746-4df2-af3d-7f22d30bc7be</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>46291207-0d34-471b-bd36-12898fc5707c</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>tipo_afectacion</value>
         <variableId>a23ecfa5-0531-4fe2-903f-988e494092d6</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>46291207-0d34-471b-bd36-12898fc5707c</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>id_localidad</value>
         <variableId>7624bb0e-bb06-48a4-9e85-4d99263c0148</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>46291207-0d34-471b-bd36-12898fc5707c</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>id_suministro</value>
         <variableId>30acef80-c844-4ad4-81f2-acee6e8c191b</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>cccdddc0-337b-49a9-9051-8b73c8f20e8b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CC/CC2/CC2_15 SETUPCerrar Reclamos</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6fa49fa1-7ace-4aac-9297-96354446206f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CC/CC2/CC2_15 Cierre de documento</testCaseId>
   </testCaseLink>
</TestSuiteEntity>

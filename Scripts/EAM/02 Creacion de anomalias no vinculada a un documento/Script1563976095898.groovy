import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'nexus.Main.inicioCromo'(usuario, password, ambiente)

CustomKeywords.'nexus.Main.inicioWorkstation'(usuario, password, ambiente)

CustomKeywords.'nexus.Main.abrirOMS'()

CustomKeywords.'oms.Panel_De_Navegacion.seleccionarAnomalias'()

CustomKeywords.'oms.Panel_De_Navegacion.clickAnomaliasDetectadas'()

CustomKeywords.'oms.Panel_AnomaliasDetectadas.clickNuevaAnomalia'()

CustomKeywords.'oms.Window_EditarAnomalia.seleccionarTipoDered'(2)

CustomKeywords.'oms.Window_EditarAnomalia.seleccionarAreaOperativa'(2)

CustomKeywords.'oms.Window_EditarAnomalia.seleccionarPartido'(2)

CustomKeywords.'oms.Window_EditarAnomalia.seleccionarLocalidad'(2)

CustomKeywords.'oms.Window_EditarAnomalia.seleccionarCalle'(2)

CustomKeywords.'oms.Window_EditarAnomalia.seleccionarEntreCalle1'(2)

CustomKeywords.'oms.Window_EditarAnomalia.clickBotonVerde'()

CustomKeywords.'cromo.Page_Main.seleccionarElementoAfectado'(localidad, suministro)

CustomKeywords.'oms.Window_EditarAnomalia.seleccionarClasificacion'(2)

CustomKeywords.'oms.Window_EditarAnomalia.seleccionarinstalacion'(1)

CustomKeywords.'oms.Window_EditarAnomalia.seleccionarCausa'(1)

CustomKeywords.'oms.Window_EditarAnomalia.seleccionarPrioridad'(1)

CustomKeywords.'oms.Window_EditarAnomalia.seleccionarTipoDeAnomalia'(1)

CustomKeywords.'oms.PopUp_DocumentoCreado.verificarAnomaliaCreada'()
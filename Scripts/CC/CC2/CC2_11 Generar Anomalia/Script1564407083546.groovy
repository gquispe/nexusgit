import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'oms.Main.focus'()

CustomKeywords.'oms.Main.buscarDocumentoGuardado'("C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\CC2.xlsx", 1, 1, 0)

CustomKeywords.'oms.Window_Documento.clickAnomalias'()

CustomKeywords.'oms.Window_Documento_Tab_Anomalias.clickNuevaAnomalia'()

CustomKeywords.'oms.Window_EditarAnomalia.clickBotonVerde'()

CustomKeywords.'cromo.Page_Main.seleccionarElementoAfectado'(id_localidad, id_suministro)

CustomKeywords.'oms.Window_EditarAnomalia.seleccionarClasificacion'(2)

CustomKeywords.'oms.Window_EditarAnomalia.seleccionarinstalacion'(1)

CustomKeywords.'oms.Window_EditarAnomalia.seleccionarCausa'(1)

CustomKeywords.'oms.Window_EditarAnomalia.seleccionarPrioridad'(1)

CustomKeywords.'oms.Window_EditarAnomalia.seleccionarTipoDeAnomalia'(1)

CustomKeywords.'oms.Window_EditarAnomalia.seleccionarCalle'(1)

CustomKeywords.'oms.Window_EditarAnomalia.seleccionarEntreCalle1'(1)

CustomKeywords.'oms.PopUp_DocumentoCreado.clickCerrar'()

CustomKeywords.'oms.Window_Documento.cerrarVentanaForzado'()
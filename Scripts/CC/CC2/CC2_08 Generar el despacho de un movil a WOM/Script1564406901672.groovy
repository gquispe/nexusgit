import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'oms.Main.focus'()

CustomKeywords.'oms.Window_Documento.focusOnDocument'()

CustomKeywords.'oms.Window_Documento.cerrarVentanaForzado'()

CustomKeywords.'oms.Main.buscarDocumentoGuardado'("C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\CC2.xlsx", 1, 1, 0)

//CustomKeywords.'oms.Window_Documento.focusOnDocument'()

CustomKeywords.'oms.Window_Documento.clickTomar'()

CustomKeywords.'oms.Window_Documento.clickDespachoMovil'()

CustomKeywords.'oms.Window_SeleccioneUnEquipoMovil.clicklista'()

CustomKeywords.'oms.SeleccioneUnEquipoMovil.seleccionarElementosMostrados'(4)

CustomKeywords.'oms.SeleccioneUnEquipoMovil.seleccionarEquipoMovilClick'()

CustomKeywords.'oms.Window_SeleccioneUnEquipoMovil.aceptarSeleccion'()

CustomKeywords.'oms.Window_Despacho.clickConfirmarDespacho'()

CustomKeywords.'oms.Window_Documento.tabAuditoria'()

CustomKeywords.'oms.Window_Documento_Tab_Auditoria.verificarDespacho'()

CustomKeywords.'oms.Window_Documento.cerrarVentanaForzado'()
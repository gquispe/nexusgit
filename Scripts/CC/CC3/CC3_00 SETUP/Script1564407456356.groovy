import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'helper.WebUIContext.startCromo'(ambiente)

//CustomKeywords.'cromo.Page_Login.login'(usuario, password)
//
//CustomKeywords.'cromo.Page_Main.buscarSuministro'(suministro)
//
//CustomKeywords.'cromo.Page_Main.clickVerClientes'()
//
//CustomKeywords.'cromo.Page_Clientes.guardarClientesAfectados'("C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\CC3.xlsx", 1, 0)
//
//CustomKeywords.'cromo.Page_Clientes.clickCerrarVentana'()

CustomKeywords.'nexus.Main.inicioWorkstation'(usuario, password, ambiente)

CustomKeywords.'nexus.Main.abrirCallCenter'()

CustomKeywords.'nexus.Main.abrirOMS'()

CustomKeywords.'oms.Main.clickMenuNuevoDocumento'()

CustomKeywords.'oms.Window_NuevoDocumento.generarNuevoDocumento'("Programado BT", "0", "X: 5634026,2196 Y: 6176005,2868")

CustomKeywords.'oms.PopUp_DocumentoCreado.clickAbrir'()

CustomKeywords.'oms.Window_Documento.guardarDocumento'("C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\CC3.xlsx", 1, 2, 2)

CustomKeywords.'oms.Window_Documento.clickCerrarVentana'()


//focus
CustomKeywords.'cromo.Page_Login.login'(usuario, password)

CustomKeywords.'cromo.Page_Main.buscarSuministro'(suministro)

CustomKeywords.'cromo.Page_Main.clickVerClientes'()

CustomKeywords.'cromo.Page_Clientes.guardarClientesAfectados'("C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\CC3.xlsx", 1, 0)

CustomKeywords.'cromo.Page_Clientes.clickCerrarVentana'()


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'oms.Main.focus'()

CustomKeywords.'oms.Main.buscarDocumentoGuardado'("C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\CC3.xlsx", 2, 1, 3)

CustomKeywords.'oms.Window_Documento.clickReclamos'()

CustomKeywords.'oms.Window_Documento_Tab_Reclamos.clickMoverReclamos'()

CustomKeywords.'oms.Window_ApliqueUnFiltroYSeleccionUnDocumento.buscarDocumento'("C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\CC3.xlsx", 1, 2, 2)

CustomKeywords.'oms.Window_ApliqueUnFiltroYSeleccionUnDocumento.clickAceptar'()

CustomKeywords.'oms.Window_Documento.verificarDocumentoCancelado'()

CustomKeywords.'oms.Window_Documento.cerrarVentanaForzado'()
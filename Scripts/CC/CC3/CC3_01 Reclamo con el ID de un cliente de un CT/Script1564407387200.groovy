import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'callCenter.Page_Administracion_De_Reclamos.focus'()

CustomKeywords.'callCenter.Page_Administracion_De_Reclamos.ingresarReclamo'("s", "C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\CC3.xlsx", 1, 1, 0)

CustomKeywords.'callCenter.Window_ReclamosActivosDelCliente.clickNuevo1'()

CustomKeywords.'callCenter.Window_DocumentoAlQueElReclamoPuedeSerAsociado.clickAsociar'()

CustomKeywords.'callCenter.Windows_AfeccionesRecientesDelCliente.clickNuevoReclamo'()

CustomKeywords.'callCenter.Window_ReclamosActivosDelCliente.clickNuevo2'()

CustomKeywords.'callCenter.Window_Reclamos.clickAceptar'()

CustomKeywords.'callCenter.Window_OperacionExitosa.guardarCodigoDeReclamoOverwrite'("C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\CC3.xlsx", 1, 1, 1)

CustomKeywords.'callCenter.Window_OperacionExitosa.aceptarOperacion'()
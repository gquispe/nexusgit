import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'nexus.CircuitoCritico1.paso1AbrirAplicacionesNexus'(usuario, password, ambiente)

CustomKeywords.'nexus.CircuitoCritico1.paso2CreacionDocumentoProgramadoBT'(tipo_documento, minutos, coordenadas)

CustomKeywords.'nexus.CircuitoCritico1.paso3AfectacionDocumentoProgramadoBT'(tipo_afectacion, id_localidad, id_suministro)

CustomKeywords.'nexus.CircuitoCritico1.paso4DespachoMovilDocumentoProgramado'(equipo_movil, horas_restauracion)

CustomKeywords.'nexus.CircuitoCritico1.paso5AltaDeReclamosAsociarADocProgBT'()

CustomKeywords.'nexus.CircuitoCritico1.paso6VerificacionDeDocumentosSituacionMonitor'()

//CustomKeywords.'nexus.CircuitoCritico1.paso7LlamadasSalientesSituacionMonitor'(ambiente)

CustomKeywords.'nexus.CircuitoCritico1.paso8RestauracionDocumentoProgramadoBT'(usuario, password, ambiente, id_suministro)

CustomKeywords.'nexus.CircuitoCritico1.paso9VerificarClientesRestaurados'()

CustomKeywords.'nexus.CircuitoCritico1.paso10CerrarReclamosDocProgramadoBT'()

CustomKeywords.'nexus.CircuitoCritico1.paso11ManiobraAperturaDocumento'(usuario, password, ambiente)

CustomKeywords.'nexus.CircuitoCritico1.paso12DarDeAltaReclamosClienteSensibles'()

CustomKeywords.'nexus.CircuitoCritico1.paso13ManiobraCierreDocumento'()

CustomKeywords.'nexus.CircuitoCritico1.paso14CierreProvisorioDeDocumento'()

CustomKeywords.'nexus.CircuitoCritico1.paso15CambioTipoRegistracion'(ambiente)

CustomKeywords.'nexus.CircuitoCritico1.paso16CierreDeDocumentoPostOperacion'()

CustomKeywords.'nexus.CircuitoCritico1.resultadoEsperado'()
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'workstation.Configuration.verifyConfigurationFile'(ambiente)


CustomKeywords.'workstation.Workstation.startWorkstationCore'()

CustomKeywords.'workstation.Workstation.login'(usuario, password)

CustomKeywords.'workstation.Workstation.openOMS'()

CustomKeywords.'oms.Main.focus'()

CustomKeywords.'oms.Window_Regiones.guardarRegionesSeleccionadas'()

CustomKeywords.'oms.Main.buscarDocumento'(documento)

CustomKeywords.'oms.Window_Documento.clickActividades'()

CustomKeywords.'oms.Window_Documento.clickNuevoTrabajo'()

CustomKeywords.'oms.Window_AgregarActividad.ingresarCodigo'()

CustomKeywords.'oms.Window_AgregarActividad.aceptarActividad'()

CustomKeywords.'oms.Window_Documento.clickEncabezado'()

CustomKeywords.'oms.Window_Documento.clickCerrar'()

CustomKeywords.'oms.Window_CerrarDocumento.seleccionarInstalacion'()

CustomKeywords.'oms.Window_CerrarDocumento.seleccionarCausa'()

CustomKeywords.'oms.Window_CerrarDocumento.clickAceptar'()

CustomKeywords.'oms.Window_Documento.verificarCierrePostoperacion'()
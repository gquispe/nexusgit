import com.kms.katalon.core.util.KeywordUtil

KeywordUtil.logInfo("Test case :" + testcaseid)

CustomKeywords.'nexus.Main.inicioWorkstation'(usuario, password, ambiente)
CustomKeywords.'workstation.Workstation.openOMS'()
CustomKeywords.'workstation.Workstation.inicioCalculoTiempoApertura'()
CustomKeywords.'oms.Main.focus'()
CustomKeywords.'oms.Window_Regiones.verificarTiempoDeInicioOMS'() 
CustomKeywords.'oms.Window_Regiones.guardarRegionesSeleccionadas'()
CustomKeywords.'oms.Main.verificarInicioDeOMS'()
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'nexus.Main.inicioWorkstation'(usuario, password, ambiente)

CustomKeywords.'nexus.Main.abrirOMS'()

CustomKeywords.'oms.Main.buscarDocumento'(numero_documento)

CustomKeywords.'oms.Window_Documento.clickDespachoMovil'()

CustomKeywords.'oms.Window_SeleccioneUnEquipoMovil.clicklista'()

CustomKeywords.'oms.SeleccioneUnEquipoMovil.seleccionarElementoPorIndex'(index_equipo_movil)

CustomKeywords.'oms.Window_SeleccioneUnEquipoMovil.aceptarSeleccion'()

CustomKeywords.'oms.Window_Despacho.clickConfirmarDespacho'()

CustomKeywords.'oms.Window_Documento.actualizarRestauracion'(horas_restauracion)
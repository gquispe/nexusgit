import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil


KeywordUtil.logInfo("======================= Iteracion " + iteracion + " =======================")

CustomKeywords.'oms.Panel_De_Navegacion.clickHeaderPanelDeNavegacion'()
CustomKeywords.'oms.Main.checkCodigoReclamo'(codigo_reclamo)
CustomKeywords.'oms.Panel_De_Navegacion.inputReclamoId'(codigo_reclamo)
CustomKeywords.'oms.Window_DetalleDeReclamo.abrirDocumento'()
CustomKeywords.'oms.Window_Documento_Header.checkTipoForzado'(tipo_forzado,iteracion)
CustomKeywords.'oms.Window_Documento.cerrarVentana'()
CustomKeywords.'oms.Window_DetalleDeReclamo.cerrarVentana'()
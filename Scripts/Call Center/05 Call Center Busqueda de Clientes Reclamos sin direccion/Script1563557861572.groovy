import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'nexus.Main.inicioCallCenter'(usuario, password, ambiente)

CustomKeywords.'callCenter.Page_Administracion_De_Reclamos.focus'()

CustomKeywords.'callCenter.Page_Administracion_De_Reclamos.seleccionarTipoReclamo'(2)

CustomKeywords.'callCenter.Page_Administracion_De_Reclamos.seleccionarAreasOperativas'(1)

CustomKeywords.'callCenter.Page_Administracion_De_Reclamos.seleccionarPartidos'(1)

CustomKeywords.'callCenter.Page_Administracion_De_Reclamos.seleccionarLocalidades'(1)

CustomKeywords.'callCenter.Page_Administracion_De_Reclamos.clickCallesPuntos'()

CustomKeywords.'callCenter.Page_Administracion_De_Reclamos.seleccionarCalle'(7)

CustomKeywords.'callCenter.Page_Administracion_De_Reclamos.seleccionarNumero'("2650")

CustomKeywords.'callCenter.Page_Administracion_De_Reclamos.seleccionarBusqueda'(3)

CustomKeywords.'callCenter.Page_Administracion_De_Reclamos.buscarReclamo'()

CustomKeywords.'callCenter.Window_resultadosDeLaBusqueda.clickAceptar'()

CustomKeywords.'callCenter.Window_ReclamosActivosDelCliente.clickNuevo1'()

CustomKeywords.'callCenter.Window_DocumentoAlQueElReclamoPuedeSerAsociado.clickAsociar'()

CustomKeywords.'callCenter.Window_Reclamos.clickAceptar'()

CustomKeywords.'callCenter.Window_OperacionExitosa.verificarOperacionExitosa'()

CustomKeywords.'callCenter.Window_OperacionExitosa.aceptarOperacion'()


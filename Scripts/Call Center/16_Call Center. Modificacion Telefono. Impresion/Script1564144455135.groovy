/**
 * Prueba agregar un telefono e imprimirlo
 */

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'nexus.Main.inicioCallCenter'(usuario, password, ambiente)

CustomKeywords.'callCenter.Page_Administracion_De_Reclamos.ingresarReclamo'(tipo_reclamo, num_cta)

CustomKeywords.'callCenter.Window_ReclamosActivosDelCliente.clickNuevo1'()

CustomKeywords.'callCenter.Window_ReclamosActivosDelCliente.clickDetaDatCliente'()

// Desarrollar la nueva funcionalidad. Agregar Telefonos ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
CustomKeywords.'callCenter.Window_ReclamosActivosDelCliente.clickOtrosTelef'()

CustomKeywords.'callCenter.Window_ReclamosActivosDelCliente.clickNuevoTelef'()

// Tipo Celular
CustomKeywords.'callCenter.Window_ReclamosActivosDelCliente.seleccionarTipoTel'(1)
// Ingresar Telefonos
String nroTelefono = CustomKeywords.'callCenter.Window_ReclamosActivosDelCliente.randomNumber'(11000000, 88999999)
CustomKeywords.'callCenter.Window_ReclamosActivosDelCliente.ingresarTelefono'(nroTelefono)
// Tipo Contacto
CustomKeywords.'callCenter.Window_ReclamosActivosDelCliente.clickCheckCtoTel'()

CustomKeywords.'callCenter.Window_ReclamosActivosDelCliente.clickTelAcepta'()
CustomKeywords.'callCenter.Window_ReclamosActivosDelCliente.clickTelAceptaMsg'()

CustomKeywords.'oms.Window_Documento.cerrarVentanaForzado'()

// Ejecuta impresion -------------------------------------------------------------------
CustomKeywords.'callCenter.Window_ReclamosActivosDelCliente.clickImprimir'()

// Manejo de error - Eliminar cuando este resuelto
CustomKeywords.'callCenter.Window_ReclamosActivosDelCliente.clickTelAceptarError'()
CustomKeywords.'callCenter.Window_ReclamosActivosDelCliente.clickTelAceptarError'()

CustomKeywords.'callCenter.Window_ReclamosActivosDelCliente.clickTelCerrarPreview'()

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'nexus.Main.inicioCromo'(usuario, password, ambiente)

CustomKeywords.'nexus.Main.inicioWorkstation'(usuario, password, ambiente)

CustomKeywords.'nexus.Main.abrirCallCenter'()

CustomKeywords.'nexus.Main.abrirOMS'()

CustomKeywords.'oms.Main.focus'()

CustomKeywords.'oms.Main.clickMenuNuevoDocumento'()

CustomKeywords.'oms.Window_NuevoDocumento.generarNuevoDocumento'("Forzado BT", "30", coordenadas)

CustomKeywords.'oms.PopUp_DocumentoCreado.clickAbrir'()

CustomKeywords.'oms.Window_Documento.guardarDocumento'("C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\Call Center Smoke.xlsx", 4, GlobalVariable.COUNT + 1, 3)

CustomKeywords.'oms.ProcesoAfectacionRestauracion.inicioAfectacion'("Topologica")

CustomKeywords.'cromo.Page_Main.seleccionarElementoAfectado'("369", suministro)

CustomKeywords.'oms.ProcesoAfectacionRestauracion.finalizarAfectacion'()

CustomKeywords.'oms.Window_Documento.cerrarVentanaForzado'()

CustomKeywords.'helper.UIContext.resetCount'()
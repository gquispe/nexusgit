import com.kms.katalon.core.util.KeywordUtil

import internal.GlobalVariable as GlobalVariable

CustomKeywords.'nexus.Main.inicioCromo'(usuario, password, ambiente)

CustomKeywords.'nexus.Main.inicioWorkstation'(usuario, password, ambiente)

CustomKeywords.'nexus.Main.abrirCallCenter'()

CustomKeywords.'nexus.Main.abrirOMS'()

CustomKeywords.'oms.Main.clickMenuNuevoDocumento'()

CustomKeywords.'oms.Window_NuevoDocumento.generarNuevoDocumento'("Forzado BT", minutos, coordenadas)

CustomKeywords.'oms.PopUp_DocumentoCreado.clickAbrir'()

CustomKeywords.'oms.Window_Documento.guardarDocumento'("C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\WOM.xlsx", 0, 1, 8)

CustomKeywords.'oms.ProcesoAfectacionRestauracion.inicioAfectacion'(tipo_afectacion)

CustomKeywords.'cromo.Page_Main.seleccionarElementoAfectado'(id_localidad, id_suministro)

CustomKeywords.'cromo.Page_Main.clickVerClientes'()

CustomKeywords.'cromo.Page_Clientes.guardarClientesAfectados'("C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\WOM.xlsx", 1, 0)

CustomKeywords.'oms.ProcesoAfectacionRestauracion.finalizarAfectacion'()

CustomKeywords.'oms.Window_Documento.cerrarVentanaForzado'()

CustomKeywords.'callCenter.Page_Administracion_De_Reclamos.focus'()

CustomKeywords.'callCenter.Page_Administracion_De_Reclamos.ingresarReclamo'("s", "C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\WOM.xlsx", 1, 1, 0)

CustomKeywords.'callCenter.Window_DocumentoAlQueElReclamoPuedeSerAsociado.clickAsociar'()

CustomKeywords.'callCenter.Window_ReclamosActivosDelCliente.clickNuevo1'()

CustomKeywords.'callCenter.Window_DocumentoAlQueElReclamoPuedeSerAsociado.clickAsociar'()

CustomKeywords.'callCenter.Window_ReclamosActivosDelCliente.clickNuevo2'()

CustomKeywords.'callCenter.Window_Reclamos.clickAceptar'()

CustomKeywords.'callCenter.Window_OperacionExitosa.guardarCodigoDeReclamoOverwrite'("C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\WOM.xlsx", 0, 1, 9)

CustomKeywords.'callCenter.Window_OperacionExitosa.aceptarOperacion'()

CustomKeywords.'oms.Main.focus'()

CustomKeywords.'oms.Main.buscarDocumentoGuardado'("C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\WOM.xlsx", 0, 1, 8)

CustomKeywords.'oms.Window_Documento.clickTomar'()

CustomKeywords.'oms.Window_Documento.clickDespachoMovil'()

CustomKeywords.'oms.Window_SeleccioneUnEquipoMovil.clicklista'()

CustomKeywords.'oms.SeleccioneUnEquipoMovil.seleccionarElementosMostrados'(4)

CustomKeywords.'oms.SeleccioneUnEquipoMovil.seleccionarEquipoMovilClick'()

CustomKeywords.'oms.Window_SeleccioneUnEquipoMovil.aceptarSeleccion'()

CustomKeywords.'oms.Window_Despacho.clickConfirmarDespacho'()

CustomKeywords.'oms.Window_Documento.tabAuditoria'()

CustomKeywords.'oms.Window_Documento_Tab_Auditoria.verificarDespacho'()

CustomKeywords.'oms.Window_Documento.cerrarVentanaForzado'()


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'nexus.Main.inicioCromo'(usuario, password, ambiente)

CustomKeywords.'nexus.Main.inicioWorkstation'(usuario, password, ambiente)

CustomKeywords.'nexus.Main.abrirCallCenter'()

CustomKeywords.'nexus.Main.abrirOMS'()

CustomKeywords.'oms.Main.clickMenuNuevoDocumento'()

CustomKeywords.'oms.Window_NuevoDocumento.generarNuevoDocumento'("Programado BT", minutos, coordenadas)

CustomKeywords.'oms.PopUp_DocumentoCreado.clickAbrir'()

CustomKeywords.'oms.Window_Documento.guardarDocumento'("C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\WOM.xlsx", 0, 4, 8)

CustomKeywords.'oms.ProcesoAfectacionRestauracion.inicioAfectacion'(tipo_afectacion)

CustomKeywords.'cromo.Page_Main.seleccionarElementoAfectado'(id_localidad, id_suministro)

CustomKeywords.'cromo.Page_Main.clickVerClientes'()

CustomKeywords.'cromo.Page_Clientes.guardarClientesAfectados'("C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\WOM.xlsx", 1, 3)

CustomKeywords.'oms.ProcesoAfectacionRestauracion.finalizarAfectacion'()

CustomKeywords.'oms.Window_Documento.cerrarVentanaForzado'()

CustomKeywords.'callCenter.Page_Administracion_De_Reclamos.focus'()

CustomKeywords.'callCenter.Page_Administracion_De_Reclamos.ingresarReclamo'("s", "C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\WOM.xlsx", 1, 1, 3)

CustomKeywords.'callCenter.Window_DocumentoAlQueElReclamoPuedeSerAsociado.clickAsociar'()

CustomKeywords.'callCenter.Window_ReclamosActivosDelCliente.clickNuevo1'()

CustomKeywords.'callCenter.Window_DocumentoAlQueElReclamoPuedeSerAsociado.clickAsociar'()

CustomKeywords.'callCenter.Window_ReclamosActivosDelCliente.clickNuevo2'()

CustomKeywords.'callCenter.Window_Reclamos.clickAceptar'()

CustomKeywords.'callCenter.Window_OperacionExitosa.guardarCodigoDeReclamoOverwrite'("C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\WOM.xlsx", 0, 4, 9)

CustomKeywords.'callCenter.Window_OperacionExitosa.aceptarOperacion'()

CustomKeywords.'oms.Main.focus'()

CustomKeywords.'oms.Main.buscarDocumentoGuardado'("C:\\Automatizaciones\\Scripts\\nexus\\Datos de Entrada\\WOM.xlsx", 0, 4, 8)

CustomKeywords.'oms.Window_Documento.clickEquiposMoviles'()

CustomKeywords.'oms.Window_Documento_Tab_EquiposMoviles.clickDespachar'()

CustomKeywords.'oms.Window_SeleccioneUnEquipoMovil.clicklista'()

CustomKeywords.'oms.SeleccioneUnEquipoMovil.seleccionarElementosMostrados'(4)

CustomKeywords.'oms.SeleccioneUnEquipoMovil.seleccionarEquipoMovilClick'(2)

CustomKeywords.'oms.SeleccioneUnEquipoMovil.clickAceptar'()

CustomKeywords.'oms.Window_SeleccioneUnEquipoMovil.aceptarSeleccion'()

CustomKeywords.'oms.Window_Despacho.clickConfirmarDespacho'()

CustomKeywords.'oms.Window_Documento_Tab_EquiposMoviles.clickDespachar'()

CustomKeywords.'oms.Window_SeleccioneUnEquipoMovil.clicklista'()

CustomKeywords.'oms.SeleccioneUnEquipoMovil.seleccionarElementosMostrados'(4)

CustomKeywords.'oms.SeleccioneUnEquipoMovil.seleccionarEquipoMovilClick'(2)

CustomKeywords.'oms.SeleccioneUnEquipoMovil.clickAceptar'()

CustomKeywords.'oms.Window_SeleccioneUnEquipoMovil.aceptarSeleccion'()

CustomKeywords.'oms.Window_Despacho.clickConfirmarDespacho'()

CustomKeywords.'oms.Window_Documento_Tab_EquiposMoviles.verificarMultipleDespacho'()

CustomKeywords.'oms.Window_Documento.cerrarVentanaForzado'()